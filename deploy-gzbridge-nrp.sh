#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

source $HOME/.bashrc

# Install node modules
npm install

# install bower components
bower install

#
# build the c++ server component
#
rm -rf build
mkdir build
cd build


# Run cmake and check for the exit code
cmake ..

RETVAL=$?
if [ $RETVAL -ne 0 ]; then
  echo There are cmake errors, exiting.
  exit 1
fi

# continue building if cmake is happy
make -j `nproc`

cd ../gzbridge
$DIR/node_modules/.bin/node-gyp configure

# gazebo pkg-config in binding.gyp adds some weird -lBoost::* dependencies to the build command that can't be found. Manually replacing them with the right boost library names
sed -i 's/Boost::/boost_/g' build/gzbridge.target.mk

$DIR/node_modules/.bin/node-gyp build -r

RETVAL=$?
if [ $RETVAL -ne 0 ]; then
  echo There are node-gyp build errors, exiting.
  exit 1
fi

echo "Done"

