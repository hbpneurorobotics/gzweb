#!/bin/bash
set -e
set -o xtrace

whoami
env | sort
pwd

if [ -f .ci/env ]; then
    # add quotes to all vars (but do it once)
    sudo sed -i -E 's/="*(.*[^"])"*$/="\1"/' .ci/env 
    source '.ci/env'
fi

sudo apt-get update && sudo apt-get install -y cmake
# Declare  nvm
. $HOME/.nvm/nvm.sh
. $HOME/.bashrc 
# Make gazebo discoverable
export PKG_CONFIG_PATH=$HOME/.local/lib/pkgconfig:$PKG_CONFIG_PATH


npm install -g bower
rm -rf build/
rm -rf gzbridge/build
sudo chown -R bbpnrsoa:bbp-ext $WORKSPACE/$GIT_CHECKOUT_DIR/http

./deploy-gzbridge-nrp.sh

npm dedupe
npm install --no-save
cd gz3d/utils
npm dedupe
npm install --no-save
