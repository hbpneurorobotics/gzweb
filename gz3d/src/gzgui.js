/*global $:false */
/*global angular*/

var guiEvents = new EventEmitter2({ verbose: true });

var emUnits = function(value) {
  return value * parseFloat($('#gz3d-body').css('font-size'));
};

var isMobile = function() {
  var agent = navigator.userAgent || navigator.vendor || window.opera;
  return (
    / (android | bb\d+| meego).+mobile | avantgo | bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
      agent
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
      agent.substr(0, 4)
    )
  );
};

var isTouchDevice = isMobile();

var isWideScreen = function() {
  return $(window).width() / emUnits(1) > 35;
};
var isTallScreen = function() {
  return $(window).height() / emUnits(1) > 35;
};

function getNameFromPath(path) {
  if (path === 'box') {
    return 'Box';
  }
  if (path === 'sphere') {
    return 'Sphere';
  }
  if (path === 'cylinder') {
    return 'Cylinder';
  }
  if (path === 'pointlight') {
    return 'Point Light';
  }
  if (path === 'spotlight') {
    return 'Spot Light';
  }
  if (path === 'directionallight') {
    return 'Directional Light';
  }

  if (GZ3D.modelList) {
    for (var i = 0; i < GZ3D.modelList.length; ++i) {
      for (var j = 0; j < GZ3D.modelList[i].models.length; ++j) {
        if (GZ3D.modelList[i].models[j].modelPath === path) {
          return GZ3D.modelList[i].models[j].modelTitle;
        }
      }
    }
  }
}

// World tree
var gzangular = angular.module('gzangular', []);
// add ng-right-click
gzangular.directive('ngRightClick', function($parse) {
  return function(scope, element, attrs) {
    var fn = $parse(attrs.ngRightClick);
    element.bind('contextmenu', function(event) {
      scope.$apply(function() {
        event.preventDefault();
        fn(scope, { $event: event });
      });
    });
  };
});

gzangular.controller('treeControl', [
  '$scope',
  function($scope) {
    $scope.updateStats = function() {
      $scope.models = modelStats;
      $scope.lights = lightStats;
      $scope.scene = sceneStats;
      $scope.physics = physicsStats;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };

    $scope.selectEntity = function(name) {
      guiEvents.emit('selectEntity', name);
    };

    $scope.openEntityMenu = function(event, name) {
      guiEvents.emit('openEntityPopup', event, name);
    };

    $scope.openTab = function(tab) {
      guiEvents.emit('openTab', tab, 'treeMenu');
    };

    $scope.changePose = function(prop1, prop2, name, value) {
      guiEvents.emit('setPose', prop1, prop2, name, value);
    };

    $scope.changeLight = function(prop, name, value) {
      guiEvents.emit('setLight', prop, name, value);
    };

    $scope.toggleProperty = function(prop, entity, subEntity) {
      // only for links so far
      guiEvents.emit('toggleProperty', prop, entity, subEntity);
    };
  }
]);

// Insert menu
gzangular.controller('insertControl', [
  '$scope',
  '$http',
  function($scope, $http) {
    $scope.spawnEntity = function(path) {
      guiEvents.emit('spawn_entity_start', path);
    };

    $scope.openTab = function(tab) {
      guiEvents.emit('openTab', tab, 'insertMenu');
    };
  }
]);

/**
 * Graphical user interface
 * @constructor
 * @param {GZ3D.Scene} scene - A scene to connect to
 */
GZ3D.Gui = function(scene) {
  this.scene = scene;
  this.domElement = scene.getDomElement();
  this.init();
  this.emitter = new EventEmitter2({ verbose: true });
  this.guiEvents = guiEvents;
};

/**
 * Initialize GUI
 */
GZ3D.Gui.prototype.init = function() {
  this.spawnState = null;
  this.longPressContainerState = null;
  this.showNotifications = false;
  this.openTreeWhenSelected = false;

  var that = this;

  // On guiEvents, emitter events
  guiEvents.on('manipulation_mode', function(mode) {
    that.scene.setManipulationMode(mode);
    var space = that.scene.modelManipulator.space;

    if (mode === 'view') {
      guiEvents.emit('notification_popup', 'View mode');
    } else {
      guiEvents.emit(
        'notification_popup',
        mode.charAt(0).toUpperCase() +
          mode.substring(1) +
          ' mode in ' +
          space.charAt(0).toUpperCase() +
          space.substring(1) +
          ' space'
      );
    }
  });

  // Create temp model
  guiEvents.on('spawn_entity_start', function(modelPath, modelSDF) {
    // manually trigger view mode
    that.scene.setManipulationMode('view');

    var name = getNameFromPath(modelPath);

    that.spawnState = 'START';
    that.scene.spawnModel.start(modelPath, modelSDF, name, function(obj) {
      that.emitter.emit('entityCreated', obj, modelPath);
    });
    guiEvents.emit(
      'notification_popup',
      'Place ' + name + ' at the desired position'
    );
  });

  // Move temp model by touch
  guiEvents.on('spawn_entity_move', function(event) {
    that.spawnState = 'MOVE';
    that.scene.spawnModel.onTouchMove(event, false);
  });
  // Place temp model by touch
  guiEvents.on('spawn_entity_end', function() {
    if (that.spawnState === 'MOVE') {
      that.scene.spawnModel.onTouchEnd();
    }
    that.spawnState = null;
  });

  guiEvents.on('world_reset', function() {
    that.emitter.emit('reset', 'world');
    guiEvents.emit('notification_popup', 'Reset world');
  });

  guiEvents.on('model_reset', function() {
    that.emitter.emit('reset', 'model');
    guiEvents.emit('notification_popup', 'Reset model poses');
  });

  guiEvents.on('view_reset', function() {
    that.scene.resetView();
    guiEvents.emit('notification_popup', 'Reset view');
  });

  guiEvents.on('pause', function(paused) {
    that.emitter.emit('pause', paused);
  });

  guiEvents.on('show_collision', function() {
    that.scene.showCollision(!that.scene.showCollisions);
    if (!that.scene.showCollisions) {
      guiEvents.emit('notification_popup', 'Hiding collisions');
    } else {
      guiEvents.emit('notification_popup', 'Viewing collisions');
    }
  });

  guiEvents.on('show_grid', function(option) {
    if (option === 'show') {
      that.scene.grid.visible = true;
    } else if (option === 'hide') {
      that.scene.grid.visible = false;
    } else if (option === 'toggle') {
      that.scene.grid.visible = !that.scene.grid.visible;
    }

    if (!that.scene.grid.visible) {
      guiEvents.emit('notification_popup', 'Hiding grid');
    } else {
      guiEvents.emit('notification_popup', 'Viewing grid');
    }
  });

  guiEvents.on('show_orbit_indicator', function() {});

  guiEvents.on('snap_to_grid', function() {
    if (that.scene.modelManipulator.snapDist === null) {
      that.scene.modelManipulator.snapDist = 0.5;
      that.scene.spawnModel.snapDist = that.scene.modelManipulator.snapDist;
      guiEvents.emit('notification_popup', 'Snapping to grid');
    } else {
      that.scene.modelManipulator.snapDist = null;
      that.scene.spawnModel.snapDist = null;
      guiEvents.emit('notification_popup', 'Not snapping to grid');
    }
  });

  guiEvents.on('longpress_container_start', function(event) {
    if (
      event.originalEvent.touches.length !== 1 ||
      that.scene.modelManipulator.hovered ||
      that.scene.spawnModel.active
    ) {
      guiEvents.emit('longpress_container_end', event.originalEvent, true);
    } else {
      that.scene.showRadialMenu(event);
      that.longPressContainerState = 'START';
    }
  });

  guiEvents.on('longpress_container_end', function(event, cancel) {
    if (that.longPressContainerState !== 'START') {
      that.longPressContainerState = 'END';
      return;
    }
    that.longPressContainerState = 'END';
    if (that.scene.radialMenu.showing) {
      if (cancel) {
        that.scene.radialMenu.hide(event);
      } else {
        that.scene.radialMenu.hide(event, function(type, entity) {
          if (type === 'delete') {
            that.emitter.emit('deleteEntity', entity);
            that.scene.setManipulationMode('view');
          } else if (type === 'translate') {
            that.scene.attachManipulator(entity, type);
          } else if (type === 'rotate') {
            that.scene.attachManipulator(entity, type);
          } else if (type === 'transparent') {
            guiEvents.emit('set_view_as', 'transparent');
          } else if (type === 'wireframe') {
            guiEvents.emit('set_view_as', 'wireframe');
          } else if (type === 'joints') {
            that.scene.selectEntity(entity);
            guiEvents.emit('view_joints');
          }
        });
      }
    }
  });

  guiEvents.on('longpress_container_move', function(event) {
    if (event.originalEvent.touches.length !== 1) {
      guiEvents.emit('longpress_container_end', event.originalEvent, true);
    } else {
      if (that.longPressContainerState !== 'START') {
        return;
      }
      if (that.scene.radialMenu.showing) {
        that.scene.radialMenu.onLongPressMove(event);
      }
    }
  });

  guiEvents.on('longpress_insert_start', function(event, path) {
    navigator.vibrate(50);
    guiEvents.emit('spawn_entity_start', path);
    event.stopPropagation();
  });

  guiEvents.on('longpress_insert_end', function(event) {
    guiEvents.emit('spawn_entity_end');
  });

  guiEvents.on('longpress_insert_move', function(event) {
    guiEvents.emit('spawn_entity_move', event);
    event.stopPropagation();
  });

  var notificationTimeout;
  guiEvents.on('notification_popup', function(notification, duration) {
    if (this.showNotifications) {
      clearTimeout(notificationTimeout);
      $('#notification-popup').popup('close');
      $('#notification-popup').html('&nbsp;' + notification + '&nbsp;');
      $('#notification-popup').popup('open', {
        y: window.innerHeight - 50
      });

      if (duration === undefined) {
        duration = 2000;
      }
      notificationTimeout = setTimeout(function() {
        $('#notification-popup').popup('close');
      }, duration);
    }
  });

  guiEvents.on('right_click', function(event) {
    that.scene.onRightClick(event, function(entity) {
      that.openEntityPopup(event, entity);
    });
  });

  guiEvents.on('set_view_as', function(viewAs) {
    that.scene.setViewAs(that.scene.selectedEntity, viewAs);
  });

  guiEvents.on('view_joints', function() {
    that.scene.viewJoints(that.scene.selectedEntity);
  });

  guiEvents.on('lookat_entity', function() {
    that.scene.controls.setLookatTarget(that.scene.selectedEntity);
  });

  guiEvents.on('delete_entity', function() {
    that.emitter.emit('deleteEntity', that.scene.selectedEntity);
    that.scene.selectEntity(null);
    that.scene.manipulationMode = 'view';
  });

  guiEvents.on('duplicate_entity', function() {
    var modelEntity = that.scene.selectedEntity;
    if (that.scene.selectedEntity) {
      var entity = GZ3D.Gui.findBaseModelName(modelEntity.name);
      if (entity) {
        // manually trigger view mode
        that.scene.setManipulationMode('view');

        let name = getNameFromPath(entity);
        let modelSDF = modelEntity.isSimpleShape()
          ? undefined
          : entity + '/model.sdf';

        that.spawnState = 'START';

        that.scene.spawnModel.start(
          entity,
          modelSDF,
          modelEntity.name,
          obj => that.emitter.emit('entityCreated', obj, entity),
          modelEntity.position,
          modelEntity.quaternion,
          modelEntity.scale
        );

        guiEvents.emit(
          'notification_popup',
          'Place ' + name + ' at the desired position'
        );
      }
    }
  });

  guiEvents.on('pointerOnMenu', function() {
    that.scene.pointerOnMenu = true;
  });

  guiEvents.on('pointerOffMenu', function() {
    that.scene.pointerOnMenu = false;
  });

  guiEvents.on('selectEntity', function(name) {
    var object = that.scene.getByName(name);
    that.scene.selectEntity(object);
  });

  guiEvents.on('openEntityPopup', function(event, name) {
    if (!isTouchDevice) {
      var object = that.scene.getByName(name);
      that.openEntityPopup(event, object);
    }
  });

  guiEvents.on('setPoseStats', function(modelName, linkName) {
    var object;
    if (linkName === undefined) {
      object = that.scene.getByName(modelName);
    } else {
      object = that.scene.getByName(linkName);
    }

    var stats = {};
    stats.name = object.name;
    stats.pose = {};
    stats.pose.position = {
      x: object.position.x,
      y: object.position.y,
      z: object.position.z
    };
    stats.pose.orientation = {
      x: object.quaternion._x,
      y: object.quaternion._y,
      z: object.quaternion._z,
      w: object.quaternion._w
    };

    if (object.children[0] instanceof THREE.Light) {
      that.setLightStats(stats, 'update');
    } else {
      that.setModelStats(stats, 'update');
    }
  });

  guiEvents.on('resizePanel', function() {
    if ($('.leftPanels').is(':visible')) {
      if (isWideScreen()) {
        $('.tab').css('left', '23em');
      } else {
        $('.tab').css('left', '10.5em');
      }
    }

    if ($('.propertyPanels').is(':visible')) {
      var maxWidth = $(window).width();
      if (isWideScreen()) {
        maxWidth = emUnits(23);
      }

      $('.propertyPanels').css('width', maxWidth);
    }
  });

  guiEvents.on('setPose', function(prop1, prop2, name, value) {
    if (value === undefined) {
      return;
    }

    var entity = that.scene.getByName(name);
    if (prop1 === 'orientation') {
      entity['rotation']['_' + prop2] = value;
      entity['quaternion'].setFromEuler(entity['rotation']);
    } else {
      entity[prop1][prop2] = value;
    }
    entity.updateMatrixWorld();

    if (
      entity.children[0] &&
      (entity.children[0] instanceof THREE.SpotLight ||
        entity.children[0] instanceof THREE.DirectionalLight)
    ) {
      var lightObj = entity.children[0];
      var dir = new THREE.Vector3(0, 0, 0);
      dir.copy(entity.direction);
      entity.localToWorld(dir);
      lightObj.target.position.copy(dir);
    }

    that.scene.emitter.emit('entityChanged', entity);
  });

  guiEvents.on('setLight', function(prop, name, value) {
    if (value === undefined) {
      return;
    }

    var entity = that.scene.getByName(name);
    var lightObj = entity.children[0];
    if (prop === 'diffuse') {
      lightObj.color = new THREE.Color(value);
    } else if (prop === 'specular') {
      entity.serverProperties.specular = new THREE.Color(value);
    } else if (prop === 'range') {
      lightObj.distance = value;
    } else if (prop === 'attenuation_constant') {
      entity.serverProperties.attenuation_constant = value;
    } else if (prop === 'attenuation_linear') {
      entity.serverProperties.attenuation_linear = value;
      lightObj.intensity = lightObj.intensity / (1 + value);
    } else if (prop === 'attenuation_quadratic') {
      entity.serverProperties.attenuation_quadratic = value;
      lightObj.intensity = lightObj.intensity / (1 + value);
    }

    // updating color too often, maybe only update when popup is closed
    that.scene.emitter.emit('entityChanged', entity);
  });

  guiEvents.on('toggleProperty', function(prop, subEntityName) {
    var entity = that.scene.getByName(subEntityName);
    entity.serverProperties[prop] = !entity.serverProperties[prop];

    that.scene.emitter.emit('linkChanged', entity);
  });
};

var sceneStats = {};
/**
 * Update scene stats on scene tree
 * @param {} stats
 */
GZ3D.Gui.prototype.setSceneStats = function(stats) {
  sceneStats['ambient'] = this.round(stats.ambient, true);
  sceneStats['background'] = this.round(stats.background, true);
};

var physicsStats = {};
/**
 * Update physics stats on scene tree
 * @param {} stats
 */
GZ3D.Gui.prototype.setPhysicsStats = function(stats) {
  physicsStats = stats;
  physicsStats['enable_physics'] = this.trueOrFalse(
    physicsStats['enable_physics']
  );
  physicsStats['max_step_size'] = this.round(
    physicsStats['max_step_size'],
    false,
    3
  );
  physicsStats['gravity'] = this.round(physicsStats['gravity'], false, 3);
  physicsStats['sor'] = this.round(physicsStats['sor'], false, 3);
  physicsStats['cfm'] = this.round(physicsStats['cfm'], false, 3);
  physicsStats['erp'] = this.round(physicsStats['erp'], false, 3);
  physicsStats['contact_max_correcting_vel'] = this.round(
    physicsStats['contact_max_correcting_vel'],
    false,
    3
  );
  physicsStats['contact_surface_layer'] = this.round(
    physicsStats['contact_surface_layer'],
    false,
    3
  );

  this.updateStats();
};

var modelStats = [];
/**
 * Update model stats on property panel
 * @param {} stats
 * @param {} action: 'update' / 'delete'
 */
GZ3D.Gui.prototype.setModelStats = function(stats, action) {
  var modelName = stats.name;
  var linkShortName;

  // if it's a link
  if (stats.name.indexOf('::') >= 0) {
    modelName = stats.name.substring(0, stats.name.indexOf('::'));
    linkShortName = stats.name.substring(stats.name.lastIndexOf('::') + 2);
  }

  if (action === 'update') {
    var model = $.grep(modelStats, function(e) {
      return e.name === modelName;
    });

    var formatted;

    // New model
    if (model.length === 0) {
      var thumbnail = this.findModelThumbnail(modelName);

      formatted = this.formatStats(stats);

      modelStats.push({
        name: modelName,
        thumbnail: thumbnail,
        selected: 'unselectedTreeItem',
        is_static: this.trueOrFalse(stats.is_static),
        position: formatted.pose.position,
        orientation: formatted.pose.orientation,
        links: [],
        joints: []
      });

      var newModel = modelStats[modelStats.length - 1];

      // links
      for (var l = 0; l < stats.link.length; ++l) {
        var shortName = stats.link[l].name.substring(
          stats.link[l].name.lastIndexOf('::') + 2
        );

        formatted = this.formatStats(stats.link[l]);

        newModel.links.push({
          name: stats.link[l].name,
          shortName: shortName,
          self_collide: this.trueOrFalse(stats.link[l].self_collide),
          gravity: this.trueOrFalse(stats.link[l].gravity),
          kinematic: this.trueOrFalse(stats.link[l].kinematic),
          canonical: this.trueOrFalse(stats.link[l].canonical),
          position: formatted.pose.position,
          orientation: formatted.pose.orientation,
          inertial: formatted.inertial
        });
      }

      // joints
      for (var j = 0; j < stats.joint.length; ++j) {
        var jointShortName = stats.joint[j].name.substring(
          stats.joint[j].name.lastIndexOf('::') + 2
        );
        var parentShortName = stats.joint[j].parent.substring(
          stats.joint[j].parent.lastIndexOf('::') + 2
        );
        var childShortName = stats.joint[j].child.substring(
          stats.joint[j].child.lastIndexOf('::') + 2
        );

        var type;
        switch (stats.joint[j].type) {
          case 1:
            type = 'Revolute';
            break;
          case 2:
            type = 'Revolute2';
            break;
          case 3:
            type = 'Prismatic';
            break;
          case 4:
            type = 'Universal';
            break;
          case 5:
            type = 'Ball';
            break;
          case 6:
            type = 'Screw';
            break;
          case 7:
            type = 'Gearbox';
            break;
          default:
            type = 'Unknown';
        }

        formatted = this.formatStats(stats.joint[j]);

        newModel.joints.push({
          name: stats.joint[j].name,
          shortName: jointShortName,
          type: type,
          parent: stats.joint[j].parent,
          parentShortName: parentShortName,
          child: stats.joint[j].child,
          childShortName: childShortName,
          position: formatted.pose.position,
          orientation: formatted.pose.orientation,
          axis1: formatted.axis1,
          axis2: formatted.axis2
        });
      }
    } else {
      // Update existing model
      var link;

      if (stats.link && stats.link[0]) {
        var LinkShortName = stats.link[0].name;

        link = $.grep(model[0].links, function(e) {
          return e.shortName === LinkShortName;
        });

        if (link[0]) {
          if (link[0].self_collide) {
            link[0].self_collide = this.trueOrFalse(stats.link[0].self_collide);
          }
          if (link[0].gravity) {
            link[0].gravity = this.trueOrFalse(stats.link[0].gravity);
          }
          if (link[0].kinematic) {
            link[0].kinematic = this.trueOrFalse(stats.link[0].kinematic);
          }
        }
      }

      if (stats.position) {
        stats.pose = {};
        stats.pose.position = stats.position;
        stats.pose.orientation = stats.orientation;
      }

      if (stats.pose) {
        formatted = this.formatStats(stats);

        if (linkShortName === undefined) {
          model[0].position = formatted.pose.position;
          model[0].orientation = formatted.pose.orientation;
        } else {
          link = $.grep(model[0].links, function(e) {
            return e.shortName === linkShortName;
          });

          link[0].position = formatted.pose.position;
          link[0].orientation = formatted.pose.orientation;
        }
      }
    }
  } else if (action === 'delete') {
    this.deleteFromStats('model', modelName);
  }

  this.updateStats();
};

var lightStats = [];
/**
 * Update light stats on property panel
 * @param {} stats
 * @param {} action: 'update' / 'delete'
 */
GZ3D.Gui.prototype.setLightStats = function(stats, action) {
  var name = stats.name;

  if (action === 'update') {
    var light = $.grep(lightStats, function(e) {
      return e.name === name;
    });

    var formatted;

    // New light
    if (light.length === 0) {
      var type = stats.type;

      var thumbnail;
      switch (type) {
        case GZ3D.LIGHT_SPOT:
          thumbnail = 'style/images/spotlight.png';
          break;
        case GZ3D.LIGHT_DIRECTIONAL:
          thumbnail = 'style/images/directionallight.png';
          break;
        default:
          thumbnail = 'style/images/pointlight.png';
      }

      stats.attenuation = {
        constant: stats.attenuation_constant,
        linear: stats.attenuation_linear,
        quadratic: stats.attenuation_quadratic
      };

      formatted = this.formatStats(stats);

      var direction;
      if (stats.direction) {
        direction = stats.direction;
      }

      lightStats.push({
        name: name,
        thumbnail: thumbnail,
        selected: 'unselectedTreeItem',
        position: formatted.pose.position,
        orientation: formatted.pose.orientation,
        diffuse: formatted.diffuse,
        specular: formatted.specular,
        color: formatted.color,
        range: stats.range,
        attenuation: this.round(stats.attenuation, false, null),
        direction: direction
      });
    } else {
      formatted = this.formatStats(stats);

      if (stats.pose) {
        light[0].position = formatted.pose.position;
        light[0].orientation = formatted.pose.orientation;
      }

      if (stats.diffuse) {
        light[0].diffuse = formatted.diffuse;
      }

      if (stats.specular) {
        light[0].specular = formatted.specular;
      }
    }
  } else if (action === 'delete') {
    this.deleteFromStats('light', name);
  }

  this.updateStats();
};

/**
 * Can model be duplicated ?
 * @param {} instanceName
 * @returns string
 */
GZ3D.Gui.findBaseModelName = function(modelName) {
  var basicObjects = [
    'box',
    'sphere',
    'cylinder',
    'pointlight',
    'pointlight',
    'spotlight',
    'directionallight'
  ];
  var i, j, k;

  for (k = 0; k < 2; k++) {
    if (k > 0) {
      let idx = modelName.search('(_[0-9]+)+');
      if (idx > 0) {
        modelName = modelName.substring(0, idx);
      }
    }

    if (GZ3D.modelList) {
      for (i = 0; i < GZ3D.modelList.length; ++i) {
        for (j = 0; j < GZ3D.modelList[i].models.length; ++j) {
          if (
            modelName ===
            GZ3D.modelList[i].models[j].modelTitle
              .toLowerCase()
              .replace(' ', '_')
          ) {
            return GZ3D.modelList[i].models[j].modelPath;
          }
        }
      }
    }

    for (j = 0; j < basicObjects.length; ++j) {
      if (modelName === basicObjects[j]) {
        return modelName;
      }
    }
  }

  return undefined;
};

/**
 * Can model be duplicated ?
 * @param {} instanceName
 * @returns string
 */
GZ3D.Gui.prototype.canModelBeDuplicated = function(modelName) {
  if (this.scene.spawnModel.active) {
    return false;
  }

  return GZ3D.Gui.findBaseModelName(modelName) !== undefined;
};

/**
 * Find thumbnail
 * @param {} instanceName
 * @returns string
 */
GZ3D.Gui.prototype.findModelThumbnail = function(instanceName) {
  if (GZ3D.modelList) {
    for (var i = 0; i < GZ3D.modelList.length; ++i) {
      for (var j = 0; j < GZ3D.modelList[i].models.length; ++j) {
        var path = GZ3D.modelList[i].models[j].modelPath;
        if (instanceName.indexOf(path) >= 0) {
          return '/assets/' + path + '/thumbnails/0.png';
        }
      }
    }
  }

  if (instanceName.indexOf('box') >= 0) {
    return 'style/images/box.png';
  }
  if (instanceName.indexOf('sphere') >= 0) {
    return 'style/images/sphere.png';
  }
  if (instanceName.indexOf('cylinder') >= 0) {
    return 'style/images/cylinder.png';
  }
  return 'style/images/box.png';
};

/**
 * Update model stats
 */
GZ3D.Gui.prototype.updateStats = function() {
  var tree = angular.element($('#treeMenu')).scope();
  if (typeof tree !== 'undefined' && typeof tree.updateStats !== 'undefined') {
    tree.updateStats();
  }
};

/**
 * Open entity (model/light) context menu
 * @param {} event
 * @param {THREE.Object3D} entity
 */
/* Unused, left for future reference
GZ3D.Gui.prototype.openEntityPopup = function(event, entity)
{
  this.scene.selectEntity(entity);
  $('.ui-popup').popup('close');

  if (entity.children[0] instanceof THREE.Light)
  {
    $('#view-transparent').css('visibility','collapse');
    $('#view-wireframe').css('visibility','collapse');
    $('#view-joints').css('visibility','collapse');
    $('#model-popup').popup('open',
      {x: event.offsetX + emUnits(6),
       y: event.offsetY + emUnits(-8)});
  }
  else
  {
    if (this.scene.selectedEntity.viewAs === 'transparent')
    {
      $('#view-transparent').buttonMarkup({icon: 'check'});
    }
    else
    {
      $('#view-transparent').buttonMarkup({icon: 'false'});
    }

    if (this.scene.selectedEntity.viewAs === 'wireframe')
    {
      $('#view-wireframe').buttonMarkup({icon: 'check'});
    }
    else
    {
      $('#view-wireframe').buttonMarkup({icon: 'false'});
    }

    if (entity.joint === undefined || entity.joint.length === 0)
    {
      $('#view-joints a').css('color', '#888888');
      $('#view-joints').buttonMarkup({icon: 'false'});
    }
    else
    {
      $('#view-joints a').css('color', '#ffffff');
      if (entity.getObjectByName('JOINT_VISUAL', true))
      {
        $('#view-joints').buttonMarkup({icon: 'check'});
      }
      else
      {
        $('#view-joints').buttonMarkup({icon: 'false'});
      }
    }

    $('#view-transparent').css('visibility','visible');
    $('#view-wireframe').css('visibility','visible');
    $('#view-joints').css('visibility','visible');
    $('#model-popup').popup('open',
      {x: event.offsetX + emUnits(6),
       y: event.offsetY + emUnits(0)});
  }
};
*/
/**
 * Format stats message for proper display
 * @param {} stats
 * @returns {Object.<position, orientation, inertial, diffuse, specular, attenuation>}
 */
GZ3D.Gui.prototype.formatStats = function(stats) {
  var position, orientation;
  var quat, rpy;
  if (stats.pose) {
    position = this.round(stats.pose.position, false, null);

    quat = new THREE.Quaternion(
      stats.pose.orientation.x,
      stats.pose.orientation.y,
      stats.pose.orientation.z,
      stats.pose.orientation.w
    );

    rpy = new THREE.Euler();
    rpy.setFromQuaternion(quat);

    orientation = { roll: rpy._x, pitch: rpy._y, yaw: rpy._z };
    orientation = this.round(orientation, false, null);
  }
  var inertial;
  if (stats.inertial) {
    inertial = this.round(stats.inertial, false, 3);

    var inertialPose = stats.inertial.pose;
    inertial.pose = {};

    inertial.pose.position = {
      x: inertialPose.position.x,
      y: inertialPose.position.y,
      z: inertialPose.position.z
    };

    inertial.pose.position = this.round(inertial.pose.position, false, 3);

    quat = new THREE.Quaternion(
      inertialPose.orientation.x,
      inertialPose.orientation.y,
      inertialPose.orientation.z,
      inertialPose.orientation.w
    );

    rpy = new THREE.Euler();
    rpy.setFromQuaternion(quat);

    inertial.pose.orientation = { roll: rpy._x, pitch: rpy._y, yaw: rpy._z };
    inertial.pose.orientation = this.round(inertial.pose.orientation, false, 3);
  }
  var diffuse, colorHex, comp;
  var color = {};
  if (stats.diffuse) {
    diffuse = this.round(stats.diffuse, true);

    colorHex = {};
    for (comp in diffuse) {
      if (diffuse.hasOwnProperty(comp)) {
        colorHex[comp] = diffuse[comp].toString(16);
        if (colorHex[comp].length === 1) {
          colorHex[comp] = '0' + colorHex[comp];
        }
      }
    }
    color.diffuse = '#' + colorHex['r'] + colorHex['g'] + colorHex['b'];
  }
  var specular;
  if (stats.specular) {
    specular = this.round(stats.specular, true);

    colorHex = {};
    for (comp in specular) {
      if (specular.hasOwnProperty(comp)) {
        colorHex[comp] = specular[comp].toString(16);
        if (colorHex[comp].length === 1) {
          colorHex[comp] = '0' + colorHex[comp];
        }
      }
    }
    color.specular = '#' + colorHex['r'] + colorHex['g'] + colorHex['b'];
  }
  var axis1;
  if (stats.axis1) {
    axis1 = {};
    axis1 = this.round(stats.axis1);
    axis1.direction = this.round(stats.axis1.xyz, false, 3);
  }
  var axis2;
  if (stats.axis2) {
    axis2 = {};
    axis2 = this.round(stats.axis2);
    axis2.direction = this.round(stats.axis2.xyz, false, 3);
  }

  return {
    pose: { position: position, orientation: orientation },
    inertial: inertial,
    diffuse: diffuse,
    specular: specular,
    color: color,
    axis1: axis1,
    axis2: axis2
  };
};

/**
 * Round numbers and format colors
 * @param {} stats
 * @param {} decimals - number of decimals to display, null for input fields
 * @returns result
 */
GZ3D.Gui.prototype.round = function(stats, isColor, decimals) {
  var result = stats;
  if (typeof result === 'number') {
    result = this.roundNumber(result, isColor, decimals);
  } else {
    // array of numbers
    result = this.roundArray(result, isColor, decimals);
  }
  return result;
};

/**
 * Round number and format color
 * @param {} stats
 * @param {} decimals - number of decimals to display, null for input fields
 * @returns result
 */
GZ3D.Gui.prototype.roundNumber = function(stats, isColor, decimals) {
  var result = stats;
  if (isColor) {
    result = Math.round(result * 255);
  } else {
    if (decimals === null) {
      result = Math.round(result * 1000) / 1000;
    } else {
      result = result.toFixed(decimals);
    }
  }
  return result;
};

/**
 * Round each number in an array
 * @param {} stats
 * @param {} decimals - number of decimals to display, null for input fields
 * @returns result
 */
GZ3D.Gui.prototype.roundArray = function(stats, isColor, decimals) {
  var result = stats;
  for (var key in result) {
    if (typeof result[key] === 'number') {
      result[key] = this.roundNumber(result[key], isColor, decimals);
    }
  }
  return result;
};

/**
 * Format toggle items
 * @param {} stats: true / false
 * @returns {Object.<icon, title>}
 */
GZ3D.Gui.prototype.trueOrFalse = function(stats) {
  return stats
    ? { icon: 'true', title: 'True' }
    : { icon: 'false', title: 'False' };
};

/**
 * Delete an entity from stats list
 * @param {} type: 'model' / 'light'
 * @param {} name
 */
GZ3D.Gui.prototype.deleteFromStats = function(type, name) {
  var list = type === 'model' ? modelStats : lightStats;

  for (var i = 0; i < list.length; ++i) {
    if (list[i].name === name) {
      list.splice(i, 1);
      break;
    }
  }
};
