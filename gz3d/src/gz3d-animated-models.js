GZ3D.AnimatedModel = function(scene) {
  this.scene = scene;
  this.loader = null;
};

GZ3D.AnimatedModel.prototype.loadAnimatedModel = function(modelName) {
  // Helper function to enable 'skinning' property so three.js treats meshes as deformable
  var enableSkinning = function(skinnedMesh) {
    var materials = skinnedMesh.material.materials;
    if (materials !== null && materials !== undefined) {
      for (var i = 0, length = materials.length; i < length; i++) {
        var mat = materials[i];
        mat.skinning = true;
      }
    }

    if (skinnedMesh.material !== undefined && skinnedMesh.material !== null) {
      skinnedMesh.material.skinning = true;
    }
  };

  // Load animated model with separate COLLADA loader instance
  var that = this;
  // Progress update: Add this asset to the assetProgressArray
  var element = {};
  element.id = modelName;
  element.url = GZ3D.animatedModel.assetsPath;
  element.progress = 0;
  element.totalSize = 0;
  element.done = false;
  GZ3D.assetProgressData.assets.push(element);

  this.scene.loadCollada(
    element.url,
    null,
    null,
    (dae) => {
      var modelParent = new THREE.Object3D();
      modelParent.name = modelName + '_animated';
      var linkParent = new THREE.Object3D();

      // Set gray, phong-shaded material for loaded model
      dae.traverse(function(child) {
        if (child instanceof THREE.Mesh) {
          var applyDefaultMaterial = true;

          if (child.material instanceof THREE.MultiMaterial) {
            if (
              child.material.materials[0].pbrMaterialDescription !== undefined
            ) {
              child.material = child.material.materials[0];
              applyDefaultMaterial = false;
            }
          } else if (child.material instanceof THREE.MeshPhongMaterial) {
            applyDefaultMaterial = false;
          }

          if (applyDefaultMaterial) {
            var transparentMaterial = new THREE.MeshPhongMaterial({
              color: 0x707070
            });
            transparentMaterial.wireframe = false;
            child.material = transparentMaterial;
          }
        }
      });

      // Enable skinning for all child meshes
      dae.traverse(function(child) {
        if (child instanceof THREE.SkinnedMesh) {
          enableSkinning(child);
        }
      });

      linkParent.add(dae);

      // Hide model coordinate frames for the time being; remove as soon as position offset and rotation axis issues are fixed
      /*var collada_scene_axes = new THREE.AxisHelper(2);
     linkParent.add(collada_scene_axes);*/

      modelParent.add(linkParent);

      // Hide model coordinate frames for the time being; remove as soon as position offset and rotation axis issues are fixed
      /*var model_parent_axes = new THREE.AxisHelper(4);
     modelParent.add(model_parent_axes);*/

      // Use scale, position, and rotation offsets provided in animated robot model specification
      var p = GZ3D.animatedModel.visualModelParams;
      modelParent.scale.x = modelParent.scale.y = modelParent.scale.z = p[6];
      modelParent.position.x = modelParent.position.x + p[0];
      modelParent.position.y = modelParent.position.y + p[1];
      modelParent.position.z = modelParent.position.z + p[2];
      modelParent.rotation.x = modelParent.rotation.x + p[3];
      modelParent.rotation.y = modelParent.rotation.y + p[4];
      modelParent.rotation.z = modelParent.rotation.z + p[5];

      // Build list of bones in rig, and attach it to scene node (userData) for later retrieval in animation handling
      var getBoneList = function(object) {
        var boneList = [];

        if (object instanceof THREE.Bone) {
          boneList.push(object);
        }

        for (var i = 0; i < object.children.length; i++) {
          boneList.push.apply(boneList, getBoneList(object.children[i]));
        }

        return boneList;
      };

      var boneList = getBoneList(dae);
      var boneHash = {};
      for (var k = 0; k < boneList.length; k++) {
        boneHash[boneList[k].name] = boneList[k];
      }
      // Skeleton visualization helper class
      var helper = new THREE.SkeletonHelper(dae);

      boneHash['Skeleton_Visual_Helper'] = helper;
      modelParent.userData = boneHash;

      helper.material.linewidth = 3;
      // Hide skeleton helper for the time being
      // TODO: Make this configurable for visualizing the underlying skeleton
      helper.visible = false;

      that.scene.add(helper);
      that.scene.add(modelParent);

      that.scene.applyComposerSettingsToModel(modelParent);

      // Progress update: execute callback
      element.done = true;
      if (GZ3D.assetProgressCallback) {
        GZ3D.assetProgressCallback(GZ3D.assetProgressData);
      }
    },
    function(progress) {
      element.progress = progress.loaded;
      element.totalSize = progress.total;
      element.error = progress.error;
      if (GZ3D.assetProgressCallback) {
        GZ3D.assetProgressCallback(GZ3D.assetProgressData);
      }
    }
  );
};

GZ3D.AnimatedModel.prototype.updateJoint = function(
  robotName,
  jointName,
  jointValue,
  jointAxis
) {
  // Check if there is an animated model for it on the client side
  var entity = this.scene.getByName(robotName + '_animated');
  if (entity) {
    if (entity.userData !== undefined && entity.userData !== null) {
      var boneName = jointName;

      // Retrieve bone instance from userData map of bone instances in animated model
      if (
        entity.userData[boneName] !== undefined &&
        entity.userData[boneName] !== null
      ) {
        var targetBone = entity.userData[boneName];
        var rotationAxis = jointAxis;
        var rotationAngle = jointValue;

        var rotationMatrix = new THREE.Matrix4();
        rotationMatrix.identity();
        rotationMatrix.makeRotationAxis(rotationAxis, rotationAngle);

        targetBone.setRotationFromMatrix(rotationMatrix);
      }
    }

    this.scene.refresh3DViews();
  }
};
