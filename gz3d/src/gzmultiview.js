/**
 * Created by Sandro Weber (webers@in.tum.de).
 */

GZ3D.RENDER_MODE = {
  MULTIPLE_CONTEXTS: 1,
  OFFSCREEN_AND_COPY_TO_CANVAS: 2,
  DYNAMIC: 3
};

GZ3D.MULTIVIEW_MAX_VIEW_COUNT = 10;
GZ3D.MULTIVIEW_MAINVIEW_NAME = 'main_view';
GZ3D.MULTIVIEW_DEFAULT_CAMERA_PARAMS = {
  fov: 60,
  aspectRatio: 1.6,
  near: 0.15,
  far: 100
};

GZ3D.MultiView = function(gz3dScene) {
  this.gz3dScene = gz3dScene;

  this.renderMode = GZ3D.RENDER_MODE.DYNAMIC;
  if (
    this.renderMode === GZ3D.RENDER_MODE.OFFSCREEN_AND_COPY_TO_CANVAS ||
    this.renderMode === GZ3D.RENDER_MODE.DYNAMIC
  ) {
    this.renderer = this.createRenderer();
  }
  this.views = [];
  // create the main user view
  this.mainUserView = this.createView(
    GZ3D.MULTIVIEW_MAINVIEW_NAME,
    GZ3D.MULTIVIEW_DEFAULT_CAMERA_PARAMS
  );
};

/**
 *
 * @param name
 * @param displayParams {left, top, width, height, zIndex, adjustable}
 * @param cameraParams {width, height, fov, near, far}
 */
GZ3D.MultiView.prototype.createView = function(name, cameraParams) {
  if (angular.isDefined(this.getViewByName(name))) {
    console.error(
      'GZ3D.MultiView.createView() - a view of that name already exists (' +
        name +
        ')'
    );
    return undefined;
  }

  if (this.views.length >= this.MULTIVIEW_MAX_VIEW_COUNT) {
    console.warn(
      'GZ3D.MultiView.createView() - creating new view will exceed MULTIVIEW_MAX_VIEW_COUNT(' +
        this.MULTIVIEW_MAX_VIEW_COUNT +
        '). This may cause z-ordering issues.'
    );
  }

  var that = this;

  // camera
  var camera = new THREE.PerspectiveCamera(
    cameraParams.fov,
    cameraParams.aspectRatio,
    cameraParams.near,
    cameraParams.far
  );
  camera.name = name;
  camera.up = new THREE.Vector3(0, 0, 1); // gazebo's up vector
  // gazebo's transform for the sensor object is different for some reason
  // or the camera sensor looks along the x-axis by default instead of negative z-axis
  camera.lookAt(camera.localToWorld(new THREE.Vector3(1, 0, 0)));
  camera.updateMatrixWorld();
  this.gz3dScene.scene.add(camera);

  // camera visualization
  var cameraHelper = new THREE.CameraHelper(camera);
  cameraHelper.visible = false;
  camera.cameraHelper = cameraHelper;
  this.gz3dScene.scene.add(cameraHelper);
  cameraHelper.update();

  var view;

  if (this.renderMode === GZ3D.RENDER_MODE.DYNAMIC) {
    // assemble view
    view = {
      name: name,
      camera: camera,
      container: undefined,
      renderer: this.renderer,
      canvas: this.renderer.domElement,
      initAspectRatio: cameraParams.aspectRatio,
      masterView: this.views.length === 0 // First view is the master view, it owns the renderer and renders directly on screen
    }; // Master view may be changed later, if another view is bigger than this view
  } else if (this.renderMode === GZ3D.RENDER_MODE.MULTIPLE_CONTEXTS) {
    // renderer
    var renderer = this.createRenderer();

    // assemble view
    view = {
      name: name,
      camera: camera,
      container: undefined,
      renderer: renderer,
      canvas: renderer.domElement,
      initAspectRatio: cameraParams.aspectRatio
    };
  } else if (
    this.renderMode === GZ3D.RENDER_MODE.OFFSCREEN_AND_COPY_TO_CANVAS
  ) {
    // assemble view
    view = {
      name: name,
      camera: camera,
      renderer: this.renderer,
      container: undefined,
      canvas: undefined,
      initAspectRatio: cameraParams.aspectRatio
    };
  }

  view.needsRefresh = true;
  view.cameraViewCurrentMatrix = camera.matrixWorld.clone();

  this.views.push(view);

  return view;
};

// This function is used as a callback attached to the "deleteEntity" event
GZ3D.MultiView.prototype.deleteRobotViews = function(entity) {
  const name = entity.name;
  if (!GZ3D.isRobot(entity)) {
    console.error(
      'GZ3D.MultiView.deleteRobotViews() - trying to delete views of a non-robot entity (' +
        name +
        ')'
    );
    return;
  }

  this.views = this.views.filter(function(view) {
    return !view.name.startsWith(name + '::');
  });
};

GZ3D.MultiView.prototype.getViewByName = function(name) {
  for (var i = 0; i < this.views.length; i = i + 1) {
    if (this.views[i].name === name) {
      return this.views[i];
    }
  }

  return undefined;
};

GZ3D.MultiView.prototype.isViewVisible = function(view) {
  if (view.container) {
    return (
      view.container.style.visibility === 'visible' &&
      view.container.className.indexOf('ng-hide') === -1
    );
  } else {
    return false;
  }
};

GZ3D.MultiView.prototype.setViewContainerElement = function(
  view,
  containerElement
) {
  if (!angular.isDefined(view)) {
    return false;
  }

  if (this.renderMode === GZ3D.RENDER_MODE.DYNAMIC) {
    if (view.masterView) {
      view.container = containerElement;
      view.container.appendChild(view.renderer.domElement);
      view.container.style.visibility = 'visible';
    } else {
      view.container = containerElement;
      view.canvas = document.createElement('canvas');
      view.container.appendChild(view.canvas);
      view.container.style.visibility = 'visible';
    }
  } else if (this.renderMode === GZ3D.RENDER_MODE.MULTIPLE_CONTEXTS) {
    view.container = containerElement;
    view.container.appendChild(view.renderer.domElement);
    view.container.style.visibility = 'visible';
  } else if (
    this.renderMode === GZ3D.RENDER_MODE.OFFSCREEN_AND_COPY_TO_CANVAS
  ) {
    view.container = containerElement;
    view.canvas = document.createElement('canvas');
    view.container.appendChild(view.canvas);
    view.container.style.visibility = 'visible';
  }
};

GZ3D.MultiView.prototype.updateView = function(view) {
  var width = view.container.clientWidth;
  var height = view.container.clientHeight;

  view.canvas.width = width;
  view.canvas.height = height;

  view.camera.aspect = width / height;
  view.camera.updateProjectionMatrix();
  view.camera.cameraHelper.update();

  view.renderer.setSize(width, height);
  view.renderer.setViewport(0, 0, width, height);
  view.renderer.setScissor(0, 0, width, height);
};

GZ3D.MultiView.prototype.updateMasterView = function() {
  // The master view should be the larger canvas

  var maxSize = 0;
  var maxSizeView;
  var masterView;

  for (var i = 0, l = this.views.length; i < l; i = i + 1) {
    var view = this.views[i];

    if (this.isViewVisible(view)) {
      var nPixels = view.canvas.width * view.canvas.height;

      if (view.masterView) {
        masterView = view;
      }

      if (i === 0) {
        maxSize = nPixels;
        maxSizeView = view;
      } else {
        if (nPixels > maxSize) {
          maxSize = nPixels;
          maxSizeView = view;
        }
      }
    }
  }

  if (maxSizeView && !maxSizeView.masterView) {
    // The masterView is not the biggest one anymore, it should be updated

    maxSizeView.masterView = true;
    maxSizeView.container.appendChild(this.renderer.domElement);

    if (masterView) {
      masterView.container.appendChild(maxSizeView.canvas);
      masterView.canvas = maxSizeView.canvas;
      masterView.masterView = false;
    } else {
      maxSizeView.container.removeChild(maxSizeView.canvas);
    }

    maxSizeView.canvas = this.renderer.domElement;
  }
};

GZ3D.MultiView.prototype.renderViews = function() {
  var masterView, oneViewNeedRefresh, i, l, view;

  if (this.renderMode === GZ3D.RENDER_MODE.DYNAMIC) {
    this.updateMasterView();
  }

  for (i = 0, l = this.views.length; i < l; i = i + 1) {
    view = this.views[i];

    if (this.isViewVisible(view)) {
      if (
        view.needsRefresh ||
        view.canvas.width !== view.container.clientWidth ||
        view.canvas.height !== view.container.clientHeight ||
        !view.camera.matrixWorld.equals(view.cameraViewCurrentMatrix)
      ) {
        oneViewNeedRefresh = true;
      }
    }
  }

  if (oneViewNeedRefresh) {
    for (i = 0, l = this.views.length; i < l; i = i + 1) {
      view = this.views[i];

      if (this.isViewVisible(view)) {
        view.needsRefresh = false;
        view.cameraViewCurrentMatrix = view.camera.matrixWorld.clone();

        if (this.renderMode === GZ3D.RENDER_MODE.DYNAMIC) {
          if (!view.masterView) {
            this.updateView(view);
            this.gz3dScene.composer.render(view);

            let context = view.canvas.getContext('2d');
            if (context.canvas.width !== 0 && context.canvas.height !== 0) {
              context.drawImage(
                this.renderer.domElement,
                0,
                0,
                view.canvas.width,
                view.canvas.height,
                0,
                0,
                view.canvas.width,
                view.canvas.height
              );
            }
          } else {
            masterView = view;
          }
        } else {
          this.updateView(view);
          this.gz3dScene.composer.render(view);

          if (
            this.renderMode === GZ3D.RENDER_MODE.OFFSCREEN_AND_COPY_TO_CANVAS
          ) {
            view.canvas
              .getContext('2d')
              .drawImage(this.renderer.domElement, 0, 0);
          }
        }
      }
    }
  }

  if (masterView) {
    this.updateView(masterView);
    this.gz3dScene.composer.render(masterView);
  }

  this.gz3dScene.composer.checkSceneReady();
};

GZ3D.MultiView.prototype.setCameraHelperVisibility = function(view, visible) {
  view.camera.cameraHelper.visible = visible;
};

GZ3D.MultiView.prototype.createRenderer = function() {
  var renderer = new THREE.WebGLRenderer({ antialias: false });
  renderer.setClearColor(0xb2b2b2, 1); // Sky
  renderer.setScissorTest(true);
  renderer.shadowMap.enabled = false;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;

  return renderer;
};

GZ3D.MultiView.prototype.setBackgrounds = function(background) {
  this.views.forEach(function(view, index, array) {
    view.renderer.clear();
    view.renderer.setClearColor(background, 1);
  });
};

GZ3D.MultiView.prototype.setShadowMaps = function(enabled) {
  this.views.forEach(function(view, index, array) {
    view.renderer.shadowMap.enabled = enabled;
  });
};

GZ3D.MultiView.prototype.clearRenderTargets = function(target) {
  this.views.forEach(function(view, index, array) {
    view.renderer.setRenderTarget(target);
    view.renderer.clear();
  });
};
