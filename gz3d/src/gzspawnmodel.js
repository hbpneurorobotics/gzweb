/**
 * Spawn a model into the scene
 * @constructor
 */
GZ3D.SpawnModel = function(scene) {
  this.scene = scene;
  this.init();
  this.obj = undefined;
  this.callback = undefined;
  this.sdfParser = undefined;
};

/**
 * Initialize SpawnModel
 */

GZ3D.SpawnModel.prototype.init = function() {
  this.plane = new THREE.Plane(new THREE.Vector3(0, 0, 1), 0);
  this.ray = new THREE.Ray();
  this.obj = null;
  this.active = false;
  this.autoAlignModel = new GZ3D.AutoAlignModel(this.scene);
};

/**
 * Start spawning an entity.
 * Adds a temp object to the scene which is not registered on the server.
 * @param {string} modelPath
 * @param {function} callback
 */
GZ3D.SpawnModel.prototype.start = function(
  modelPath,
  modelSDF,
  modelName,
  callback,
  position,
  quaternion,
  scale
) {
  if (this.active) {
    this.finish();
  }

  if (!angular.isDefined(this.scene.getDomElement())) {
    console.warn('SpawnModel.start() - no DOM element defined!');
    return;
  }

  this.attachEventListeners();

  this.callback = callback;

  this.obj = new THREE.Object3D();
  var mesh;
  var zoffset = 0.5;

  this.autoAlignModel.start(this.obj);

  if (modelPath === 'box') {
    mesh = this.scene.createBox(1, 1, 1);
    this.obj.userData.shapeName = modelPath;
  } else if (modelPath === 'sphere') {
    mesh = this.scene.createSphere(0.5);
    this.obj.userData.shapeName = modelPath;
  } else if (modelPath === 'cylinder') {
    mesh = this.scene.createCylinder(0.5, 1.0);
    this.obj.userData.shapeName = modelPath;
  } else if (modelPath === 'pointlight') {
    mesh = this.scene.createLight(this.scene.LIGHT_POINT);
  } else if (modelPath === 'spotlight') {
    mesh = this.scene.createLight(this.scene.LIGHT_SPOT);
  } else if (modelPath === 'directionallight') {
    mesh = this.scene.createLight(this.scene.LIGHT_DIRECTIONAL);
  } else {
    mesh = this.sdfParser.loadSDF(modelSDF);
    //TODO: add transparency to the object
    zoffset = 0;
    this.autoAlignModel.alignOnMeshBase();
  }

  this.obj.name = this.generateUniqueName(modelName);
  this.obj.add(mesh);

  var viewWidth = this.scene.getDomElement().clientWidth;
  var viewHeight = this.scene.getDomElement().clientHeight;

  // temp model appears within current view
  var pos = new THREE.Vector2(viewWidth / 2, viewHeight / 2);
  var intersect = new THREE.Vector3();
  this.scene.getRayCastModel(pos, intersect);

  this.obj.position.x = intersect.x;
  this.obj.position.y = intersect.y;
  this.obj.position.z += zoffset;
  this.scene.add(this.obj);

  // For the inserted light to have effect
  var allObjects = [];
  this.scene.scene.getDescendants(allObjects);
  for (var l = 0; l < allObjects.length; ++l) {
    if (allObjects[l].material) {
      allObjects[l].material.needsUpdate = true;
    }
  }

  if (position && quaternion) {
    this.scene.setPose(this.obj, position, quaternion);
  }

  if (scale) {
    this.scene.setScale(this.obj, scale);
  }

  this.active = true;
};

/**
 * Finish spawning an entity: re-enable camera controls,
 * remove listeners, remove temp object
 */
GZ3D.SpawnModel.prototype.finish = function() {
  this.detachEventListeners();

  this.scene.remove(this.obj);
  this.obj = undefined;
  this.active = false;

  this.autoAlignModel.finish();
  this.scene.updateDynamicEnvMap();
};

/**
 * Window event callback
 * @param {} event - not yet
 */
GZ3D.SpawnModel.prototype.onMouseDown = function(event) {
  // Does this ever get called?
  // Change like this:
  // https://bitbucket.org/osrf/gzweb/pull-request/14/switch-to-arrow-mode-when-spawning-models/diff
  event.preventDefault();
  event.stopImmediatePropagation();
};

/**
 * Window event callback
 * @param {} event - mousemove events
 */
GZ3D.SpawnModel.prototype.onMouseMove = function(event) {
  if (!this.active) {
    return;
  }

  event.preventDefault();

  this.moveSpawnedModel(event.offsetX, event.offsetY);
};

/**
 * Window event callback
 * @param {} event - touchmove events
 */
GZ3D.SpawnModel.prototype.onTouchMove = function(event, originalEvent) {
  if (!this.active) {
    return;
  }

  var e;

  if (originalEvent) {
    e = event;
  } else {
    e = event.originalEvent;
  }
  e.preventDefault();

  if (e.touches.length === 1) {
    this.moveSpawnedModel(e.touches[0].pageX, e.touches[0].pageY);
  }
};

/**
 * Window event callback
 * @param {} event - touchend events
 */
GZ3D.SpawnModel.prototype.onTouchEnd = function() {
  if (!this.active) {
    return;
  }

  this.callback(this.obj);
  this.finish();
};

/**
 * Window event callback
 * @param {} event - mousedown events
 */
GZ3D.SpawnModel.prototype.onMouseUp = function(event) {
  if (!this.active) {
    return;
  }

  this.callback(this.obj);
  this.finish();
};

/**
 * Window event callback
 * @param {} event - keyup events
 */
GZ3D.SpawnModel.prototype.onKeyUp = function(event) {
  this.autoAlignModel.onKeyUp(event);
};

/**
 * Window event callback
 * @param {} event - keydown events
 */
GZ3D.SpawnModel.prototype.onKeyDown = function(event) {
  if (event.keyCode === 27) {
    // Esc
    this.finish();
  } else {
    this.autoAlignModel.onKeyDown(event);
  }
};

/**
 * Move temp spawned model
 * @param {integer} positionX - Horizontal position on the canvas
 * @param {integer} positionY - Vertical position on the canvas
 */
GZ3D.SpawnModel.prototype.moveSpawnedModel = function(positionX, positionY) {
  this.autoAlignModel.moveAlignModel(positionX, positionY);
};

/**
 * Generate unique name for spawned entity
 * @param {string} entity - entity type
 */
GZ3D.SpawnModel.prototype.generateUniqueName = function(entity) {
  /*
  The name of the entity is later converted downstream to lower case and has the white spaces replaced by underscores.
  To avoid conflicts happening later, after that normalization is done, we make sure to use the final format
  here for correctly detecting collision with pre-existing ids.
  */
  entity = entity.toLowerCase().replace(/\s/g, '_');
  var i = 0;
  while (i < 1000) {
    if (this.scene.getByName(entity + '_' + i)) {
      ++i;
    } else {
      return entity + '_' + i;
    }
  }
};

/**
 * Attach the event listeners for interaction
 */
GZ3D.SpawnModel.prototype.attachEventListeners = function() {
  var that = this;

  this.mouseDown = function(event) {
    that.onMouseDown(event);
  };
  this.mouseUp = function(event) {
    that.onMouseUp(event);
  };
  this.mouseMove = function(event) {
    that.onMouseMove(event);
  };
  this.keyDown = function(event) {
    that.onKeyDown(event);
  };
  this.keyUp = function(event) {
    that.onKeyUp(event);
  };
  this.touchMove = function(event) {
    that.onTouchMove(event, true);
  };
  this.touchEnd = function(event) {
    that.onTouchEnd(event);
  };

  this.scene
    .getDomElement()
    .addEventListener('mousedown', that.mouseDown, false);
  this.scene.getDomElement().addEventListener('mouseup', that.mouseUp, false);
  this.scene
    .getDomElement()
    .addEventListener('mousemove', that.mouseMove, false);
  window.addEventListener('keydown', that.keyDown, false);
  window.addEventListener('keyup', that.keyUp, false);

  this.scene
    .getDomElement()
    .addEventListener('touchmove', that.touchMove, false);
  this.scene.getDomElement().addEventListener('touchend', that.touchEnd, false);
};

/**
 * Detach the event listeners for interaction
 */
GZ3D.SpawnModel.prototype.detachEventListeners = function() {
  var that = this;

  this.scene
    .getDomElement()
    .removeEventListener('mousedown', that.mouseDown, false);
  this.scene
    .getDomElement()
    .removeEventListener('mouseup', that.mouseUp, false);
  this.scene
    .getDomElement()
    .removeEventListener('mousemove', that.mouseMove, false);
  window.removeEventListener('keydown', that.keyDown, false);
  window.removeEventListener('keyup', that.keyUp, false);
};
