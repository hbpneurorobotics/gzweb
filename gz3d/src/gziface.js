//var GAZEBO_MODEL_DATABASE_URI='http://gazebosim.org/models';

THREE.ImageUtils.crossOrigin = 'anonymous'; // needed to allow cross-origin loading of textures

GZ3D.GZIface = function(scene, gui) {
  this.scene = scene;
  this.gui = gui;

  this.isConnected = false;

  this.emitter = new EventEmitter2({ verbose: true });

  this.init();
  this.visualsToAdd = [];
  this.canDeletePredicates = [];

  // Stores AnimatedModel instances
  this.animatedModels = {};

  this.waitingForScaleNewObjects = [];

  GZ3D.assetProgressData = {};
  GZ3D.assetProgressData.assets = [];
  GZ3D.assetProgressData.prepared = false;
  GZ3D.assetProgressCallback = undefined;

  this.webSocketConnectionCallbacks = [];
  let deleteRobotViewsCallback = function(entity) {
    if (GZ3D.isRobot(entity)) {
      let deleteRobotViews = scene.viewManager.deleteRobotViews.bind(
        scene.viewManager
      );
      deleteRobotViews(entity);
    }
  };
  this.onDeleteEntityCallbacks = [deleteRobotViewsCallback];
  this.onCreateEntityCallbacks = [];
};

GZ3D.GZIface.prototype.addCanDeletePredicate = function(canDeletePredicate) {
  this.canDeletePredicates.push(canDeletePredicate);
};

GZ3D.GZIface.prototype.addOnDeleteEntityCallbacks = function(...args) {
  args.forEach(callback => this.onDeleteEntityCallbacks.push(callback));
};

GZ3D.GZIface.prototype.addOnCreateEntityCallbacks = function(...args) {
  args.forEach(callback => this.onCreateEntityCallbacks.push(callback));
};

GZ3D.GZIface.prototype.init = function() {
  this.material = [];
  this.entityMaterial = {};

  this.connect();
};

GZ3D.GZIface.prototype.setAssetProgressCallback = function(callback) {
  GZ3D.assetProgressCallback = callback;
};

GZ3D.GZIface.prototype.registerWebSocketConnectionCallback = function(
  callback
) {
  this.webSocketConnectionCallbacks.push(callback);
};

GZ3D.GZIface.prototype.connect = function() {
  // connect to websocket
  var url = GZ3D.webSocketUrl;

  if (GZ3D.webSocketToken !== undefined) {
    url = url + '?token=' + GZ3D.webSocketToken;
  }

  this.webSocket = new ROSLIB.PhoenixRos({
    url: url
  });

  var that = this;
  this.webSocket.on('connection', function() {
    that.onConnected();
  });
  this.webSocket.on('error', function() {
    that.onError();
  });
};

GZ3D.GZIface.prototype.onError = function() {
  this.emitter.emit('error');
};

GZ3D.GZIface.prototype.createEntityModifyMessage = function(entity) {
  var matrix = entity.matrixWorld;
  var translation = new THREE.Vector3();
  var quaternion = new THREE.Quaternion();
  var scale = new THREE.Vector3();
  matrix.decompose(translation, quaternion, scale);

  var entityMsg = {
    name: entity.name,
    id: entity.userData.id,
    createEntity: 0,
    position: {
      x: translation.x,
      y: translation.y,
      z: translation.z
    },
    scale: {
      x: scale.x,
      y: scale.y,
      z: scale.z
    },
    orientation: {
      w: quaternion.w,
      x: quaternion.x,
      y: quaternion.y,
      z: quaternion.z
    }
  };
  return entityMsg;
};

GZ3D.GZIface.prototype.onConnected = function() {
  this.heartbeatTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/heartbeat',
    messageType: 'heartbeat'
  });

  var that = this;
  var publishHeartbeat = function() {
    var hearbeatMsg = {
      alive: 1
    };
    that.heartbeatTopic.publish(hearbeatMsg);
  };

  setInterval(publishHeartbeat, 5000);

  // call all the registered callbacks since we are connected now
  this.webSocketConnectionCallbacks.forEach(function(callback) {
    callback();
  });

  this.statusTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/status',
    messageType: 'status'
  });

  var statusUpdate = function(message) {
    if (message.status === 'error') {
      that.isConnected = false;
      this.emitter.emit('gzstatus', 'error');
    }
  };
  this.statusTopic.subscribe(statusUpdate.bind(this));

  this.errorTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/error',
    messageType: 'error'
  });

  var errorUpdate = function (message) {
    // error with id 1 means that opensim faied to load a mesh
    if (message.id === 1) {
      this.emitter.emit('opensimMeshError', message);
    }
    // generic gazebo error handling
    else {
      console.error(`Error received from gazebo: ${message}`);
    }
  };

  this.errorTopic.subscribe(errorUpdate.bind(this));

  this.materialTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/material',
    messageType: 'material'
  });

  var materialUpdate = function(message) {
    this.material = message;
    this.emitter.emit('material', this.material);
  };
  this.materialTopic.subscribe(materialUpdate.bind(this));

  this.sceneTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/scene',
    messageType: 'scene'
  });

  var isChildModel = function(modelList, name, firstlevel = true) {
    for (var i = 0; i < modelList.length; i++) {
      var model = modelList[i];
      if (!firstlevel) {
        if (name === model.name) {
          return true;
        }
      }

      var isChild = isChildModel(model.model, name, false);
      if (isChild) {
        return true;
      }
    }

    return false;
  };

  var createModelHierarchy = function(gzface, parentObject, model) {
    var modelObj = gzface.createModelFromMsg(model);
    if (modelObj) {
      parentObject.add(modelObj);
      gzface.scene.applyComposerSettingsToModel(modelObj);
      gzface.gui.setModelStats(model, 'update');

      for (var i = 0; i < model.model.length; i++) {
        createModelHierarchy(gzface, modelObj, model.model[i]);
      }
    }

    return modelObj;
  };

  var sceneUpdate = function(message) {
    if (message.name) {
      this.scene.name = message.name;
    }

    if (message.grid === true) {
      //this.gui.guiEvents.emit('show_grid', 'show'); // do not show grid by default for now
    }

    if (message.ambient) {
      var ambient = new THREE.Color();
      ambient.r = message.ambient.r;
      ambient.g = message.ambient.g;
      ambient.b = message.ambient.b;

      this.scene.ambient.color = ambient;
    }

    if (message.background) {
      var background = new THREE.Color();
      background.r = message.background.r;
      background.g = message.background.g;
      background.b = message.background.b;

      this.scene.viewManager.setBackgrounds(background);
    }

    for (var i = 0; i < message.light.length; ++i) {
      var light = message.light[i];
      var lightObj = this.createLightFromMsg(light);
      if (lightObj) {
        this.scene.add(lightObj);
        this.gui.setLightStats(light, 'update');
      }
    }

    var j, model;
    var hierachyOnlyModels = [];

    for (j = 0; j < message.model.length; ++j) {
      model = message.model[j];
      if (!isChildModel(message.model, model.name)) {
        hierachyOnlyModels.push(model);
      }
    }

    for (j = 0; j < hierachyOnlyModels.length; ++j) {
      createModelHierarchy(this, this.scene, hierachyOnlyModels[j]);
    }

    GZ3D.assetProgressData.prepared = true;
    if (GZ3D.assetProgressCallback) {
      GZ3D.assetProgressCallback(GZ3D.assetProgressData);
    }
    this.gui.setSceneStats(message);
    this.sceneTopic.unsubscribe();

    this.scene.refresh3DViews();
  };
  this.sceneTopic.subscribe(sceneUpdate.bind(this));

  this.physicsTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/physics',
    messageType: 'physics'
  });

  var physicsUpdate = function(message) {
    this.gui.setPhysicsStats(message);
  };
  this.physicsTopic.subscribe(physicsUpdate.bind(this));

  // Update model pose
  this.poseTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/pose/info',
    messageType: 'pose'
  });

  var poseUpdate = function(message) {
    var poseApplyUpdate = function(scope, message) {
      var entity = scope.scene.getGazeboObject(message);
      if (
        entity &&
        entity !== scope.scene.modelManipulator.object &&
        entity.parent !== scope.scene.modelManipulator.object
      ) {
        scope.scene.updatePose(entity, message.position, message.orientation);
        scope.gui.setModelStats(message, 'update');
      }
    };

    if (message.name) {
      poseApplyUpdate(this, message);
    } else {
      var keys = Object.keys(message);

      for (var i = 0; i < keys.length; i++) {
        poseApplyUpdate(this, message[keys[i]]);
      }
    }
  };

  this.poseTopic.subscribe(poseUpdate.bind(this));

  // ROS topic subscription for joint_state messages
  // Requires gzserver version with support for joint state messages
  this.jointTopicSubscriber = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/joint_states',
    message_type: 'jointstates',
    throttle_rate: 1.0 / 25.0 * 1000.0
  });

  // function for updating transformations of bones in a client-side-only animated model
  var updateJoint = function(message) {
    if (message.robot_name in this.animatedModels) {
      var animatedModel = this.animatedModels[message.robot_name];
      animatedModel.updateJoint(
        message.robot_name,
        message.name,
        message.position,
        message.axes
      );
    }
  };

  // Subscription to joint update topic
  this.jointTopicSubscriber.subscribe(updateJoint.bind(this));

  // Requests - for deleting models
  this.requestTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/request',
    messageType: 'request'
  });

  // ROS topic subscription for muscle system visualization messages
  // Requires gzserver version with OpenSim support, and OpenSim as active physics engine (gzserver -e opensim)
  console.debug('Subscribing to muscle visualization topic (gzbridge)');
  this.muscleVisualizationSubscriber = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/muscles',
    message_type: 'muscles',
    throttle_rate: 1.0 / 20.0 * 1000.0
  });

  // ROS topic subscription for fluid positions visualization messages
  // Requires gzserver version with OpenSim support, and OpenSim as active physics engine (gzserver -e opensim)
  console.debug('Subscribing to fluid visualization topic (gzbridge)');
  this.fluidVisualizationSubscriber = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/fluid_pos',
    message_type: 'fluid',
    throttle_rate: 1.0 / 200.0 * 1000.0
  });

  // function for updating client system visualization
  var updateMuscleVisualization = function(message) {
    if (!(message.robot_name in this.scene.muscleVisuzalizations)) {
      this.scene.muscleVisuzalizations[
        message.robot_name
      ] = new GZ3D.VisualMuscleModel(this.scene, message.robot_name);
    }

    if (message.robot_name in this.scene.muscleVisuzalizations) {
      this.scene.muscleVisuzalizations[message.robot_name].updateVisualization(
        message
      );
    }
  };

  // function for updating client system visualization
  var updateFluidVisualization = function(message) {
    if (!this.scene.fluidVisualizations.length) {
      this.scene.fluidVisualizations.push(
        new GZ3D.VisualFluidModel(this.scene, message)
      );
      this.scene.fluidVisualizations[0].initFluidBuffers(message);
    }
    this.scene.fluidVisualizations[0].updateVisualization(message);
  };

  // Subscription to joint update topic
  this.muscleVisualizationSubscriber.subscribe(
    updateMuscleVisualization.bind(this)
  );

  // Subscription to fluid_pos topic
  this.fluidVisualizationSubscriber.subscribe(
    updateFluidVisualization.bind(this)
  );

  var requestUpdate = function(message) {
    if (message.request === 'entity_delete') {
      var entity = this.scene.getByName(message.data);
      if (entity) {
        if (entity.children[0] instanceof THREE.Light) {
          this.gui.setLightStats({ name: message.data }, 'delete');
          guiEvents.emit('notification_popup', message.data + ' deleted');
        } else {
          this.gui.setModelStats({ name: message.data }, 'delete');
          guiEvents.emit('notification_popup', message.data + ' deleted');
        }
        this.scene.remove(entity);
      }
    }
  };

  this.requestTopic.subscribe(requestUpdate.bind(this));

  // Model info messages - currently used for spawning new models
  this.modelInfoTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/model/info',
    messageType: 'model'
  });

  var modelUpdate = function(message) {
    if (!this.scene.getGazeboObject(message)) {
      var modelObj = createModelHierarchy(this, this.scene, message);
      if (modelObj) {
        guiEvents.emit('notification_popup', message.name + ' inserted');
      }

      // visuals may arrive out of order (before the model msg),
      // add the visual in if we find its parent here
      var len = this.visualsToAdd.length;
      var i = 0;
      var j = 0;
      while (i < len) {
        var parentName = this.visualsToAdd[j].parent_name;
        if (parentName.indexOf(modelObj.name) >= 0) {
          var parent = this.scene.getByName(parentName);
          var visualObj = this.createVisualFromMsg(this.visualsToAdd[j]);
          parent.add(visualObj);
          this.visualsToAdd.splice(j, 1);
        } else {
          j++;
        }
        i++;
      }
      this.onCreateEntityCallbacks.forEach(function(callback) {
        callback(modelObj);
      });
    } else {
      this.updateModelFromMsg(this.scene.getGazeboObject(message), message);
    }
    this.gui.setModelStats(message, 'update');
  };

  this.modelInfoTopic.subscribe(modelUpdate.bind(this));

  // Visual messages - currently just used for collision visuals
  this.visualTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/visual',
    messageType: 'visual'
  });

  var visualUpdate = function(message) {
    if (!this.scene.getByName(message.name)) {
      // accept only collision visual msgs for now
      if (message.name.indexOf('COLLISION_VISUAL') < 0) {
        return;
      }

      // delay the add if parent not found, this array will checked in
      // modelUpdate function
      var parent = this.scene.getByName(message.parent_name);
      if (message.parent_name && !parent) {
        this.visualsToAdd.push(message);
      } else {
        var visualObj = this.createVisualFromMsg(message);
        parent.add(visualObj);
      }
    } else {
      // backend generated material updates for objects in the scene
      // these are not a user actions, so update the visuals
      var backendVisualObj = this.scene.getByName(message.name);
      if (message.material && message.material.script.name !== '__default__') {
        this.updateVisualFromMsg(backendVisualObj, message);
      }
    }
  };

  this.visualTopic.subscribe(visualUpdate.bind(this));

  // world stats
  this.worldStatsTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/world_stats',
    messageType: 'world_stats'
  });

  var worldStatsUpdate = function(message) {
    this.updateStatsGuiFromMsg(message);
  };

  this.worldStatsTopic.subscribe(worldStatsUpdate.bind(this));

  // Spawn new lights
  this.lightFactoryTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/factory/light',
    messageType: 'light'
  });

  var lightCreate = function(message) {
    var entity = this.scene.getByName(message.name);
    if (!entity) {
      var lightObj = this.createLightFromMsg(message);
      if (lightObj) {
        this.scene.add(lightObj);

        // For the inserted light to have effect
        var allObjects = [];
        this.scene.scene.getDescendants(allObjects);
        for (var l = 0; l < allObjects.length; ++l) {
          if (allObjects[l].material) {
            allObjects[l].material.needsUpdate = true;
          }
        }

        guiEvents.emit('notification_popup', message.name + ' inserted');
      }
    }
    this.gui.setLightStats(message, 'update');
  };

  this.lightFactoryTopic.subscribe(lightCreate.bind(this));

  // Update existing lights
  var lightModifyTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/light/modify',
    messageType: 'light'
  });

  var lightUpdate = function(message) {
    var entity = this.scene.getByName(message.name);
    if (
      entity &&
      entity !== this.scene.modelManipulator.object &&
      entity.parent !== this.scene.modelManipulator.object
    ) {
      this.scene.updateLight(entity, message);
    }
    this.gui.setLightStats(message, 'update');
  };

  lightModifyTopic.subscribe(lightUpdate.bind(this));

  // heightmap
  this.heightmapDataService = new ROSLIB.Service({
    ros: this.webSocket,
    name: '~/heightmap_data',
    serviceType: 'heightmap_data'
  });

  // road
  this.roadService = new ROSLIB.Service({
    ros: this.webSocket,
    name: '~/roads',
    serviceType: 'roads'
  });

  var request = new ROSLIB.ServiceRequest({
    name: 'roads'
  });

  // send service request and load road on response
  this.roadService.callService(request, function(result) {
    var roadsObj = that.createRoadsFromMsg(result);
    this.scene.add(roadsObj);
  });

  // Model modify messages - for modifying models
  this.modelModifyTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/model/modify',
    messageType: 'model'
  });

  // Light messages - for modifying lights
  this.lightModifyTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/light/modify',
    messageType: 'light'
  });

  /*
  TODO: (Sandro Weber)
  The following functions are used to change all lights at the same time through the light slider of the NRP.
  Gazebo only knows attenuation factors, not intensity, so we manipulate diffuse color for now.
  Probably replaced / revamped after a dedicated edit tab is introduced to allow manipulation of lights directly.
   */
  var createEntityModifyMessageWithLight = function(entity, diffuse) {
    var entityMsg = that.createEntityModifyMessage(entity);

    var lightObj = entity.children[0];

    if (diffuse === undefined) {
      diffuse = lightObj.color;
    }
    entityMsg.diffuse = {
      r: diffuse.r,
      g: diffuse.g,
      b: diffuse.b
    };
    entityMsg.specular = {
      r: entity.serverProperties.specular.r,
      g: entity.serverProperties.specular.g,
      b: entity.serverProperties.specular.b
    };

    entityMsg.direction = entity.direction;
    entityMsg.range = lightObj.distance;

    if (entityMsg.direction === undefined && lightObj.target !== undefined) {
      entityMsg.direction = {
        x: lightObj.target.position.x,
        y: lightObj.target.position.y,
        z: lightObj.target.position.z
      };
    }

    entityMsg.attenuation_constant =
      entity.serverProperties.attenuation_constant;
    entityMsg.attenuation_linear = entity.serverProperties.attenuation_linear;
    entityMsg.attenuation_quadratic =
      entity.serverProperties.attenuation_quadratic;

    return entityMsg;
  };

  var publishEntityModify = function(entity) {
    if (entity) {
      var lightObj = entity.children ? entity.children[0] : null;
      if (lightObj && lightObj instanceof THREE.Light) {
        that.lightModifyTopic.publish(
          createEntityModifyMessageWithLight(entity, undefined)
        );
      } else {
        that.modelModifyTopic.publish(that.createEntityModifyMessage(entity));
      }
    }
  };

  this.scene.emitter.on('entityChanged', publishEntityModify);

  var publishLightModify = function(vec) {
    var lights = [];
    that.scene.scene.traverse(function(node) {
      if (node instanceof THREE.Light) {
        lights.push(node);
      }
    });

    var numberOfLights = lights.length;
    for (var i = 0; i < numberOfLights; i += 1) {
      if (lights[i] instanceof THREE.AmbientLight) {
        // we don't change ambient lights
        continue;
      }
      var entity = that.scene.getByName(lights[i].name);
      lights[i].color.r = THREE.Math.clamp(vec + lights[i].color.r, 0, 1);
      lights[i].color.g = THREE.Math.clamp(vec + lights[i].color.g, 0, 1);
      lights[i].color.b = THREE.Math.clamp(vec + lights[i].color.b, 0, 1);

      that.lightModifyTopic.publish(
        createEntityModifyMessageWithLight(entity, lights[i].color)
      );
    }
  };

  this.scene.emitter.on('lightChanged', publishLightModify);
  /*
  end of light slider change functions
   */

  var publishLinkModify = function(entity, type) {
    var modelMsg = {
      name: entity.parent.name,
      id: entity.parent.userData.id,
      link: {
        name: entity.name,
        id: entity.userData.id,
        self_collide: entity.serverProperties.self_collide,
        gravity: entity.serverProperties.gravity,
        kinematic: entity.serverProperties.kinematic
      }
    };

    that.linkModifyTopic.publish(modelMsg);
  };

  this.scene.emitter.on('linkChanged', publishLinkModify);

  // Factory messages - for spawning new models
  this.factoryTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/factory',
    messageType: 'factory'
  });

  // Factory messages - for spawning new lights
  this.lightFactoryTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/factory/light',
    messageType: 'light'
  });

  var publishFactory = function(model, type) {
    var matrix = model.matrixWorld;
    var translation = new THREE.Vector3();
    var quaternion = new THREE.Quaternion();
    var scale = new THREE.Vector3();
    matrix.decompose(translation, quaternion, scale);
    var entityMsg = {
      name: model.name,
      type: type,
      createEntity: 1,
      position: {
        x: translation.x,
        y: translation.y,
        z: translation.z
      },
      scale: {
        x: scale.x,
        y: scale.y,
        z: scale.z
      },
      orientation: {
        w: quaternion.w,
        x: quaternion.x,
        y: quaternion.y,
        z: quaternion.z
      }
    };
    if (
      model.children[0] &&
      model.children[0].children[0] &&
      model.children[0].children[0] instanceof THREE.Light
    ) {
      that.lightFactoryTopic.publish(entityMsg);
    } else {
      that.factoryTopic.publish(entityMsg);

      if (scale.x !== 1.0 || scale.y !== 1.0 || scale.z !== 1.0) {
        // The factory topic does not handle the scale value, so I have to publish a modify message
        // to apply the scale transform. I'll do it later, since I don't have the ID of the new created object now
        that.waitingForScaleNewObjects[model.name] = [
          scale.x,
          scale.y,
          scale.z
        ];
      }
    }
  };

  // For deleting models
  this.deleteTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/entity_delete',
    messageType: 'entity_delete'
  });

  var publishDeleteEntity = function(entity) {
    var canDelete =
      !that.canDeletePredicates.length ||
      that.canDeletePredicates.some(function(deletePredicate) {
        return deletePredicate(entity) !== false;
      });
    if (canDelete) {
      that.onDeleteEntityCallbacks.forEach(function(callback) {
        callback(entity);
      });
      that.deleteTopic.publish({ name: entity.name });
    }
  };

  this.gui.emitter.on('deleteEntity', function(entity) {
    publishDeleteEntity(entity);
  });

  // World control messages - for resetting world/models
  this.worldControlTopic = new ROSLIB.Topic({
    ros: this.webSocket,
    name: '~/world_control',
    messageType: 'world_control'
  });

  var publishWorldControl = function(state, resetType) {
    var worldControlMsg = {};
    if (state !== null) {
      worldControlMsg.pause = state;
    }
    if (resetType) {
      worldControlMsg.reset = resetType;
    }
    that.worldControlTopic.publish(worldControlMsg);
  };

  this.gui.emitter.on('entityCreated', publishFactory);

  this.gui.emitter.on('reset', function(resetType) {
    publishWorldControl(null, resetType);
  });

  this.gui.emitter.on('pause', function(paused) {
    publishWorldControl(paused, null);
  });

  this.isConnected = true;
  this.emitter.emit('connection');
};

GZ3D.GZIface.prototype.updateStatsGuiFromMsg = function(stats) {

  var simSec = stats.sim_time.sec;
  var simNSec = stats.sim_time.nsec;

  var simDay = Math.floor(simSec / 86400);
  simSec -= simDay * 86400;

  var simHour = Math.floor(simSec / 3600);
  simSec -= simHour * 3600;

  var simMin = Math.floor(simSec / 60);
  simSec -= simMin * 60;

  var simMsec = Math.floor(simNSec * 1e-6);

  var realSec = stats.real_time.sec;
  var realNSec = stats.real_time.nsec;

  var realDay = Math.floor(realSec / 86400);
  realSec -= realDay * 86400;

  var realHour = Math.floor(realSec / 3600);
  realSec -= realHour * 3600;

  var realMin = Math.floor(realSec / 60);
  realSec -= realMin * 60;

  var realMsec = Math.floor(realNSec * 1e-6);

  var simTimeValue = '';
  var realTimeValue = '';

  if (realDay < 10) {
    realTimeValue += '0';
  }
  realTimeValue += realDay.toFixed(0) + ' ';
  if (realHour < 10) {
    realTimeValue += '0';
  }
  realTimeValue += realHour.toFixed(0) + ':';
  if (realMin < 10) {
    realTimeValue += '0';
  }
  realTimeValue += realMin.toFixed(0) + ':';
  if (realSec < 10) {
    realTimeValue += '0';
  }
  realTimeValue += realSec.toFixed(0);

  if (simDay < 10) {
    simTimeValue += '0';
  }
  simTimeValue += simDay.toFixed(0) + ' ';
  if (simHour < 10) {
    simTimeValue += '0';
  }
  simTimeValue += simHour.toFixed(0) + ':';
  if (simMin < 10) {
    simTimeValue += '0';
  }
  simTimeValue += simMin.toFixed(0) + ':';
  if (simSec < 10) {
    simTimeValue += '0';
  }
  simTimeValue += simSec.toFixed(0);
};

var getShapeName = function(object3D) {
  var getObject3DGeometryType = function() {
    var geometryType = null;

    //object::link::visual::Mesh
    object3D.traverse(function(node) {
      if (!geometryType && node.name.indexOf('link::visual') >= 0) {
        //current node is a link::visual node, test the mesh geometry
        node.traverse(function(subnode) {
          if (subnode instanceof THREE.Mesh) {
            geometryType = subnode.geometry;
          }
        });
      }
    });

    return geometryType;
  };

  var shapeName = 'complex';

  var geomType = getObject3DGeometryType();

  if (geomType instanceof THREE.BoxGeometry) {
    shapeName = 'box';
  } else if (geomType instanceof THREE.CylinderGeometry) {
    shapeName = 'cylinder';
  } else if (geomType instanceof THREE.SphereGeometry) {
    shapeName = 'sphere';
  }

  return shapeName;
};

GZ3D.GZIface.prototype.loadCollisionVisuals = function(object) {
  if (object._pendingCollisionVisuals) {
    for (var i = 0; i < object._pendingCollisionVisuals.length; i++) {
      var collDef = object._pendingCollisionVisuals[i];

      var collisionVisualObj = this.createVisualFromMsg(
        collDef.collisionVisual,
        collDef.modelScale
      );
      if (collisionVisualObj && !collisionVisualObj.parent) {
        object.add(collisionVisualObj);
      }
    }

    object._pendingCollisionVisuals = undefined;
  }
};

GZ3D.GZIface.prototype.createModelFromMsg = function(model) {
  if (this.scene.getGazeboObject(model)) {
    console.error(
      'GZIface.createModelFromMsg() - model ' + model.name + ' already exists'
    );
    return;
  }

  var modelObj = new THREE.Object3D();
  modelObj.name = model.name;
  modelObj.userData.id = model.id;
  modelObj.userData.gazeboType = 'model';
  modelObj.userData.is_static = model.is_static;
  if (model.pose) {
    this.scene.setPose(modelObj, model.pose.position, model.pose.orientation);
  }

  // Check for client-side-only animated model for robot
  var animatedModel = new GZ3D.AnimatedModel(this.scene);
  if (GZ3D.isRobot(model) && GZ3D.animatedModel) {
    animatedModel.loadAnimatedModel(model.name);
    this.animatedModels[model.name] = animatedModel;
  } else {
    animatedModel = null;
  }

  for (var j = 0; j < model.link.length; ++j) {
    var link = model.link[j];
    var linkObj = new THREE.Object3D();
    linkObj.name = link.name;
    linkObj.userData.id = link.id;
    linkObj.userData.gazeboType = 'link';
    linkObj.serverProperties = {
      self_collide: link.self_collide,
      gravity: link.gravity,
      kinematic: link.kinematic
    };

    if (link.pose) {
      this.scene.setPose(linkObj, link.pose.position, link.pose.orientation);
    }

    linkObj._linkSource = link;
    modelObj.add(linkObj);

    // only load individual link visuals if they are not replaced by an createVisualFromMsganimated model
    if (animatedModel === null) {
      for (var k = 0; k < link.visual.length; ++k) {
        var visual = link.visual[k];
        var visualObj = this.createVisualFromMsg(visual, model.scale);
        if (visualObj && !visualObj.parent) {
          linkObj.add(visualObj);
        }
      }

      for (var l = 0; l < link.collision.length; ++l) {
        var collision = link.collision[l];
        for (var m = 0; m < link.collision[l].visual.length; ++m) {
          var collisionVisual = link.collision[l].visual[m];

          if (!linkObj._pendingCollisionVisuals) {
            linkObj._pendingCollisionVisuals = [];
          }

          linkObj._pendingCollisionVisuals.push({
            collisionVisual: collisionVisual,
            modelScale: model.scale
          });
        }
      }
    }

    // always add sensor links, even with animated models (e.g. cameras)
    for (var i = 0; i < link.sensor.length; ++i) {
      var sensor = link.sensor[i];

      var sensorObj = this.createSensorFromMsg(
        sensor,
        link.name,
        '/' + modelObj.name
      );
      if (sensorObj && !sensorObj.parent) {
        linkObj.add(sensorObj);
      }
    }
  }
  if (model.joint) {
    modelObj.joint = model.joint;
  }

  if (model.scale) {
    this.scene.setScale(modelObj, model.scale);
  }

  var shapeName = getShapeName(modelObj);

  modelObj.userData.shapeName = shapeName;
  modelObj.userData.isSimpleShape = shapeName !== 'complex';
  modelObj.getShapeName = function() {
    return this.userData.shapeName;
  };
  modelObj.isSimpleShape = function() {
    return this.userData.isSimpleShape;
  };

  if (this.waitingForScaleNewObjects[model.name]) {
    modelObj.scale.x = this.waitingForScaleNewObjects[model.name][0];
    modelObj.scale.y = this.waitingForScaleNewObjects[model.name][1];
    modelObj.scale.z = this.waitingForScaleNewObjects[model.name][2];
    modelObj.updateMatrixWorld();
    this.modelModifyTopic.publish(this.createEntityModifyMessage(modelObj));

    delete this.waitingForScaleNewObjects[model.name];
  }

  return modelObj;
};

// This method uses code also to be found at GZ3D.GZIface.prototype.createModelFromMsg.
// Currently not everything is handled for an update, but this method was introduced to handle
// the updates of colors of objects; if there should be more functionality one could consider
// merging the two methods and extracting the different things to parameters (or any other means
// of configuration).
GZ3D.GZIface.prototype.updateModelFromMsg = function(modelObj, modelMsg) {
  if (modelMsg.scale) {
    this.scene.setScale(modelObj, modelMsg.scale);
  }

  for (var j = 0; j < modelMsg.link.length; ++j) {
    var link = modelMsg.link[j];
    var linkObj = modelObj.children[j];

    for (var k = 0; k < link.visual.length; ++k) {
      var visual = link.visual[k];
      var visualObj = linkObj.getObjectByName(visual.name);
      this.updateVisualFromMsg(visualObj, visual);
    }
    //update view mode, possibly overwritten by visual/material update
    this.scene.setViewAs(modelObj, modelObj.viewAs);
  }
};

GZ3D.GZIface.prototype.createVisualFromMsg = function(visual, modelScale) {
  if (visual.geometry) {
    var geom = visual.geometry;
    var visualObj = new THREE.Object3D();
    visualObj.name = visual.name;

    if (visual.pose) {
      this.scene.setPose(
        visualObj,
        visual.pose.position,
        visual.pose.orientation
      );
    }

    visualObj.userData.id = visual.id;
    visualObj.userData.gazeboType = 'visual';

    visualObj.castShadow = visual.cast_shadows;
    visualObj.receiveShadow = visual.receive_shadows;

    this.createGeom(geom, visual.material, visualObj, modelScale);

    return visualObj;
  }
};

GZ3D.GZIface.prototype.updateVisualFromMsg = function(visualObj, visual) {
  if (visual.geometry) {
    var obj = visualObj.children[0];
    var mat = this.parseMaterial(visual.material);

    if (obj && mat) {
      this.scene.setMaterial(obj, mat);
    }
  }
};

GZ3D.GZIface.prototype.createLightFromMsg = function(light) {
  var obj, range, direction;

  if (light.type === this.scene.LIGHT_POINT) {
    direction = null;
    range = light.range;
  } else if (light.type === this.scene.LIGHT_SPOT) {
    direction = new THREE.Vector3();
    direction.x = light.direction.x;
    direction.y = light.direction.y;
    direction.z = light.direction.z;
    range = light.range;
  } else if (light.type === this.scene.LIGHT_DIRECTIONAL) {
    direction = new THREE.Vector3();
    direction.x = light.direction.x;
    direction.y = light.direction.y;
    direction.z = light.direction.z;
    range = null;
  }

  // For now I ignore this kind of problematic lights, since they
  // cause the scene to be completely gray. This kind
  // of messages are happening only when a playback
  // is occuring with a __default__ light name.
  // See NRRPLT-7234.

  if (
    light.name === '__default__' &&
    (!light.attenuation_linear || !light.attenuation_quadratic)
  )
    return;

  // equation taken from
  // http://wiki.blender.org/index.php/Doc:2.6/Manual/Lighting/Lights/Light_Attenuation
  var E = 1;
  var D = 1;
  var r = 1;
  var L = light.attenuation_linear;
  var Q = light.attenuation_quadratic;
  var intensity =
    E *
    (D / (D + L * r)) *
    (Math.pow(D, 2) / (Math.pow(D, 2) + Q * Math.pow(r, 2)));

  obj = this.scene.createLight(
    light.type,
    light.diffuse,
    intensity,
    light.pose,
    range,
    light.cast_shadows,
    light.name,
    direction,
    light.specular,
    light.attenuation_constant,
    light.attenuation_linear,
    light.attenuation_quadratic,
    light.spot_outer_angle,
    light.spot_falloff
  );

  return obj;
};

GZ3D.GZIface.prototype.createRoadsFromMsg = function(roads) {
  var roadObj = new THREE.Object3D();

  var mat = this.material['Gazebo/Road'];
  var texture = null;
  if (mat) {
    texture = this.parseUri('media/materials/textures/' + mat['texture']);
  }
  var obj = this.scene.createRoads(roads.point, roads.width, texture);
  roadObj.add(obj);
  return roadObj;
};

GZ3D.GZIface.prototype.createLinkInfo = function(link, parent) {
  this.createLabel(
    parent,
    new THREE.Vector3(0, 0, 0),
    link.name.replace('robot::', ''),
    true
  );
};

GZ3D.GZIface.prototype.createSensorFromMsg = function(
  sensor,
  modelName,
  topicPrefix = undefined
) {
  var sensorObj = new THREE.Object3D();
  sensorObj.name = sensor.name;
  sensorObj.userData.gazeboType = 'sensor';
  sensorObj.userData.sensorType = sensor.type;
  sensorObj._sensorSource = sensor;
  sensorObj.userData.id = sensor.id;

  if (sensor.pose) {
    this.scene.setPose(
      sensorObj,
      sensor.pose.position,
      sensor.pose.orientation
    );
  }

  if (sensor.type === 'camera') {
    // If we have a camera sensor we have a potential view that could be rendered
    var camera = sensor.camera;

    // The following camera parameters are available only for Gazebo versions >= 6.5 (imageSize exists but always 0x0)

    // set to default values if not available
    // If no rendering is available, image_size is set to { x: 0.0, y: 0.0 }, see
    // https://bitbucket.org/osrf/gazebo/issues/1663/sensor-camera-elements-from-sdf-not-being
    if (
      !angular.isDefined(camera.image_size) ||
      (camera.image_size.x === 0.0 && camera.image_size.y === 0.0)
    ) {
      camera.image_size = {};
      camera.image_size.x = 960; // width
      camera.image_size.y = 600; // height
    }
    if (!angular.isDefined(camera.horizontal_fov)) {
      camera.horizontal_fov = Math.PI * 0.3;
    }
    if (!angular.isDefined(camera.near_clip)) {
      camera.near_clip = 0.1;
    }
    if (!angular.isDefined(camera.far_clip)) {
      camera.far_clip = 100.0;
    }

    var aspectRatio = camera.image_size.x / camera.image_size.y;

    // FOV: THREE uses 1) vertical instead of horizontal FOV, 2) degree instead of radians,
    // FOV conversion taken from: http://wiki.panotools.org/Field_of_View
    // aspect ratio inverted because it is given in sensor.camera as width/height
    var vFOV =
      2 *
      Math.atan(Math.tan(camera.horizontal_fov / 2.0) * (1.0 / aspectRatio));
    var camFOV = THREE.Math.radToDeg(vFOV);
    var camNear = camera.near_clip;
    var camFar = camera.far_clip;

    var cameraParams = {
      width: camera.image_size.x,
      height: camera.image_size.y,
      aspectRatio: aspectRatio,
      fov: camFOV,
      near: camNear,
      far: camFar
    };

    var viewManager = this.scene.viewManager;
    var viewName = modelName + ' (' + sensor.name + ')';
    var view = viewManager.createView(viewName, cameraParams);
    if (!view) {
      console.error(
        'GZ3D.GZIface.createSensorFromMsg() - failed to create view ' + viewName
      );
      return;
    }
    view.type = sensor.type;
    view.topic = topicPrefix ? topicPrefix + sensor.topic : sensor.topic;

    sensorObj.add(view.camera);
  }

  return sensorObj;
};

GZ3D.GZIface.prototype.parseUri = function(uri) {
  var uriPath = GZ3D.assetsPath;
  var idx = uri.indexOf('://');
  if (idx > 0) {
    idx += 3;
  }
  return uriPath + '/' + uri.substring(idx);
};

GZ3D.GZIface.prototype.createGeom = function(
  geom,
  material,
  parent,
  modelScale
) {
  var obj;
  var uriPath = GZ3D.assetsPath;
  var that = this;
  var mat = this.parseMaterial(material);
  var defaultMat = {
    diffuse: [0.7, 0.7, 0.7],
    flatShading: THREE.SmoothShading
  };

  //geometries' sizes in scene messages are scaled, must un-scale them first
  //cfr. physics::Link::UpdateVisualGeomSDF
  if (geom.box) {
    var unScaledSize = new THREE.Vector3()
      .copy(geom.box.size)
      .divide(modelScale);
    obj = this.scene.createBox(unScaledSize.x, unScaledSize.y, unScaledSize.z);
    if (!mat) {
      mat = defaultMat;
    }
  } else if (geom.cylinder) {
    var unscaledRadiusCylinder =
      geom.cylinder.radius / Math.max(modelScale.x, modelScale.y);
    var unScaledLength = geom.cylinder.length / modelScale.z;
    obj = this.scene.createCylinder(unscaledRadiusCylinder, unScaledLength);
    if (!mat) {
      mat = defaultMat;
    }
  } else if (geom.sphere) {
    var unscaledRadiusSphere =
      geom.sphere.radius / Math.max(modelScale.x, modelScale.y, modelScale.z);
    obj = this.scene.createSphere(unscaledRadiusSphere);
    if (!mat) {
      mat = defaultMat;
    }
  } else if (geom.plane) {
    obj = this.scene.createPlane(
      geom.plane.normal.x,
      geom.plane.normal.y,
      geom.plane.normal.z,
      geom.plane.size.x,
      geom.plane.size.y
    );
    if (!mat) {
      mat = defaultMat;
    }
  } else if (geom.mesh) {
    // get model name which the mesh is in
    var rootModel = parent;
    while (rootModel.parent) {
      rootModel = rootModel.parent;
    }

    var isCollModel = rootModel.name.indexOf('COLLISION_VISUAL') >= 0;

    // find model from database, download the mesh if it exists
    // var manifestXML;
    // var manifestURI = GAZEBO_MODEL_DATABASE_URI + '/manifest.xml';
    // var request = new XMLHttpRequest();
    // request.open('GET', manifestURI, false);
    // request.onreadystatechange = function(){
    //   if (request.readyState === 4)
    //   {
    //     if (request.status === 200 || request.status === 0)
    //     {
    //         manifestXML = request.responseXML;
    //     }
    //   }
    // };
    // request.send();

    // var uriPath;
    // var modelAvailable = false;
    // var modelsElem = manifestXML.getElementsByTagName('models')[0];
    // var i;
    // for (i = 0; i < modelsElem.getElementsByTagName('uri').length; ++i)
    // {
    //   var uri = modelsElem.getElementsByTagName('uri')[i];
    //   var model = uri.substring(uri.indexOf('://') + 3);
    //   if (model === rootModel)
    //   {
    //     modelAvailable = true;
    //   }
    // }

    // if (modelAvailable)
    {
      var meshUri = geom.mesh.filename;
      var submesh = geom.mesh.submesh;
      var centerSubmesh = geom.mesh.center_submesh;

      var uriType = meshUri.substring(0, meshUri.indexOf('://'));
      if (uriType === 'file' || uriType === 'model') {
        var modelName = meshUri.substring(meshUri.indexOf('://') + 3);
        if (geom.mesh.scale) {
          parent.scale.x = geom.mesh.scale.x;
          parent.scale.y = geom.mesh.scale.y;
          parent.scale.z = geom.mesh.scale.z;
        }

        var modelUri = uriPath + '/' + modelName;
        var modelCacheKey = modelUri;

        if (isCollModel) {
          modelCacheKey += '__COLLISION_VISUAL__';
        }

        if (modelCacheKey in this.scene.cachedModels) {
          var cachedData = this.scene.cachedModels[modelCacheKey];

          if (cachedData.referenceObject) {
            parent.add(cachedData.referenceObject.clone());
            loadGeom(parent);
          } else {
            cachedData.objects.push(parent);
          }
        } else {
          this.scene.cachedModels[modelCacheKey] = {
            referenceObject: null,
            objects: [parent]
          };

          // Progress update: Add this asset to the assetProgressArray, always do this before async call
          // for dae-s below so that asset list is the proper length for splash progress without blocking
          var element = {};
          element.id = parent.name;
          element.url = modelUri;
          element.progress = 0;
          element.totalSize = 0;
          element.done = false;
          GZ3D.assetProgressData.assets.push(element);

          // HBP Change (Luc): we assume that all requested collada files are
          // are web-compliant
          if (modelUri.substr(-4).toLowerCase() === '.dae') {
            var materialName = parent.name + '::' + element.url;

            that.scene.loadMesh(
              element.url,
              submesh,
              centerSubmesh,
              function(dae) {
                let colladaDefinesMaterials = false;
                dae.traverse(node => {
                  if (
                    node.type === 'Mesh' &&
                    typeof node.material !== 'undefined'
                  ) {
                    colladaDefinesMaterials = true;
                  }
                });

                if (mat) {
                  that.entityMaterial[materialName] = mat;
                } else if (!colladaDefinesMaterials) {
                  console.error('GZIface.createGeom() - material definition missing for ' + meshUri + '!'
                   + ' Neither parsed material nor mesh material defined.');
                }

                if (that.entityMaterial[materialName]) {
                  var allChildren = [];
                  dae.getDescendants(allChildren);
                  for (var c = 0; c < allChildren.length; ++c) {
                    if (allChildren[c] instanceof THREE.Mesh) {
                      that.scene.setMaterial(
                        allChildren[c],
                        that.entityMaterial[materialName]
                      );
                    }
                  }
                }

                cachedData = that.scene.cachedModels[modelCacheKey];
                cachedData.referenceObject = dae;
                for (var i = 0; i < cachedData.objects.length; i++) {
                  cachedData.objects[i].add(i === 0 ? dae : dae.clone());
                  loadGeom(cachedData.objects[i]);
                }

                // Progress update: execute callback
                element.done = true;
                if (GZ3D.assetProgressCallback) {
                  GZ3D.assetProgressCallback(GZ3D.assetProgressData);
                }
              },
              function(progress) {
                element.progress = progress.loaded;
                element.totalSize = progress.total;
                element.error = progress.error;
                if (GZ3D.assetProgressCallback) {
                  GZ3D.assetProgressCallback(GZ3D.assetProgressData);
                }
              }
            );
          } else {
            // non-dae mesh, not supported by the mesh pipeline, log and display a loading error
            console.error('Unsupported model mesh, non dae file: ' + modelUri);
            element.error = true;
            if (GZ3D.assetProgressCallback) {
              GZ3D.assetProgressCallback(GZ3D.assetProgressData);
            }
          }
        }
      }
    }
  } else if (geom.heightmap) {
    var request = new ROSLIB.ServiceRequest({
      name: that.scene.name
    });

    // redirect the texture paths to the assets dir
    var textures = geom.heightmap.texture;
    for (var k = 0; k < textures.length; ++k) {
      textures[k].diffuse = this.parseUri(textures[k].diffuse);
      textures[k].normal = this.parseUri(textures[k].normal);
    }

    var sizes = geom.heightmap.size;

    // send service request and load heightmap on response
    this.heightmapDataService.callService(request, function(result) {
      var heightmap = result.heightmap;
      // gazebo heightmap is always square shaped,
      // and a dimension of: 2^N + 1
      that.scene.loadHeightmap(
        heightmap.heights,
        heightmap.size.x,
        heightmap.size.y,
        heightmap.width,
        heightmap.height,
        heightmap.origin,
        textures,
        geom.heightmap.blend,
        parent
      );
      //console.log('Result for service call on ' + result);
    });

    //this.scene.loadHeightmap(parent)
  }

  if (obj) {
    if (mat) {
      // texture mapping for simple shapes and planes only,
      // not used by mesh and terrain
      this.scene.setMaterial(obj, mat);
    }
    obj.updateMatrix();
    parent.add(obj);
    loadGeom(parent);
  }

  function loadGeom(visualObj) {
    var allChildren = [];
    visualObj.getDescendants(allChildren);
    for (var c = 0; c < allChildren.length; ++c) {
      if (allChildren[c] instanceof THREE.Mesh) {
        allChildren[c].castShadow = visualObj.castShadow === undefined;
        allChildren[c].receiveShadow = visualObj.receiveShadow === undefined;

        if (visualObj.castShadow) {
          allChildren[c].castShadow = visualObj.castShadow;
        }
        if (visualObj.receiveShadow) {
          allChildren[c].receiveShadow = visualObj.receiveShadow;
        }

        if (
          visualObj.name !== undefined &&
          visualObj.name.indexOf('COLLISION_VISUAL') >= 0
        ) {
          allChildren[c].castShadow = false;
          allChildren[c].receiveShadow = false;

          allChildren[c].visible =
            that.scene.showCollisions || visualObj.visible;
        }
      }
    }

    that.scene.applyComposerSettingsToModel(visualObj);
  }
};

GZ3D.GZIface.prototype.applyMaterial = function(obj, mat) {
  if (obj) {
    if (mat) {
      obj.material = new THREE.MeshPhongMaterial();
      var emissive = mat.emissive;
      if (emissive) {
        obj.material.emissive.setRGB(emissive[0], emissive[1], emissive[2]);
      }
      var diffuse = mat.diffuse;
      if (diffuse) {
        obj.material.color.setRGB(diffuse[0], diffuse[1], diffuse[2]);
      }
      var specular = mat.specular;
      if (specular) {
        obj.material.specular.setRGB(specular[0], specular[1], specular[2]);
      }
      var opacity = mat.opacity;
      if (opacity) {
        if (opacity < 1) {
          obj.material.transparent = true;
          obj.material.opacity = opacity;
        }
      }

      if (mat.texture) {
        obj.material.map = THREE.ImageUtils.loadTexture(mat.texture);
      }
      if (mat.normalMap) {
        obj.material.normalMap = THREE.ImageUtils.loadTexture(mat.normalMap);
      }
    }
  }
};

GZ3D.GZIface.prototype.parseMaterial = function(material) {
  if (!material) {
    return null;
  }

  var uriPath = GZ3D.assetsPath; //'assets';
  var texture;
  var normalMap;
  var textureUri;
  var ambient;
  var diffuse;
  var emissive;
  var specular;
  var opacity;
  var scale;
  var mat;

  function setMatRGB(dest, src) {
    if (src) {
      return [src.r, src.g, src.b, src.a];
    }
    if (dest) {
      return dest;
    }
  }

  // get texture from material script
  var script = material.script;
  if (script) {
    if (script.uri.length > 0) {
      if (script.name) {
        mat = this.material[script.name];
        if (mat) {
          ambient = setMatRGB(mat['ambient'], material['ambient']);
          diffuse = setMatRGB(mat['diffuse'], material['diffuse']);
          emissive = setMatRGB(mat['emissive'], material['emissive']);
          specular = setMatRGB(mat['specular'], material['specular']);

          if (ambient) {
            var maxVal = Math.max(Math.max(ambient[0], ambient[1]), ambient[2]);
            if (maxVal) {
              // ThreeJS does not support ambient in material - apply the ambient only if it has a color
              var color = [
                ambient[0] / maxVal,
                ambient[1] / maxVal,
                ambient[2] / maxVal,
                1.0
              ];

              if (diffuse) {
                if (
                  Math.abs(diffuse[0] - diffuse[1]) < Number.EPSILON &&
                  Math.abs(diffuse[1] - diffuse[2]) < Number.EPSILON
                ) {
                  diffuse[0] *= color[0];
                  diffuse[1] *= color[1];
                  diffuse[2] *= color[2];
                }
              } else {
                diffuse = color;
              }
            }
          }

          opacity = mat['opacity'];
          scale = mat['scale'];

          var textureName = mat['texture'];
          if (textureName) {
            for (var i = 0; i < script.uri.length; ++i) {
              var type = script.uri[i].substring(
                0,
                script.uri[i].indexOf('://')
              );

              if (type === 'model') {
                if (script.uri[i].indexOf('textures') > 0) {
                  textureUri = script.uri[i].substring(
                    script.uri[i].indexOf('://') + 3
                  );
                  break;
                }
              } else if (type === 'file') {
                if (script.uri[i].indexOf('materials') > 0) {
                  textureUri =
                    script.uri[i].substring(
                      script.uri[i].indexOf('://') + 3,
                      script.uri[i].indexOf('materials') + 9
                    ) + '/textures';
                  break;
                }
              }
            }
            if (textureUri) {
              texture = uriPath + '/' + textureUri + '/' + textureName;
            }
          }
        }
      }
    }
  }

  if (material.emissive) {
    emissive = [material.emissive.r, material.emissive.g, material.emissive.b, material.emissive.a];
  }
  if (material.diffuse) {
    diffuse = [material.diffuse.r, material.diffuse.g, material.diffuse.b, material.diffuse.a];
  }
  if (material.specular) {
    specular = [material.specular.r, material.specular.g, material.specular.b, material.specular.a];
  }
  if (material.diffuse && material.diffuse.a) {
    opacity = material.diffuse.a;
  }

  // normal map
  if (material.normal_map) {
    var mapUri;
    if (material.normal_map.indexOf('://') > 0) {
      mapUri = material.normal_map.substring(
        material.normal_map.indexOf('://') + 3,
        material.normal_map.lastIndexOf('/')
      );
    } else {
      mapUri = textureUri;
    }
    if (mapUri) {
      var startIndex = material.normal_map.lastIndexOf('/') + 1;
      if (startIndex < 0) {
        startIndex = 0;
      }
      var normalMapName = material.normal_map.substr(
        startIndex,
        material.normal_map.lastIndexOf('.') - startIndex
      );
      normalMap = uriPath + '/' + mapUri + '/' + normalMapName + '.png';
    }
  }

  return {
    texture: texture,
    normalMap: normalMap,
    ambient: ambient,
    emissive: emissive,
    diffuse: diffuse,
    specular: specular,
    opacity: opacity,
    scale: scale
  };
};

/*GZ3D.GZIface.prototype.createGeom = function(geom, material, parent)
{
  var obj;

  var uriPath = 'assets';
  var texture;
  var normalMap;
  var textureUri;
  var mat;
  if (material)
  {
    // get texture from material script
    var script  = material.script;
    if (script)
    {
      if (script.uri.length > 0)
      {
        if (script.name)
        {
          mat = this.material[script.name];
          if (mat)
          {
            var textureName = mat['texture'];
            if (textureName)
            {
              for (var i = 0; i < script.uri.length; ++i)
              {
                var type = script.uri[i].substring(0,
                      script.uri[i].indexOf('://'));

                if (type === 'model')
                {
                  if (script.uri[i].indexOf('textures') > 0)
                  {
                    textureUri = script.uri[i].substring(
                        script.uri[i].indexOf('://') + 3);
                    break;
                  }
                }
                else if (type === 'file')
                {
                  if (script.uri[i].indexOf('materials') > 0)
                  {
                    textureUri = script.uri[i].substring(
                        script.uri[i].indexOf('://') + 3,
                        script.uri[i].indexOf('materials') + 9) + '/textures';
                    break;
                  }
                }
              }
              if (textureUri)
              {
                texture = uriPath + '/' +
                    textureUri  + '/' + textureName;
              }
            }
          }
        }
      }
    }
    // normal map
    if (material.normal_map)
    {
      var mapUri;
      if (material.normal_map.indexOf('://') > 0)
      {
        mapUri = material.normal_map.substring(
            material.normal_map.indexOf('://') + 3,
            material.normal_map.lastIndexOf('/'));
      }
      else
      {
        mapUri = textureUri;
      }
      if (mapUri)
      {
        var startIndex = material.normal_map.lastIndexOf('/') + 1;
        if (startIndex < 0)
        {
          startIndex = 0;
        }
        var normalMapName = material.normal_map.substr(startIndex,
            material.normal_map.lastIndexOf('.') - startIndex);
        normalMap = uriPath + '/' +
          mapUri  + '/' + normalMapName + '.png';
      }

    }
  }

  if (geom.box)
  {
    obj = this.scene.createBox(geom.box.size.x, geom.box.size.y,
        geom.box.size.z);
  }
  else if (geom.cylinder)
  {
    obj = this.scene.createCylinder(geom.cylinder.radius,
        geom.cylinder.length);
  }
  else if (geom.sphere)
  {
    obj = this.scene.createSphere(geom.sphere.radius);
  }
  else if (geom.plane)
  {
    obj = this.scene.createPlane(geom.plane.normal.x, geom.plane.normal.y,
        geom.plane.normal.z, geom.plane.size.x, geom.plane.size.y);
  }
  else if (geom.mesh)
  {
    // get model name which the mesh is in
    var rootModel = parent;
    while (rootModel.parent)
    {
      rootModel = rootModel.parent;
    }

    {
      var meshUri = geom.mesh.filename;
      var submesh = geom.mesh.submesh;
      var centerSubmesh = geom.mesh.center_submesh;

      console.log(geom.mesh.filename + ' ' + submesh);

      var uriType = meshUri.substring(0, meshUri.indexOf('://'));
      if (uriType === 'file' || uriType === 'model')
      {
        var modelName = meshUri.substring(meshUri.indexOf('://') + 3);
        if (geom.mesh.scale)
        {
          parent.scale.x = geom.mesh.scale.x;
          parent.scale.y = geom.mesh.scale.y;
          parent.scale.z = geom.mesh.scale.z;
        }

        this.scene.loadMesh(uriPath + '/' + modelName, submesh, centerSubmesh,
            texture, normalMap, parent);
      }
    }
  }
  else if (geom.heightmap)
  {
    var that = this;
    var request = new ROSLIB.ServiceRequest({
      name : that.scene.name
    });

    // redirect the texture paths to the assets dir
    var textures = geom.heightmap.texture;
    for ( var k = 0; k < textures.length; ++k)
    {
      textures[k].diffuse = this.parseUri(textures[k].diffuse);
      textures[k].normal = this.parseUri(textures[k].normal);
    }

    var sizes = geom.heightmap.size;

    // send service request and load heightmap on response
    this.heightmapDataService.callService(request,
        function(result)
        {
          var heightmap = result.heightmap;
          // gazebo heightmap is always square shaped,
          // and a dimension of: 2^N + 1
          that.scene.loadHeightmap(heightmap.heights, heightmap.size.x,
              heightmap.size.y, heightmap.width, heightmap.height,
              heightmap.origin, textures,
              geom.heightmap.blend, parent);
            //console.log('Result for service call on ' + result);
        });

    //this.scene.loadHeightmap(parent)
  }

  // texture mapping for simple shapes and planes only,
  // not used by mesh and terrain
  if (obj)
  {

    if (mat)
    {
      obj.material = new THREE.MeshPhongMaterial();

      var ambient = mat['ambient'];
      if (ambient)
      {
        obj.material.emissive.setRGB(ambient[0], ambient[1], ambient[2]);
      }
      var diffuse = mat['diffuse'];
      if (diffuse)
      {
        obj.material.color.setRGB(diffuse[0], diffuse[1], diffuse[2]);
      }
      var specular = mat['specular'];
      if (specular)
      {
        obj.material.specular.setRGB(specular[0], specular[1], specular[2]);
      }
      var opacity = mat['opacity'];
      if (opacity)
      {
        if (opacity < 1)
        {
          obj.material.transparent = true;
          obj.material.opacity = opacity;
        }
      }

      //this.scene.setMaterial(obj, texture, normalMap);

      if (texture)
      {
        obj.material.map = THREE.ImageUtils.loadTexture(texture);
      }
      if (normalMap)
      {
        obj.material.normalMap = THREE.ImageUtils.loadTexture(normalMap);
      }
    }
    obj.updateMatrix();
    parent.add(obj);
  }
};
*/
