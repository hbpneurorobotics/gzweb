/**
 * GZ3D Levels. Simply color levels shader.
 *
 */

GZ3D.LevelsShader = {
  uniforms: {
    tDiffuse: { value: null },

    // Levels parameters

    inBlack: { value: 0.0 },
    inWhite: { value: 1.0 },
    inGamma: { value: 1.0 },

    outBlack: { value: 0.0 },
    outWhite: { value: 1.0 }
  },

  vertexShader: [
    'varying vec2 vUv;',

    'void main() {',

    'vUv = uv;',
    'gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',

    '}'
  ].join('\n'),

  fragmentShader: [
    'uniform float inBlack,inWhite,inGamma,outBlack,outWhite;',
    'uniform sampler2D tDiffuse;',

    'varying vec2 vUv;',

    'void main() {',

    'vec4 texel = texture2D( tDiffuse, vUv );',

    'float outf =  (outWhite - outBlack);',

    'texel[0] = pow((texel[0] - inBlack) / (inWhite - inBlack),inGamma) * outf + outBlack;',
    'texel[1] = pow((texel[1] - inBlack) / (inWhite - inBlack),inGamma) * outf + outBlack;',
    'texel[2] = pow((texel[2] - inBlack) / (inWhite - inBlack),inGamma) * outf + outBlack;',

    'gl_FragColor = texel;',

    '}'
  ].join('\n')
};
