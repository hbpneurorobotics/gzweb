/**
 * The scene is where everything is placed, from objects, to lights and cameras.
 * @constructor
 */
GZ3D.Scene = function() {
  this.init();
};

GZ3D.Scene.prototype.LIGHT_POINT = 1;
GZ3D.Scene.prototype.LIGHT_SPOT = 2;
GZ3D.Scene.prototype.LIGHT_DIRECTIONAL = 3;
GZ3D.Scene.prototype.LIGHT_UNKNOWN = 4;

/**
 * Initialize scene
 */
GZ3D.Scene.prototype.init = function() {
  this.name = 'default';
  this.scene = new THREE.Scene();
  // this.scene.name = this.name;

  // Loaded models cache
  this.cachedModels = {};

  // Stores muscle visualization geometries
  this.muscleVisuzalizations = {};

  // Stores fluid visualization geometries
  this.fluidVisualizations = [];

  // only support one heightmap for now.
  this.heightmap = null;

  this.frameTime = 0;
  this.dropCycles = 0;

  this.selectedEntity = null;

  this.manipulationMode = 'view';
  this.pointerOnMenu = false;

  // create views manager
  this.viewManager = new GZ3D.MultiView(this);

  // lights
  this.ambient = new THREE.AmbientLight(0x666666);
  this.scene.add(this.ambient);

  // camera
  this.camera = this.viewManager.getViewByName(
    GZ3D.MULTIVIEW_MAINVIEW_NAME
  ).camera;
  this.defaultCameraPosition = new THREE.Vector3(5, 0, 1);
  this.defaultCameraLookAt = new THREE.Vector3(0.0, 0.0, 0.0);
  this.resetView();

  // create post-processing composer
  this.composer = new GZ3D.Composer(this);
  this.composerSettings = new GZ3D.ComposerSettings();
  this.normalizedComposerSettings = new GZ3D.ComposerSettings();

  // create label manager
  this.labelManager = new GZ3D.LabelManager(this);

  // Grid
  this.grid = new THREE.GridHelper(
    10,
    1,
    new THREE.Color(0xcccccc),
    new THREE.Color(0x4d4d4d)
  );
  this.grid.name = 'grid';
  this.grid.position.z = 0.05;
  this.grid.rotation.x = Math.PI * 0.5;
  this.grid.castShadow = false;
  this.grid.material.transparent = true;
  this.grid.material.opacity = 0.5;
  this.grid.visible = false;
  this.scene.add(this.grid);

  this.showCollisions = false;
  this.lockSelectedIdentity = false;

  this.showLightHelpers = false;

  this.spawnModel = new GZ3D.SpawnModel(this);
  // Material for simple shapes being spawned (grey transparent)
  this.spawnedShapeMaterial = new THREE.MeshPhongMaterial({
    color: 0xffffff,
    flatShading: THREE.SmoothShading
  });
  this.spawnedShapeMaterial.transparent = true;
  this.spawnedShapeMaterial.opacity = 0.5;

  var that = this;

  this.keyboardBindingsEnabled = true;

  // Handles for translating, scaling and rotating objects
  this.modelManipulator = new GZ3D.Manipulator(this, isTouchDevice);

  this.timeDown = null;

  this.emitter = new EventEmitter2({ verbose: true });

  // Radial menu (only triggered by touch)
  this.radialMenu = new GZ3D.RadialMenu(this);
  this.scene.add(this.radialMenu.menu);

  // Bounding Box
  var vertices = [
    new THREE.Vector3(0, 0, 0),
    new THREE.Vector3(0, 0, 0),
    new THREE.Vector3(0, 0, 0),
    new THREE.Vector3(0, 0, 0),

    new THREE.Vector3(0, 0, 0),
    new THREE.Vector3(0, 0, 0),
    new THREE.Vector3(0, 0, 0),
    new THREE.Vector3(0, 0, 0)
  ];
  var boxGeometry = new THREE.Geometry();
  boxGeometry.vertices.push(
    vertices[0],
    vertices[1],
    vertices[1],
    vertices[2],
    vertices[2],
    vertices[3],
    vertices[3],
    vertices[0],

    vertices[4],
    vertices[5],
    vertices[5],
    vertices[6],
    vertices[6],
    vertices[7],
    vertices[7],
    vertices[4],

    vertices[0],
    vertices[4],
    vertices[1],
    vertices[5],
    vertices[2],
    vertices[6],
    vertices[3],
    vertices[7]
  );
  this.boundingBox = new THREE.LineSegments(
    boxGeometry,
    new THREE.LineBasicMaterial({ color: 0xffffff })
  );
  this.boundingBox.visible = false;

  // Joint visuals
  this.jointTypes = {
    REVOLUTE: 1,
    REVOLUTE2: 2,
    PRISMATIC: 3,
    UNIVERSAL: 4,
    BALL: 5,
    SCREW: 6,
    GEARBOX: 7
  };
  this.jointAxis = new THREE.Object3D();
  this.jointAxis.name = 'JOINT_VISUAL';
  var geometry, material, mesh;

  // XYZ
  var XYZaxes = new THREE.Object3D();

  geometry = new THREE.CylinderGeometry(0.01, 0.01, 0.3, 10, 1, false);

  material = new THREE.MeshBasicMaterial({ color: new THREE.Color(0xff0000) });
  mesh = new THREE.Mesh(geometry, material);
  mesh.position.x = 0.15;
  mesh.rotation.z = -Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  XYZaxes.add(mesh);

  material = new THREE.MeshBasicMaterial({ color: new THREE.Color(0x00ff00) });
  mesh = new THREE.Mesh(geometry, material);
  mesh.position.y = 0.15;
  mesh.name = 'JOINT_VISUAL';
  XYZaxes.add(mesh);

  material = new THREE.MeshBasicMaterial({ color: new THREE.Color(0x0000ff) });
  mesh = new THREE.Mesh(geometry, material);
  mesh.position.z = 0.15;
  mesh.rotation.x = Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  XYZaxes.add(mesh);

  geometry = new THREE.CylinderGeometry(0, 0.03, 0.1, 10, 1, true);

  material = new THREE.MeshBasicMaterial({ color: new THREE.Color(0xff0000) });
  mesh = new THREE.Mesh(geometry, material);
  mesh.position.x = 0.3;
  mesh.rotation.z = -Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  XYZaxes.add(mesh);

  material = new THREE.MeshBasicMaterial({ color: new THREE.Color(0x00ff00) });
  mesh = new THREE.Mesh(geometry, material);
  mesh.position.y = 0.3;
  mesh.name = 'JOINT_VISUAL';
  XYZaxes.add(mesh);

  material = new THREE.MeshBasicMaterial({ color: new THREE.Color(0x0000ff) });
  mesh = new THREE.Mesh(geometry, material);
  mesh.position.z = 0.3;
  mesh.rotation.x = Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  XYZaxes.add(mesh);

  this.jointAxis['XYZaxes'] = XYZaxes;

  var mainAxis = new THREE.Object3D();

  material = new THREE.MeshLambertMaterial();
  material.color = new THREE.Color(0xffff00);
  material.emissive = material.color;

  geometry = new THREE.CylinderGeometry(0.02, 0.02, 0.25, 36, 1, false);

  mesh = new THREE.Mesh(geometry, material);
  mesh.position.z = -0.175;
  mesh.rotation.x = Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  mainAxis.add(mesh);

  geometry = new THREE.CylinderGeometry(0, 0.035, 0.1, 36, 1, false);

  mesh = new THREE.Mesh(geometry, material);
  mesh.rotation.x = Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  mainAxis.add(mesh);

  this.jointAxis['mainAxis'] = mainAxis;

  var rotAxis = new THREE.Object3D();

  geometry = new THREE.TorusGeometry(0.04, 0.006, 10, 36, Math.PI * 3 / 2);

  mesh = new THREE.Mesh(geometry, material);
  mesh.name = 'JOINT_VISUAL';
  rotAxis.add(mesh);

  geometry = new THREE.CylinderGeometry(0.015, 0, 0.025, 10, 1, false);

  mesh = new THREE.Mesh(geometry, material);
  mesh.position.y = -0.04;
  mesh.rotation.z = Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  rotAxis.add(mesh);

  this.jointAxis['rotAxis'] = rotAxis;

  var transAxis = new THREE.Object3D();

  geometry = new THREE.CylinderGeometry(0.01, 0.01, 0.1, 10, 1, true);

  mesh = new THREE.Mesh(geometry, material);
  mesh.position.x = 0.03;
  mesh.position.y = 0.03;
  mesh.position.z = -0.15;
  mesh.rotation.x = Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  transAxis.add(mesh);

  geometry = new THREE.CylinderGeometry(0.02, 0, 0.0375, 10, 1, false);

  mesh = new THREE.Mesh(geometry, material);
  mesh.position.x = 0.03;
  mesh.position.y = 0.03;
  mesh.position.z = -0.15 + 0.05;
  mesh.rotation.x = -Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  transAxis.add(mesh);

  mesh = new THREE.Mesh(geometry, material);
  mesh.position.x = 0.03;
  mesh.position.y = 0.03;
  mesh.position.z = -0.15 - 0.05;
  mesh.rotation.x = Math.PI / 2;
  mesh.name = 'JOINT_VISUAL';
  transAxis.add(mesh);

  this.jointAxis['transAxis'] = transAxis;

  var screwAxis = new THREE.Object3D();

  mesh = new THREE.Mesh(geometry, material);
  mesh.position.x = -0.04;
  mesh.position.z = -0.11;
  mesh.rotation.z = -Math.PI / 4;
  mesh.rotation.x = -Math.PI / 10;
  mesh.name = 'JOINT_VISUAL';
  screwAxis.add(mesh);

  var radius = 0.04;
  var length = 0.02;
  var curve = new THREE.CatmullRomCurve3([
    new THREE.Vector3(radius, 0, 0 * length),
    new THREE.Vector3(0, radius, 1 * length),
    new THREE.Vector3(-radius, 0, 2 * length),
    new THREE.Vector3(0, -radius, 3 * length),
    new THREE.Vector3(radius, 0, 4 * length),
    new THREE.Vector3(0, radius, 5 * length),
    new THREE.Vector3(-radius, 0, 6 * length)
  ]);
  geometry = new THREE.TubeGeometry(curve, 36, 0.01, 10, false);

  mesh = new THREE.Mesh(geometry, material);
  mesh.position.z = -0.23;
  mesh.name = 'JOINT_VISUAL';
  screwAxis.add(mesh);

  this.jointAxis['screwAxis'] = screwAxis;

  var ballVisual = new THREE.Object3D();

  geometry = new THREE.SphereGeometry(0.06);

  mesh = new THREE.Mesh(geometry, material);
  mesh.name = 'JOINT_VISUAL';
  ballVisual.add(mesh);

  this.jointAxis['ballVisual'] = ballVisual;
};

GZ3D.Scene.prototype.setSDFParser = function(sdfParser) {
  this.spawnModel.sdfParser = sdfParser;
};

/**
 * Begin natural manipulation of model
 * @param {} event - mousedown or touchdown events
 */

GZ3D.Scene.prototype.beginNaturalManipulation = function(event) {
  this.naturalAutoAlignMode = null;

  if (event.ctrlKey) {
    return;
  }

  // check for model selection
  var mainPointer = true;
  var pos;
  if (event.touches) {
    if (event.touches.length === 1) {
      pos = new THREE.Vector2(
        event.touches[0].offsetX,
        event.touches[0].offsetY
      );
    } else {
      return;
    }
  } else {
    pos = new THREE.Vector2(event.offsetX, event.offsetY);
    if (event.button !== 0) {
      mainPointer = false;
    }
  }

  var intersect = new THREE.Vector3();
  var model = this.getRayCastModel(pos, intersect);

  // Cancel in case of multitouch
  if ((event.touches && event.touches.length !== 1) || !mainPointer) {
    return;
  }

  if (model) {
    if (model.name === 'plane') {
      return;
    } else if (model.name !== '') {
      // Attach manipulator to model
      if (mainPointer && model.parent === this.scene) {
        this.selectEntity(model);
      }
    } else {
      return;
    }

    this.naturalAutoAlignMode = new GZ3D.AutoAlignModel(this);
    this.naturalAutoAlignMode.start(model);

    if (this.controls && this.controls.input && this.controls.input.mouse) {
      this.controls.input.mouse.enabled = false;
    }
  }
};

/**
 * Update natural manipulation of model
 * @param {} event - mousedown or touchdown events
 */

GZ3D.Scene.prototype.updateMoveNaturalManipulation = function(
  positionX,
  positionY
) {
  if (this.naturalAutoAlignMode) {
    this.naturalAutoAlignMode.moveAlignModel(positionX, positionY);
  }
};

/**
 * End natural manipulation of model
 * @param {} event - mousedown or touchdown events
 */

GZ3D.Scene.prototype.endNaturalManipulation = function(event) {
  if (this.naturalAutoAlignMode) {
    this.naturalAutoAlignMode.finish();
    this.naturalAutoAlignMode = null;
    this.updateDynamicEnvMap();

    if (this.controls && this.controls.input && this.controls.input.mouse) {
      this.controls.input.mouse.enabled = true;
    }
  }
};

/**
 * attach window event callbacks
 */
GZ3D.Scene.prototype.attachEventListeners = function() {
  if (!this.getDomElement()) {
    console.warn(
      'GZ3D.Scene.attachEventListeners() - user view container element not defined!'
    );
    return;
  }

  var that = this;

  // Need to use `document` instead of getDomElement in order to get event outside the webgl div element.
  this.getDomElement().addEventListener(
    'mouseup',
    function(event) {
      that.onPointerUp(event);
    },
    false
  );
  //document.addEventListener( 'mouseup', function(event) {that.onPointerUp(event);}, false );
  this.getDomElement().addEventListener(
    'mousedown',
    function(event) {
      that.onPointerDown(event);
    },
    false
  );
  this.getDomElement().addEventListener(
    'touchstart',
    function(event) {
      that.onPointerDown(event);
    },
    false
  );
  this.getDomElement().addEventListener(
    'touchend',
    function(event) {
      that.onPointerUp(event);
    },
    false
  );

  document.addEventListener(
    'keydown',
    function(event) {
      that.onKeyDown(event);
    },
    false
  );
};

/**
 * Window event callback
 * @param {} event - mousedown or touchdown events
 */
GZ3D.Scene.prototype.onPointerDown = function(event) {
  if (this.keyboardBindingsEnabled === false) {
    return;
  }

  event.preventDefault();

  if (this.spawnModel.active) {
    return;
  }

  if (this.manipulationMode === 'natural') {
    this.beginNaturalManipulation(event);
    this.updateUI();

    if (this.naturalAutoAlignMode) {
      return;
    }
  }

  this.timeDown = new Date().getTime();
};

/**
 * Window event callback
 * @param {} event - mouseup or touchend events
 */
GZ3D.Scene.prototype.onPointerUp = function(event) {
  if (this.keyboardBindingsEnabled === false) {
    return;
  }

  event.preventDefault();

  if (this.naturalAutoAlignMode) {
    this.endNaturalManipulation();
    return;
  }

  var millisecs = new Date().getTime();
  if (millisecs - this.timeDown < 300) {
    // check for model selection
    var mainPointer = true;
    var pos;
    if (event.touches) {
      if (event.touches.length === 1) {
        pos = new THREE.Vector2(
          event.touches[0].offsetX,
          event.touches[0].offsetY
        );
      } else if (event.touches.length === 2) {
        pos = new THREE.Vector2(
          (event.touches[0].offsetX + event.touches[1].offsetX) / 2,
          (event.touches[0].offsetY + event.touches[1].offsetY) / 2
        );
      } else {
        return;
      }
    } else {
      pos = new THREE.Vector2(event.offsetX, event.offsetY);
      if (event.button !== 0) {
        mainPointer = false;
      }
    }

    var intersect = new THREE.Vector3();
    var model = this.getRayCastModel(pos, intersect);

    // Cancel in case of multitouch
    if (event.touches && event.touches.length !== 1) {
      return;
    }

    // Manipulation modes
    // Model found
    if (model) {
      if (model.name.indexOf('LABEL_INFO_VISUAL') >= 0) {
        guiEvents.emit('setTreeSelection', model.name);
      } else if (model.name === 'plane') {
        // Do nothing
      } else if (this.modelManipulator.pickerNames.indexOf(model.name) >= 0) {
        // Do not attach manipulator to itself
      } else if (model.name !== '') {
        // Attach manipulator to model
        if (mainPointer && model.parent === this.scene) {
          this.selectEntity(model);
        }
      } else if (this.modelManipulator.hovered) {
        // Manipulator pickers, for mouse
        this.modelManipulator.update();
        this.modelManipulator.object.updateMatrixWorld();
      } else {
        // Sky
        // Do nothing
      }
    } else {
      // Clicks (<150ms) outside any models trigger view mode
      this.setManipulationMode('view');
      $('#view-mode').click();
      $('input[type="radio"]').checkboxradio('refresh');
    }
  }

  this.timeDown = null;
};

/**
 * Window event callback
 * @param {} event - mousescroll event
 */
GZ3D.Scene.prototype.onMouseScroll = function(event) {
  event.preventDefault();

  var pos = new THREE.Vector2(event.offsetX, event.offsetY);

  var intersect = new THREE.Vector3();
  var model = this.getRayCastModel(pos, intersect);
};

/**
 * Window event callback
 * @param {} event - keydown events
 */
GZ3D.Scene.prototype.onKeyDown = function(event) {
  if (this.keyboardBindingsEnabled === false) {
    return;
  }

  if (event.shiftKey) {
    // + and - for zooming
    if (event.keyCode === 187 || event.keyCode === 189) {
      var pos = new THREE.Vector2(
        window.innerWidth / 2.0,
        window.innerHeight / 2.0
      );

      var intersect = new THREE.Vector3();
      var model = this.getRayCastModel(pos, intersect);
    }
  }

  // DEL to delete entities
  if (event.keyCode === 46) {
    if (this.selectedEntity) {
      guiEvents.emit('delete_entity');
    }
  }

  // Esc/R/T for changing manipulation modes
  if (event.keyCode === 27) {
    // Esc
    $('#view-mode').click();
    $('input[type="radio"]').checkboxradio('refresh');
  }
  if (event.keyCode === 82) {
    // R
    $('#rotate-mode').click();
    $('input[type="radio"]').checkboxradio('refresh');
  }
  if (event.keyCode === 84) {
    // T
    $('#translate-mode').click();
    $('input[type="radio"]').checkboxradio('refresh');
  }
};

/**
 * Prepare face to camera object for ray cast
 *
 */

GZ3D.Scene.prototype.prepareModelsForRaycast = function(before) {
  var that = this;
  this.scene.traverse(function(node) {
    if (before) {
      if (node._ignoreRaycast) {
        node._ignoreRayCastVisiblePrevState = node.visible;
        node.visible = false;
      } else if (node._raycastOnly) {
        node.visible = true;
      }
    } else {
      if (node._ignoreRayCastVisiblePrevState) {
        node.visible = node._ignoreRayCastVisiblePrevState;
        node._ignoreRayCastVisiblePrevState = undefined;
      } else if (node._raycastOnly) {
        node.visible = false;
      }
    }
  });
};

/**
 * Check if there's a model immediately under canvas coordinate 'pos'
 * @param {THREE.Vector2} pos - Canvas coordinates
 * @param {THREE.Vector3} intersect - Empty at input,
 * contains point of intersection in 3D world coordinates at output
 * @returns {THREE.Object3D} model - Intercepted model closest to the camera
 */
GZ3D.Scene.prototype.getRayCastModel = function(pos, intersect) {
  var normalizedScreenCoords = new THREE.Vector2(
    pos.x / this.viewManager.mainUserView.container.clientWidth * 2 - 1,
    -(pos.y / this.viewManager.mainUserView.container.clientHeight) * 2 + 1
  );

  var raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(normalizedScreenCoords, this.camera);

  this.prepareModelsForRaycast(true);
  var objects = raycaster.intersectObjects(this.scene.children, true);
  this.prepareModelsForRaycast(false);

  var model;
  var point, i;
  if (objects.length > 0) {
    for (i = 0; i < objects.length; ++i) {
      model = objects[i].object;

      // Check first robot info

      if (model.name.indexOf('LABEL_INFO_VISUAL') >= 0) {
        this.robotInfoObject = model;
        return model;
      }
    }

    modelsloop: for (i = 0; i < objects.length; ++i) {
      model = objects[i].object;
      if (model.name.indexOf('_lightHelper') >= 0) {
        model = model.parent;
        break;
      }

      if (!this.modelManipulator.hovered && model.name === 'plane') {
        // model = null;
        point = objects[i].point;
        break;
      }

      if (
        model.name === 'grid' ||
        model.name === 'boundingBox' ||
        model.name === 'JOINT_VISUAL'
      ) {
        point = objects[i].point;
        model = null;
        continue;
      }

      while (model.parent !== this.scene) {
        // Select current mode's handle
        if (
          model.parent.parent === this.modelManipulator.gizmo &&
          ((this.manipulationMode === 'translate' &&
            model.name.indexOf('T') >= 0) ||
            (this.manipulationMode === 'rotate' &&
              model.name.indexOf('R') >= 0))
        ) {
          break modelsloop;
        }
        model = model.parent;
      }

      if (model === this.radialMenu.menu) {
        continue;
      }

      if (model.name.indexOf('COLLISION_VISUAL') >= 0) {
        model = null;
        continue;
      }

      if (this.modelManipulator.hovered) {
        if (model === this.modelManipulator.gizmo) {
          break;
        }
      } else if (model.name !== '') {
        point = objects[i].point;
        break;
      }
    }
  }
  if (point) {
    intersect.x = point.x;
    intersect.y = point.y;
    intersect.z = point.z;
  }
  return model;
};

/**
 * Get dom element
 * @returns {domElement}
 */
GZ3D.Scene.prototype.getDomElement = function() {
  return this.viewManager.mainUserView.container;
};

/**
 * Render scene
 */

GZ3D.Scene.prototype.render = function() {
  this.viewManager.renderViews();
  this.updateUI();
};

/**
 * updateUI
 */

GZ3D.Scene.prototype.updateUI = function() {
  this.modelManipulator.update();
  this.radialMenu.update();
};

/**
 * Add object to the scene
 * @param {THREE.Object3D} model
 */
GZ3D.Scene.prototype.add = function(model) {
  model.viewAs = 'normal';
  this.scene.add(model);
  this.updateDynamicEnvMap();
  this.refresh3DViews();
};

/**
 * Remove object from the scene
 * @param {THREE.Object3D} model
 */
GZ3D.Scene.prototype.remove = function(model) {
  this.scene.remove(model);
  this.updateDynamicEnvMap();
  this.refresh3DViews();
};

/**
 * Returns the object which has the given name
 * @param {string} name
 * @returns {THREE.Object3D} model
 */
GZ3D.Scene.prototype.getByName = function(name) {
  return this.scene.getObjectByName(name, true);
};

/**
 * Returns the object which has the specified ID and name.
 * @param {object} gazeboSpec Object specifying ID and name from gazebo.
 * @returns {THREE.Object3D} model
 */
GZ3D.Scene.prototype.getGazeboObject = function(gazeboSpec) {
  let object;
  this.scene.traverse(node => {
    if (node.userData.id === gazeboSpec.id && node.name === gazeboSpec.name) {
      object = node;
    }
  });

  return object;
};

/**
 * Update a model's pose
 * @param {THREE.Object3D} model
 * @param {} position
 * @param {} orientation
 */
GZ3D.Scene.prototype.updatePose = function(model, position, orientation) {
  if (
    this.modelManipulator &&
    this.modelManipulator.object &&
    this.modelManipulator.hovered
  ) {
    return;
  }

  this.setPose(model, position, orientation);
};

/**
 * Set a model's pose
 * @param {THREE.Object3D} model
 * @param {} position
 * @param {} orientation
 */
GZ3D.Scene.prototype.setPose = function(model, position, orientation) {
  model.position.x = position.x;
  model.position.y = position.y;
  model.position.z = position.z;
  model.quaternion.w = orientation.w;
  model.quaternion.x = orientation.x;
  model.quaternion.y = orientation.y;
  model.quaternion.z = orientation.z;

  this.refresh3DViews();
  this.updateBoundingBox(model);
};

/**
 * Set the scale of a model
 * @param {THREE.Object3D} model
 * @param {THREE.Vector3} scale
 */
GZ3D.Scene.prototype.setScale = function(model, scale) {
  model.scale.copy(scale);
  model.updateMatrixWorld();
  this.refresh3DViews();
  this.updateBoundingBox(model);
};

GZ3D.Scene.prototype.removeAll = function() {
  while (this.scene.children.length > 0) {
    this.scene.remove(this.scene.children[0]);
  }
  this.refresh3DViews();
};

/**
 * Create plane
 * @param {double} normalX
 * @param {double} normalY
 * @param {double} normalZ
 * @param {double} width
 * @param {double} height
 * @returns {THREE.Mesh}
 */
GZ3D.Scene.prototype.createPlane = function(
  normalX,
  normalY,
  normalZ,
  width,
  height
) {
  var geometry = new THREE.PlaneGeometry(width, height, 1, 1);
  var material = new THREE.MeshPhongMaterial({
    color: 0xbbbbbb,
    flatShading: THREE.SmoothShading
  }); // Later Gazebo/Grey
  var mesh = new THREE.Mesh(geometry, material);
  var normal = new THREE.Vector3(normalX, normalY, normalZ);
  var cross = normal.crossVectors(normal, mesh.up);
  mesh.rotation = normal.applyAxisAngle(cross, -normal.angleTo(mesh.up));
  mesh.name = 'plane';
  mesh.receiveShadow = true;
  return mesh;
};

/**
 * Create sphere
 * @param {double} radius
 * @returns {THREE.Mesh}
 */
GZ3D.Scene.prototype.createSphere = function(radius) {
  var geometry = new THREE.SphereGeometry(radius, 32, 32);
  var mesh = new THREE.Mesh(geometry, this.spawnedShapeMaterial);
  return mesh;
};

/**
 * Create cylinder
 * @param {double} radius
 * @param {double} length
 * @returns {THREE.Mesh}
 */
GZ3D.Scene.prototype.createCylinder = function(radius, length) {
  var geometry = new THREE.CylinderGeometry(
    radius,
    radius,
    length,
    32,
    1,
    false
  );
  var mesh = new THREE.Mesh(geometry, this.spawnedShapeMaterial);
  mesh.rotation.x = Math.PI * 0.5;
  return mesh;
};

/**
 * Create box
 * @param {double} width
 * @param {double} height
 * @param {double} depth
 * @returns {THREE.Mesh}
 */
GZ3D.Scene.prototype.createBox = function(width, height, depth) {
  var geometry = new THREE.BoxGeometry(width, height, depth, 1, 1, 1);

  // Fix UVs so textures are mapped in a way that is consistent to gazebo
  // Some face uvs need to be rotated clockwise, while others anticlockwise
  // After updating to threejs rev 62, geometries changed from quads (6 faces)
  // to triangles (12 faces).
  geometry.dynamic = true;
  var faceUVFixA = [1, 4, 5];
  var faceUVFixB = [0];
  for (var i = 0; i < faceUVFixA.length; ++i) {
    var idx = faceUVFixA[i] * 2;
    var uva = geometry.faceVertexUvs[0][idx][0];
    geometry.faceVertexUvs[0][idx][0] = geometry.faceVertexUvs[0][idx][1];
    geometry.faceVertexUvs[0][idx][1] = geometry.faceVertexUvs[0][idx + 1][1];
    geometry.faceVertexUvs[0][idx][2] = uva;

    geometry.faceVertexUvs[0][idx + 1][0] =
      geometry.faceVertexUvs[0][idx + 1][1];
    geometry.faceVertexUvs[0][idx + 1][1] =
      geometry.faceVertexUvs[0][idx + 1][2];
    geometry.faceVertexUvs[0][idx + 1][2] = geometry.faceVertexUvs[0][idx][2];
  }
  for (var ii = 0; ii < faceUVFixB.length; ++ii) {
    var idxB = faceUVFixB[ii] * 2;
    var uvc = geometry.faceVertexUvs[0][idxB][0];
    geometry.faceVertexUvs[0][idxB][0] = geometry.faceVertexUvs[0][idxB][2];
    geometry.faceVertexUvs[0][idxB][1] = uvc;
    geometry.faceVertexUvs[0][idxB][2] = geometry.faceVertexUvs[0][idxB + 1][1];

    geometry.faceVertexUvs[0][idxB + 1][2] = geometry.faceVertexUvs[0][idxB][2];
    geometry.faceVertexUvs[0][idxB + 1][1] =
      geometry.faceVertexUvs[0][idxB + 1][0];
    geometry.faceVertexUvs[0][idxB + 1][0] = geometry.faceVertexUvs[0][idxB][1];
  }
  geometry.uvsNeedUpdate = true;

  var mesh = new THREE.Mesh(geometry, this.spawnedShapeMaterial);
  mesh.castShadow = true;
  return mesh;
};

/**
 * Create light
 * @param {} type - 1: point, 2: spot, 3: directional
 * @param {} diffuse
 * @param {} intensity
 * @param {} pose
 * @param {} distance
 * @param {} cast_shadows
 * @param {} name
 * @param {} direction
 * @param {} specular
 * @param {} attenuation_constant
 * @param {} attenuation_linear
 * @param {} attenuation_quadratic
 * @param {} spot_angle
 * @param {} spot_falloff
 * @returns {THREE.Object3D}
 */
GZ3D.Scene.prototype.createLight = function(
  type,
  diffuse,
  intensity,
  pose,
  distance,
  cast_shadows,
  name,
  direction,
  specular,
  attenuation_constant,
  attenuation_linear,
  attenuation_quadratic,
  spot_angle,
  spot_falloff
) {
  var obj = new THREE.Object3D();
  var color = new THREE.Color();

  if (typeof diffuse === 'undefined') {
    diffuse = 0xffffff;
  } else if (typeof diffuse !== THREE.Color) {
    color.r = diffuse.r;
    color.g = diffuse.g;
    color.b = diffuse.b;
    diffuse = color.clone();
  } else if (typeof specular !== THREE.Color) {
    color.r = specular.r;
    color.g = specular.g;
    color.b = specular.b;
    specular = color.clone();
  }

  if (typeof specular === 'undefined') {
    specular = 0xffffff;
  }

  var matrixWorld;

  if (pose) {
    var quaternion = new THREE.Quaternion(
      pose.orientation.x,
      pose.orientation.y,
      pose.orientation.z,
      pose.orientation.w
    );

    var translation = new THREE.Vector3(
      pose.position.x,
      pose.position.y,
      pose.position.z
    );

    matrixWorld = new THREE.Matrix4();
    matrixWorld.compose(translation, quaternion, new THREE.Vector3(1, 1, 1));

    this.setPose(obj, pose.position, pose.orientation);
    obj.matrixWorldNeedsUpdate = true;
  }

  var elements;
  if (type === this.LIGHT_POINT) {
    elements = this.createPointLight(
      obj,
      diffuse,
      intensity,
      distance,
      cast_shadows
    );
  } else if (type === this.LIGHT_SPOT) {
    elements = this.createSpotLight(
      obj,
      diffuse,
      intensity,
      distance,
      cast_shadows,
      spot_angle,
      spot_falloff
    );
  } else if (type === this.LIGHT_DIRECTIONAL) {
    elements = this.createDirectionalLight(
      obj,
      diffuse,
      intensity,
      cast_shadows
    );
  }

  var lightObj = elements[0];
  var helper = elements[1];
  obj.add(lightObj);
  obj.add(helper);

  helper.visible = this.showLightHelpers;
  lightObj.up = new THREE.Vector3(0, 0, 1);
  lightObj.shadow.bias = -0.0005;

  if (name) {
    lightObj.name = name;
    obj.name = name;
    helper.name = name + '_lightHelper';
  } else {
    helper.name = '_lightHelper';
  }

  if (
    (type === this.LIGHT_SPOT || type === this.LIGHT_DIRECTIONAL) &&
    !(direction instanceof THREE.Vector3)
  ) {
    direction = new THREE.Vector3(0, 0, -1);
  }
  if (direction) {
    lightObj.target.name = name + '_target';
    obj.add(lightObj.target);
    lightObj.target.position.copy(direction);
    lightObj.target.updateMatrixWorld();
  }

  // Add properties which exist on the server but have no meaning on THREE.js
  obj.serverProperties = {};
  obj.serverProperties.specular = specular;
  obj.serverProperties.attenuation_constant = attenuation_constant;
  obj.serverProperties.attenuation_linear = attenuation_linear;
  obj.serverProperties.attenuation_quadratic = attenuation_quadratic;
  obj.serverProperties.initial = {};
  obj.serverProperties.initial.diffuse = diffuse;

  return obj;
};

/**
 * Create point light - called by createLight
 * @param {} obj - light object
 * @param {} color
 * @param {} intensity
 * @param {} distance
 * @param {} cast_shadows
 * @returns {Object.<THREE.Light, THREE.Mesh>}
 */
GZ3D.Scene.prototype.createPointLight = function(
  obj,
  color,
  intensity,
  distance,
  cast_shadows
) {
  if (typeof intensity === 'undefined') {
    intensity = 0.5;
  }

  var lightObj = new THREE.PointLight(color, intensity);
  //  lightObj.shadowDarkness = 0.3;

  if (distance) {
    lightObj.distance = distance;
  }

  // point light shadow maps not supported yet, disable for now
  /*if (cast_shadows)
  {
    lightObj.castShadow = cast_shadows;
  }*/

  var helperGeometry = new THREE.OctahedronGeometry(0.25, 0);
  helperGeometry.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / 2));
  var helperMaterial = new THREE.MeshBasicMaterial({
    wireframe: true,
    color: 0x00ff00
  });
  var helper = new THREE.Mesh(helperGeometry, helperMaterial);

  return [lightObj, helper];
};

/**
 * Create spot light - called by createLight
 * @param {} obj - light object
 * @param {} color
 * @param {} intensity
 * @param {} distance
 * @param {} cast_shadows
 * @returns {Object.<THREE.Light, THREE.Mesh>}
 */
GZ3D.Scene.prototype.createSpotLight = function(
  obj,
  color,
  intensity,
  distance,
  cast_shadows,
  angle,
  falloff
) {
  if (typeof intensity === 'undefined') {
    intensity = 1;
  }
  if (typeof distance === 'undefined') {
    distance = 20;
  }
  if (typeof angle === 'undefined') {
    angle = Math.PI / 3;
  }
  if (typeof falloff === 'undefined') {
    falloff = 1;
  }

  var lightObj = new THREE.SpotLight(
    color,
    intensity,
    distance,
    angle,
    falloff
  );
  lightObj.position.set(0, 0, 0);
  //  lightObj.shadowDarkness = 0.3;
  lightObj.shadow.bias = 0.0001;

  lightObj.shadow.camera.near = 0.1;
  lightObj.shadow.camera.far = 50;
  lightObj.shadow.camera.fov = 2 * angle / Math.PI * 180;

  lightObj.shadow.mapSize.width = 2048;
  lightObj.shadow.mapSize.height = 2048;

  if (cast_shadows) {
    lightObj.castShadow = cast_shadows;
  }

  var helperGeometry = new THREE.CylinderGeometry(0, 0.3, 0.2, 4, 1, true);
  helperGeometry.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / 2));
  helperGeometry.applyMatrix(new THREE.Matrix4().makeRotationZ(Math.PI / 4));
  var helperMaterial = new THREE.MeshBasicMaterial({
    wireframe: true,
    color: 0x00ff00
  });
  var helper = new THREE.Mesh(helperGeometry, helperMaterial);

  return [lightObj, helper];
};

/**
 * Create directional light - called by createLight
 * @param {} obj - light object
 * @param {} color
 * @param {} intensity
 * @param {} cast_shadows
 * @returns {Object.<THREE.Light, THREE.Mesh>}
 */
GZ3D.Scene.prototype.createDirectionalLight = function(
  obj,
  color,
  intensity,
  cast_shadows
) {
  if (typeof intensity === 'undefined') {
    intensity = 1;
  }

  var lightObj = new THREE.DirectionalLight(color, intensity);
  lightObj.shadow.camera.near = 0.1;
  lightObj.shadow.camera.far = 50;
  lightObj.shadow.mapSize.width = 2048;
  lightObj.shadow.mapSize.height = 2048;
  lightObj.shadow.camera.bottom = -15;
  lightObj.shadow.camera.left = -15;
  lightObj.shadow.camera.right = 15;
  lightObj.shadow.camera.top = 15;
  lightObj.shadow.bias = 0.0001;
  lightObj.position.set(0, 0, 0);

  if (cast_shadows) {
    lightObj.castShadow = cast_shadows;
  }

  var helperGeometry = new THREE.Geometry();
  helperGeometry.vertices.push(new THREE.Vector3(-0.5, -0.5, 0));
  helperGeometry.vertices.push(new THREE.Vector3(-0.5, 0.5, 0));
  helperGeometry.vertices.push(new THREE.Vector3(-0.5, 0.5, 0));
  helperGeometry.vertices.push(new THREE.Vector3(0.5, 0.5, 0));
  helperGeometry.vertices.push(new THREE.Vector3(0.5, 0.5, 0));
  helperGeometry.vertices.push(new THREE.Vector3(0.5, -0.5, 0));
  helperGeometry.vertices.push(new THREE.Vector3(0.5, -0.5, 0));
  helperGeometry.vertices.push(new THREE.Vector3(-0.5, -0.5, 0));
  helperGeometry.vertices.push(new THREE.Vector3(0, 0, 0));
  helperGeometry.vertices.push(new THREE.Vector3(0, 0, -0.5));
  var helperMaterial = new THREE.LineBasicMaterial({ color: 0x00ff00 });
  var helper = new THREE.LineSegments(helperGeometry, helperMaterial);

  return [lightObj, helper];
};

/**
 * Create roads
 * @param {} points
 * @param {} width
 * @param {} texture
 * @returns {THREE.Mesh}
 */
GZ3D.Scene.prototype.createRoads = function(points, width, texture) {
  var geometry = new THREE.Geometry();
  geometry.dynamic = true;
  var texCoord = 0.0;
  var texMaxLen = width;
  var factor = 1.0;
  var curLen = 0.0;
  var tangent = new THREE.Vector3(0, 0, 0);
  var pA;
  var pB;
  var prevPt = new THREE.Vector3(0, 0, 0);
  var prevTexCoord;
  var texCoords = [];
  var j = 0;
  for (var i = 0; i < points.length; ++i) {
    var pt0 = new THREE.Vector3(points[i].x, points[i].y, points[i].z);
    var pt1;
    if (i !== points.length - 1) {
      pt1 = new THREE.Vector3(
        points[i + 1].x,
        points[i + 1].y,
        points[i + 1].z
      );
    }
    factor = 1.0;
    if (i > 0) {
      curLen += pt0.distanceTo(prevPt);
    }
    texCoord = curLen / texMaxLen;
    if (i === 0) {
      tangent.x = pt1.x;
      tangent.y = pt1.y;
      tangent.z = pt1.z;
      tangent.sub(pt0);
      tangent.normalize();
    } else if (i === points.length - 1) {
      tangent.x = pt0.x;
      tangent.y = pt0.y;
      tangent.z = pt0.z;
      tangent.sub(prevPt);
      tangent.normalize();
    } else {
      var v0 = new THREE.Vector3(0, 0, 0);
      var v1 = new THREE.Vector3(0, 0, 0);
      v0.x = pt0.x;
      v0.y = pt0.y;
      v0.z = pt0.z;
      v0.sub(prevPt);
      v0.normalize();

      v1.x = pt1.x;
      v1.y = pt1.y;
      v1.z = pt1.z;
      v1.sub(pt0);
      v1.normalize();

      var dot = v0.dot(v1 * -1);

      tangent.x = pt1.x;
      tangent.y = pt1.y;
      tangent.z = pt1.z;
      tangent.sub(prevPt);
      tangent.normalize();

      if (dot > -0.97 && dot < 0.97) {
        factor = 1.0 / Math.sin(Math.acos(dot) * 0.5);
      }
    }
    var theta = Math.atan2(tangent.x, -tangent.y);
    pA = new THREE.Vector3(pt0.x, pt0.y, pt0.z);
    pB = new THREE.Vector3(pt0.x, pt0.y, pt0.z);
    var w = width * factor * 0.5;
    pA.x += Math.cos(theta) * w;
    pA.y += Math.sin(theta) * w;
    pB.x -= Math.cos(theta) * w;
    pB.y -= Math.sin(theta) * w;

    geometry.vertices.push(pA);
    geometry.vertices.push(pB);

    texCoords.push([0, texCoord]);
    texCoords.push([1, texCoord]);

    // draw triangle strips
    if (i > 0) {
      geometry.faces.push(
        new THREE.Face3(j, j + 1, j + 2, new THREE.Vector3(0, 0, 1))
      );
      geometry.faceVertexUvs[0].push([
        new THREE.Vector2(texCoords[j][0], texCoords[j][1]),
        new THREE.Vector2(texCoords[j + 1][0], texCoords[j + 1][1]),
        new THREE.Vector2(texCoords[j + 2][0], texCoords[j + 2][1])
      ]);
      j++;

      geometry.faces.push(
        new THREE.Face3(j, j + 2, j + 1, new THREE.Vector3(0, 0, 1))
      );
      geometry.faceVertexUvs[0].push([
        new THREE.Vector2(texCoords[j][0], texCoords[j][1]),
        new THREE.Vector2(texCoords[j + 2][0], texCoords[j + 2][1]),
        new THREE.Vector2(texCoords[j + 1][0], texCoords[j + 1][1])
      ]);
      j++;
    }

    prevPt.x = pt0.x;
    prevPt.y = pt0.y;
    prevPt.z = pt0.z;

    prevTexCoord = texCoord;
  }

  // geometry.computeTangents();
  geometry.computeFaceNormals();

  geometry.verticesNeedUpdate = true;
  geometry.uvsNeedUpdate = true;

  var material = new THREE.MeshPhongMaterial();

  /* var ambient = mat['ambient'];
  if (ambient)
  {
    material.emissive.setRGB(ambient[0], ambient[1], ambient[2]);
  }
  var diffuse = mat['diffuse'];
  if (diffuse)
  {
    material.color.setRGB(diffuse[0], diffuse[1], diffuse[2]);
  }
  var specular = mat['specular'];
  if (specular)
  {
    material.specular.setRGB(specular[0], specular[1], specular[2]);
  }*/
  if (texture) {
    var tex = THREE.ImageUtils.loadTexture(texture);
    tex.wrapS = tex.wrapT = THREE.RepeatWrapping;
    material.map = tex;
  }

  var mesh = new THREE.Mesh(geometry, material);
  mesh.castShadow = false;
  return mesh;
};

/**
 * findLightIntensityInfo
 * Return min/max/ratio light intensity by combining all lights values
 */

GZ3D.Scene.prototype.findLightIntensityInfo = function() {
  var lights = [];
  this.scene.traverse(function(node) {
    if (node instanceof THREE.Light && !(node instanceof THREE.AmbientLight)) {
      lights.push(node);
    }
  });

  var info = { min: 100.0, max: -100.0 };
  var numberOfLights = lights.length;

  for (var i = 0; i < numberOfLights; i += 1) {
    var entity = this.getByName(lights[i].name);
    var intensity =
      (lights[i].color.r + lights[i].color.g + lights[i].color.b) / 3.0;

    if (intensity < info.min) {
      info.min = intensity;
    }

    if (intensity > info.max) {
      info.max = intensity;
    }
  }

  return info;
};

/**
 * Load heightmap
 * @param {} heights
 * @param {} width
 * @param {} height
 * @param {} segmentWidth
 * @param {} segmentHeight
 * @param {} textures
 * @param {} blends
 * @param {} parent
 */
GZ3D.Scene.prototype.loadHeightmap = function(
  heights,
  width,
  height,
  segmentWidth,
  segmentHeight,
  origin,
  textures,
  blends,
  parent
) {
  if (this.heightmap) {
    return;
  }
  // unfortunately large heightmaps kills the fps and freeze everything so
  // we have to scale it down
  var scale = 1;
  var maxHeightmapWidth = 256;
  var maxHeightmapHeight = 256;

  if (segmentWidth - 1 > maxHeightmapWidth) {
    scale = maxHeightmapWidth / (segmentWidth - 1);
  }

  var geometry = new THREE.PlaneGeometry(
    width,
    height,
    (segmentWidth - 1) * scale,
    (segmentHeight - 1) * scale
  );
  geometry.dynamic = true;

  // flip the heights
  var vertices = [];
  for (var h = segmentHeight - 1; h >= 0; --h) {
    for (var w = 0; w < segmentWidth; ++w) {
      vertices[(segmentHeight - h - 1) * segmentWidth + w] =
        heights[h * segmentWidth + w];
    }
  }

  // sub-sample
  var col = (segmentWidth - 1) * scale;
  var row = (segmentHeight - 1) * scale;
  for (var r = 0; r < row; ++r) {
    for (var c = 0; c < col; ++c) {
      var index = r * col * 1 / (scale * scale) + c * (1 / scale);
      geometry.vertices[r * col + c].z = vertices[index];
    }
  }

  var mesh;
  if (textures && textures.length > 0) {
    geometry.computeFaceNormals();
    geometry.computeVertexNormals();
    geometry.computeTangents();

    var textureLoaded = [];
    var repeats = [];
    for (var t = 0; t < textures.length; ++t) {
      textureLoaded[t] = THREE.ImageUtils.loadTexture(
        textures[t].diffuse,
        new THREE.UVMapping()
      );
      textureLoaded[t].wrapS = THREE.RepeatWrapping;
      textureLoaded[t].wrapT = THREE.RepeatWrapping;
      repeats[t] = width / textures[t].size;
    }

    // for now, use fixed no. of textures and blends
    // so populate the remaining ones to make the fragment shader happy
    for (var tt = textures.length; tt < 3; ++tt) {
      textureLoaded[tt] = textureLoaded[tt - 1];
    }

    for (var b = blends.length; b < 2; ++b) {
      blends[b] = blends[b - 1];
    }

    for (var rr = repeats.length; rr < 3; ++rr) {
      repeats[rr] = repeats[rr - 1];
    }

    // Use the same approach as gazebo scene, grab the first directional light
    // and use it for shading the terrain
    var lightDir = new THREE.Vector3(0, 0, 1);
    var lightDiffuse = new THREE.Color(0xffffff);
    var allObjects = [];
    this.scene.getDescendants(allObjects);
    for (var l = 0; l < allObjects.length; ++l) {
      if (allObjects[l] instanceof THREE.DirectionalLight) {
        lightDir = allObjects[l].target.position;
        lightDiffuse = allObjects[l].color;
        break;
      }
    }

    var material = new THREE.ShaderMaterial({
      uniforms: {
        texture0: { type: 't', value: textureLoaded[0] },
        texture1: { type: 't', value: textureLoaded[1] },
        texture2: { type: 't', value: textureLoaded[2] },
        repeat0: { type: 'f', value: repeats[0] },
        repeat1: { type: 'f', value: repeats[1] },
        repeat2: { type: 'f', value: repeats[2] },
        minHeight1: { type: 'f', value: blends[0].min_height },
        fadeDist1: { type: 'f', value: blends[0].fade_dist },
        minHeight2: { type: 'f', value: blends[1].min_height },
        fadeDist2: { type: 'f', value: blends[1].fade_dist },
        ambient: { type: 'c', value: this.ambient.color },
        lightDiffuse: { type: 'c', value: lightDiffuse },
        lightDir: { type: 'v3', value: lightDir }
      },
      attributes: {},
      vertexShader: document.getElementById('heightmapVS').innerHTML,
      fragmentShader: document.getElementById('heightmapFS').innerHTML
    });

    mesh = new THREE.Mesh(geometry, material);
  } else {
    mesh = new THREE.Mesh(
      geometry,
      new THREE.MeshPhongMaterial({ color: 0x555555 })
    );
  }

  mesh.position.x = origin.x;
  mesh.position.y = origin.y;
  mesh.position.z = origin.z;
  parent.add(mesh);

  this.heightmap = parent;
};

/**
 * Load mesh
 * @param {string} uri
 * @param {} submesh
 * @param {} centerSubmesh
 * @param {function} callback
 */
GZ3D.Scene.prototype.loadMesh = function(
  uri,
  submesh,
  centerSubmesh,
  callback,
  progressCallback
) {
  var uriPath = uri.substring(0, uri.lastIndexOf('/'));
  var uriFile = uri.substring(uri.lastIndexOf('/') + 1);

  // load collada model
  if (uriFile.substr(-4).toLowerCase() === '.dae') {
    return this.loadCollada(
      uri,
      submesh,
      centerSubmesh,
      callback,
      progressCallback
    );
  }
  // load urdf model
  else if (uriFile.substr(-5).toLowerCase() === '.urdf') {
    /*var urdfModel = new ROSLIB.UrdfModel({
      string : uri
    });

    // adapted from ros3djs
    var links = urdfModel.links;
    for ( var l in links) {
      var link = links[l];
      if (link.visual && link.visual.geometry) {
        if (link.visual.geometry.type === ROSLIB.URDF_MESH) {
          var frameID = '/' + link.name;
          var filename = link.visual.geometry.filename;
          var meshType = filename.substr(-4).toLowerCase();
          var mesh = filename.substring(filename.indexOf('://') + 3);
          // ignore mesh files which are not in Collada format
          if (meshType === '.dae')
          {
            var dae = this.loadCollada(uriPath + '/' + mesh, parent);
            // check for a scale
            if(link.visual.geometry.scale)
            {
              dae.scale = new THREE.Vector3(
                  link.visual.geometry.scale.x,
                  link.visual.geometry.scale.y,
                  link.visual.geometry.scale.z
              );
            }
          }
        }
      }
    }*/
  }
};

GZ3D.Scene.prototype.preparePBRMaterials = function(basePath, collada) {
  var libEffects = collada.library.effects;
  var libMaterials = collada.library.materials;
  var libImages = collada.library.images;

  function getTexturePath(textureObject, effect) {
    var sampler = effect.profile.samplers[textureObject.id];
    var surface = effect.profile.surfaces[sampler.source];

    if (sampler !== undefined && surface !== null) {
      var img = libImages[surface.init_from];

      return basePath + '/' + img.build;
    }
  }

  for (const materialKey in libMaterials) {
    var material = libMaterials[materialKey];
    var effect = libEffects[material.url];

    var parameters = effect.profile.technique.parameters;
    var parameter;
    var pbrMaterial = null;

    var pbrKeyToThreeJSMap = {
      Base_Color: 'map',
      Metallic: 'metalnessMap',
      Roughness: 'roughnessMap',
      Mixed_AO: 'aoMap',
      Emissive: 'emissiveMap',
      Normal: 'normalMap',
      Height: 'displacementMap'
    };

    for (var key in parameters) {
      parameter = parameters[key];
      if (parameter.texture) {
        var texturePath = getTexturePath(parameter.texture, effect);

        if (
          texturePath &&
          (texturePath.indexOf('PBR_') >= 0 ||
            texturePath.indexOf('PBRFULL_') >= 0)
        ) {
          for (var k in pbrKeyToThreeJSMap) {
            if (texturePath.indexOf(k) >= 0) {
              if (pbrMaterial === null) {
                pbrMaterial = {};
              }

              pbrMaterial[pbrKeyToThreeJSMap[k]] = texturePath;

              if (
                texturePath.indexOf('PBRFULL_') >= 0 &&
                k === 'Base_Color'
              ) {
                // If the prefix is PBRFULL it means that all the PBR textures are present
                // but that only the basic color map have been linked to the dae.

                var allpbrkeywords = [
                  'Metallic',
                  'Mixed_AO',
                  'Roughness',
                  'Normal',
                  'Roughness'
                ];

                for (var pbri in allpbrkeywords) {
                  var pbrk = allpbrkeywords[pbri];
                  var path = texturePath;

                  path = path.replace('Base_Color', pbrk);
                  if (pbrk === 'Normal') {
                    // Normal map must be png. In case the Base_Color is a jpg,
                    // change extension to png

                    path = path.replace('.jpg', '.png');
                    path = path.replace('.jpeg', '.png');
                  }

                  pbrMaterial[pbrKeyToThreeJSMap[pbrk]] = path;
                }
              }
            }
          }
        }
      }
    }

    if (pbrMaterial !== null) {
      material.build.pbrMaterialDescription = pbrMaterial; // If PBR material has been found, store it for future usage
    }
  }
};

/**
 * Load collada file
 * @param {string} uri
 * @param {} submesh
 * @param {} centerSubmesh
 * @param {function} callback
 */
GZ3D.Scene.prototype.loadCollada = function(
  uri,
  submesh,
  centerSubmesh,
  callback,
  progressCallback
) {
  var dae;

  var cachedModel = this.cachedModels[uri];
  if (cachedModel && cachedModel.referenceObject) {
    dae = cachedModel.referenceObject.clone();
    this.useColladaSubMesh(dae, submesh, centerSubmesh);
    callback(dae);
    return;
  }

  var loader = new THREE.ColladaLoader();
  loader.textureLoadedCallback = () => {
    this.refresh3DViews();
  };
  var basePath = uri.substring(0, uri.lastIndexOf('/'));
  var thatSubmesh = submesh;

  var that = this;
  loader.load(
    uri,
    function(collada) {
      // check for a scale factor
      /*if(collada.dae.asset.unit) {
        var scale = collada.dae.asset.unit;
        collada.scene.scale = new THREE.Vector3(scale, scale, scale);
      }*/

      dae = collada.scene;
      dae.updateMatrix();
      that.prepareColladaMesh(dae);
      that.useColladaSubMesh(dae, thatSubmesh, centerSubmesh);
      that.preparePBRMaterials(basePath, collada);

      // updating ColladaLoader to be able to load model specs v1.4.1 introduced a change in rotations
      // automatic conversion of up-axis as claimed by ColladaLoader doesn't seem to work in our case
      // in case something other than Y_UP is specified (->ColladaLoader will return a rotation other than Euler(0,0,0)),
      // we apply a rotation fix
      if (dae.rotation.x !== 0 || dae.rotation.y !== 0 || dae.rotation.z !== 0) {
        dae.quaternion.premultiply(new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(1,0,0), Math.PI / 2));
      }

      dae.name = uri;
      callback(dae);
    },
    function(progress) {
      if (progressCallback !== undefined) {
        progress.error = false;
        progressCallback(progress);
      }
    },
    function() {
      if (progressCallback !== undefined) {
        progressCallback({ total: 0, loaded: 0, error: true });
      }
    }
  );
};

/**
 * Prepare collada by removing other non-mesh entities such as lights
 * @param {} dae
 */
GZ3D.Scene.prototype.prepareColladaMesh = function(dae) {
  var allChildren = [];
  dae.getDescendants(allChildren);
  for (var i = 0; i < allChildren.length; ++i) {
    if (allChildren[i] instanceof THREE.Light) {
      allChildren[i].parent.remove(allChildren[i]);
    }
  }
};

/**
 * Prepare collada by handling submesh-only loading
 * @param {} dae
 * @param {} submesh
 * @param {} centerSubmesh
 * @returns {THREE.Mesh} mesh
 */
GZ3D.Scene.prototype.useColladaSubMesh = function(dae, submesh, centerSubmesh) {
  if (!submesh) {
    return null;
  }

  var mesh;
  var allChildren = [];
  dae.getDescendants(allChildren);
  for (var i = 0; i < allChildren.length; ++i) {
    if (allChildren[i] instanceof THREE.Mesh) {
      if (!submesh && !mesh) {
        mesh = allChildren[i];
      }

      if (submesh) {
        if (allChildren[i].geometry.name === submesh) {
          if (centerSubmesh) {
            var vertices = allChildren[i].geometry.vertices;
            var vMin = new THREE.Vector3();
            var vMax = new THREE.Vector3();
            vMin.x = vertices[0].x;
            vMin.y = vertices[0].y;
            vMin.z = vertices[0].z;
            vMax.x = vMin.x;
            vMax.y = vMin.y;
            vMax.z = vMin.z;

            for (var j = 1; j < vertices.length; ++j) {
              vMin.x = Math.min(vMin.x, vertices[j].x);
              vMin.y = Math.min(vMin.y, vertices[j].y);
              vMin.z = Math.min(vMin.z, vertices[j].z);
              vMax.x = Math.max(vMax.x, vertices[j].x);
              vMax.y = Math.max(vMax.y, vertices[j].y);
              vMax.z = Math.max(vMax.z, vertices[j].z);
            }

            var center = new THREE.Vector3();
            center.x = vMin.x + 0.5 * (vMax.x - vMin.x);
            center.y = vMin.y + 0.5 * (vMax.y - vMin.y);
            center.z = vMin.z + 0.5 * (vMax.z - vMin.z);

            for (var k = 0; k < vertices.length; ++k) {
              vertices[k].x -= center.x;
              vertices[k].y -= center.y;
              vertices[k].z -= center.z;
            }
            allChildren[i].geometry.verticesNeedUpdate = true;

            allChildren[i].position.x = 0;
            allChildren[i].position.y = 0;
            allChildren[i].position.z = 0;

            allChildren[i].parent.position.x = 0;
            allChildren[i].parent.position.y = 0;
            allChildren[i].parent.position.z = 0;
          }
          mesh = allChildren[i];
        } else {
          allChildren[i].parent.remove(allChildren[i]);
        }
      }
    }
  }
  return mesh;
};

/*GZ3D.Scene.prototype.setMaterial = function(mesh, texture, normalMap)
{
  if (!mesh)
  {
    return;
  }

  if (texture || normalMap)
  {
    // normal map shader
    var shader = THREE.ShaderLib['normalmap'];
    var uniforms = THREE.UniformsUtils.clone( shader.uniforms );
    if (texture)
    {
      uniforms['enableDiffuse'].value = true;
      uniforms['tDiffuse'].value = THREE.ImageUtils.loadTexture(texture);
    }
    if (normalMap)
    {
      uniforms['tNormal'].value = THREE.ImageUtils.loadTexture(normalMap);
    }

    var parameters = { fragmentShader: shader.fragmentShader,
        vertexShader: shader.vertexShader, uniforms: uniforms,
        lights: true, fog: false };
    var shaderMaterial = new THREE.ShaderMaterial(parameters);
    mesh.geometry.computeTangents();
    mesh.material = shaderMaterial;
  }
};*/

/**
 * Set material for an object
 * @param {} obj
 * @param {} material
 */
GZ3D.Scene.prototype.setMaterial = function(obj, material) {
  if (obj) {
    if (obj instanceof THREE.Group) {
      var that = this;

      obj.traverse(function(node) {
        if (!(node instanceof THREE.Group)) {
          that.setMaterial(node, material);
        }
      });

      return;
    }

    if (material) {
      obj.material = new THREE.MeshPhongMaterial();
      var emissive = material.emissive;
      if (emissive) {
        obj.material.emissive.setRGB(emissive[0], emissive[1], emissive[2]);
      }
      var diffuse = material.diffuse;
      if (diffuse) {
        obj.material.color.setRGB(diffuse[0], diffuse[1], diffuse[2]);
      }
      var specular = material.specular;
      if (specular) {
        obj.material.specular.setRGB(specular[0], specular[1], specular[2]);
      }
      var opacity = material.opacity;
      if (opacity) {
        if (opacity < 1) {
          obj.material.transparent = true;
          obj.material.opacity = opacity;
        }
      }

      if (material.texture) {
        var texture = THREE.ImageUtils.loadTexture(material.texture);
        if (material.scale) {
          texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
          texture.repeat.x = 1.0 / material.scale[0];
          texture.repeat.y = 1.0 / material.scale[1];
        }
        obj.material.map = texture;
      }
      if (material.normalMap) {
        obj.material.normalMap = THREE.ImageUtils.loadTexture(
          material.normalMap
        );
      }
    }

    this.updateDynamicEnvMap();
    this.refresh3DViews();
  }
};

/**
 * Set manipulation mode (view/translate/rotate/scale)
 * @param {string} mode
 */
GZ3D.Scene.prototype.setManipulationMode = function(mode) {
  var previousMode = this.manipulationMode;
  this.manipulationMode = mode;

  if (mode === 'view' || mode === 'natural') {
    if (this.modelManipulator.object) {
      this.emitter.emit('entityChanged', this.modelManipulator.object);
    }

    if (mode === 'view' && previousMode === 'natural') {
      if (this.selectedEntity) {
        this.showBoundingBox(this.selectedEntity);
      }
    } else {
      this.selectEntity(null);
    }
  } else {
    this.modelManipulator.mode = this.manipulationMode;
    this.modelManipulator.setMode(this.modelManipulator.mode);
  }

  this.refresh3DViews();
};

/**
 * Show collision visuals
 * @param {boolean} show
 */
GZ3D.Scene.prototype.showCollision = function(show) {
  if (show === this.showCollisions) {
    return;
  }

  var allObjects = [];
  this.scene.getDescendants(allObjects);
  for (var i = 0; i < allObjects.length; ++i) {
    if (
      allObjects[i] instanceof THREE.Object3D &&
      allObjects[i].name.indexOf('COLLISION_VISUAL') >= 0
    ) {
      var allChildren = [];
      allObjects[i].getDescendants(allChildren);
      for (var j = 0; j < allChildren.length; ++j) {
        if (allChildren[j] instanceof THREE.Mesh) {
          allChildren[j].visible = show;
        }
      }
    }
  }
  this.showCollisions = show;
  this.refresh3DViews();
};

/**
 * Attach manipulator to an object
 * @param {THREE.Object3D} model
 * @param {string} mode (translate/rotate/scale)
 */
GZ3D.Scene.prototype.attachManipulator = function(model, mode) {
  if (this.modelManipulator.object) {
    this.emitter.emit('entityChanged', this.modelManipulator.object);
  }

  if (mode !== 'view') {
    this.modelManipulator.attach(model);
    this.modelManipulator.mode = mode;
    this.modelManipulator.setMode(this.modelManipulator.mode);
    this.scene.add(this.modelManipulator.gizmo);
  }
};

/**
 * Reset view
 */
GZ3D.Scene.prototype.resetView = function() {
  this.camera.position.copy(this.defaultCameraPosition);
  this.camera.up = new THREE.Vector3(0, 0, 1);
  this.camera.lookAt(this.defaultCameraLookAt);
  this.camera.updateMatrix();
  this.refresh3DViews();
};

/**
 * Set the camera pose and the default value (position and LookAt) and update the projection matrix
 * @param {Float} xPos - x coordinate (camera position)
 * @param {Float} yPos - y coordinate (camera position)
 * @param {Float} zPos - z coordinate (camera position)
 * @param {Float} xLookAt - x coordinate (LookAt position)
 * @param {Float} yLookAt - y coordinate (LookAt position)
 * @param {Float} zLookAt - z coordinate (LookAt position)
 */
GZ3D.Scene.prototype.setDefaultCameraPose = function(
  xPos,
  yPos,
  zPos,
  xLookAt,
  yLookAt,
  zLookAt
) {
  this.defaultCameraPosition = new THREE.Vector3(xPos, yPos, zPos);
  this.defaultCameraLookAt = new THREE.Vector3(xLookAt, yLookAt, zLookAt);
  this.resetView();
};

/**
 * Set the camera pose (position and LookAt) and update the projection matrix
 * @param {Float} xPos - x coordinate (camera position)
 * @param {Float} yPos - y coordinate (camera position)
 * @param {Float} zPos - z coordinate (camera position)
 * @param {Float} xLookAt - x coordinate (LookAt position)
 * @param {Float} yLookAt - y coordinate (LookAt position)
 * @param {Float} zLookAt - z coordinate (LookAt position)
 */
GZ3D.Scene.prototype.setCameraPose = function(
  xPos,
  yPos,
  zPos,
  xLookAt,
  yLookAt,
  zLookAt
) {
  this.camera.position = new THREE.Vector3(xPos, yPos, zPos);
  this.camera.lookAt(new THREE.Vector3(xLookAt, yLookAt, zLookAt));
  this.camera.updateMatrix();
  this.refresh3DViews();
};

/**
 * Show radial menu
 * @param {} event
 */
GZ3D.Scene.prototype.showRadialMenu = function(e) {
  var event = e.originalEvent;

  var pointer = event.touches ? event.touches[0] : event;
  var pos = new THREE.Vector2(pointer.offsetX, pointer.offsetY);

  var intersect = new THREE.Vector3();
  var model = this.getRayCastModel(pos, intersect);

  if (
    model &&
    model.name !== '' &&
    model.name !== 'plane' &&
    this.modelManipulator.pickerNames.indexOf(model.name) === -1
  ) {
    this.radialMenu.show(event, model);
    this.selectEntity(model);
  }
};

/**
 * Update bounding box for a model. The box is aligned with the world.
 * @param {THREE.Object3D} model
 */
GZ3D.Scene.prototype.updateBoundingBox = function(model) {
  if (typeof model === 'string') {
    model = this.scene.getObjectByName(model);
  }

  if (
    this.boundingBox.visible &&
    model === this.boundingBox.boundingBoxObject
  ) {
    var box = new THREE.Box3();
    var prevAngle = model.rotation.clone();
    var prevPos = model.position.clone();
    model.rotation.copy(new THREE.Euler(0, 0, 0, 'XYZ'));
    model.position.copy(new THREE.Vector3(0, 0, 0));
    model.updateMatrixWorld();
    box.setFromObject(model);
    model.rotation.copy(prevAngle);
    model.position.copy(prevPos);
    this.boundingBox.rotation.copy(prevAngle);
    this.boundingBox.position.copy(prevPos);
    model.updateMatrixWorld();
    this.boundingBox.updateMatrixWorld();

    var vertex = new THREE.Vector3(box.max.x, box.max.y, box.max.z); // 0
    this.boundingBox.geometry.vertices[0].copy(vertex);
    this.boundingBox.geometry.vertices[7].copy(vertex);
    this.boundingBox.geometry.vertices[16].copy(vertex);

    vertex.set(box.min.x, box.max.y, box.max.z); // 1
    this.boundingBox.geometry.vertices[1].copy(vertex);
    this.boundingBox.geometry.vertices[2].copy(vertex);
    this.boundingBox.geometry.vertices[18].copy(vertex);

    vertex.set(box.min.x, box.min.y, box.max.z); // 2
    this.boundingBox.geometry.vertices[3].copy(vertex);
    this.boundingBox.geometry.vertices[4].copy(vertex);
    this.boundingBox.geometry.vertices[20].copy(vertex);

    vertex.set(box.max.x, box.min.y, box.max.z); // 3
    this.boundingBox.geometry.vertices[5].copy(vertex);
    this.boundingBox.geometry.vertices[6].copy(vertex);
    this.boundingBox.geometry.vertices[22].copy(vertex);

    vertex.set(box.max.x, box.max.y, box.min.z); // 4
    this.boundingBox.geometry.vertices[8].copy(vertex);
    this.boundingBox.geometry.vertices[15].copy(vertex);
    this.boundingBox.geometry.vertices[17].copy(vertex);

    vertex.set(box.min.x, box.max.y, box.min.z); // 5
    this.boundingBox.geometry.vertices[9].copy(vertex);
    this.boundingBox.geometry.vertices[10].copy(vertex);
    this.boundingBox.geometry.vertices[19].copy(vertex);

    vertex.set(box.min.x, box.min.y, box.min.z); // 6
    this.boundingBox.geometry.vertices[11].copy(vertex);
    this.boundingBox.geometry.vertices[12].copy(vertex);
    this.boundingBox.geometry.vertices[21].copy(vertex);

    vertex.set(box.max.x, box.min.y, box.min.z); // 7
    this.boundingBox.geometry.vertices[13].copy(vertex);
    this.boundingBox.geometry.vertices[14].copy(vertex);
    this.boundingBox.geometry.vertices[23].copy(vertex);

    this.boundingBox.geometry.verticesNeedUpdate = true;
  }
};

/**
 * Show bounding box for a model. The box is aligned with the world.
 * @param {THREE.Object3D} model
 */
GZ3D.Scene.prototype.showBoundingBox = function(model) {
  if (typeof model === 'string') {
    model = this.scene.getObjectByName(model);
  }

  if (this.boundingBox.visible) {
    if (this.boundingBox.boundingBoxObject === model) {
      return;
    } else {
      this.hideBoundingBox();
    }
  }

  this.boundingBox.name = 'boundingBox';
  this.boundingBox.visible = true;

  this.boundingBox.boundingBoxObject = model;

  this.updateBoundingBox(model);

  // Add box as model's child
  this.scene.add(this.boundingBox);
  this.refresh3DViews();
};

/**
 * Hide bounding box
 */
GZ3D.Scene.prototype.hideBoundingBox = function() {
  if (this.boundingBox.boundingBoxObject) {
    this.scene.remove(this.boundingBox);
    this.boundingBox.boundingBoxObject = undefined;
  }
  this.boundingBox.visible = false;
  this.refresh3DViews();
};

/**
 * Mouse right click
 * @param {} event
 * @param {} callback - function to be executed to the clicked model
 */
GZ3D.Scene.prototype.onRightClick = function(event, callback) {
  var pos = new THREE.Vector2(event.offsetX, event.offsetY);
  var model = this.getRayCastModel(pos, new THREE.Vector3());

  if (
    model &&
    model.name !== '' &&
    model.name !== 'plane' &&
    this.modelManipulator.pickerNames.indexOf(model.name) === -1
  ) {
    callback(model);
  }
  this.refresh3DViews();
};

/**
 * Set model's view mode
 * @param {} model
 * @param {} viewAs (normal/transparent/wireframe)
 */
GZ3D.Scene.prototype.setViewAs = function(model, viewAs) {
  function materialViewAs(material) {
    if (!material.originalOpacity) {
      material.originalOpacity = material.opacity;
    }

    if (materials.indexOf(material.id) === -1) {
      materials.push(material.id);

      if (viewAs === 'transparent') {
        material.opacity = 0.4;
      } else {
        // normal or wireframe
        material.opacity = material.originalOpacity
          ? material.originalOpacity
          : 1.0;
      }
      material.transparent = material.opacity < 1.0;
    }
  }

  var descendants = [];
  var materials = [];
  model.getDescendants(descendants);
  for (var i = 0; i < descendants.length; ++i) {
    if (
      descendants[i].material &&
      descendants[i].name.indexOf('boundingBox') === -1 &&
      descendants[i].name.indexOf('COLLISION_VISUAL') === -1 &&
      descendants[i].name.indexOf('LABEL_INFO_VISUAL') === -1 &&
      !this.getParentByPartialName(descendants[i], 'COLLISION_VISUAL') &&
      !this.getParentByPartialName(descendants[i], 'LABEL_INFO_VISUAL') &&
      descendants[i].name.indexOf('wireframe') === -1 &&
      descendants[i].name.indexOf('JOINT_VISUAL') === -1
    ) {
      if (descendants[i].material instanceof THREE.MeshFaceMaterial) {
        for (var j = 0; j < descendants[i].material.materials.length; ++j) {
          materialViewAs(descendants[i].material.materials[j]);
        }
      } else {
        materialViewAs(descendants[i].material);
      }

      // wireframe handling
      var showWireframe = viewAs === 'wireframe';
      if (descendants[i].material instanceof THREE.MeshFaceMaterial) {
        for (
          var m = 0;
          m < descendants[i].material.materials.length;
          m = m + 1
        ) {
          descendants[i].material.materials[m].wireframe = showWireframe;
        }
      } else {
        descendants[i].material.wireframe = showWireframe;
      }
    }
  }
  model.viewAs = viewAs;
  this.refresh3DViews();
};

/**
 * Returns the closest parent whose name contains the given string
 * @param {} object
 * @param {} name
 */
GZ3D.Scene.prototype.getParentByPartialName = function(object, name) {
  var parent = object.parent;
  while (parent && parent !== this.scene) {
    if (parent.name.indexOf(name) !== -1) {
      return parent;
    }

    parent = parent.parent;
  }
  return null;
};

/**
 * Select entity
 * @param {} object
 */
GZ3D.Scene.prototype.selectEntity = function(object) {
  if (this.lockSelectedIdentity) {
    return;
  }

  if (object) {
    var animatedExt = '_animated';

    if (
      object.name.indexOf(animatedExt) < 0 ||
      object.name.indexOf(animatedExt) !==
        object.name.length - animatedExt.length
    ) {
      if (object !== this.selectedEntity) {
        this.showBoundingBox(object);
        this.selectedEntity = object;
      }

      //when scaling and object is not a simple shape, remove manipulator and switch to view mode
      if (this.manipulationMode === 'scale' && !object.isSimpleShape()) {
        this.modelManipulator.detach();
        this.scene.remove(this.modelManipulator.gizmo);
        this.manipulationMode = 'view';
      } else if (this.manipulationMode !== 'natural') {
        this.attachManipulator(object, this.manipulationMode);
      }

      guiEvents.emit('setTreeSelected', object.name);
    }
  } else {
    if (this.modelManipulator.object) {
      this.modelManipulator.detach();
      this.scene.remove(this.modelManipulator.gizmo);
    }
    this.hideBoundingBox();
    this.selectedEntity = null;
    guiEvents.emit('setTreeDeselected');

    this.refresh3DViews();
  }
};

/**
 * View joints
 * Toggle: if there are joints, hide, otherwise, show.
 * @param {} model
 */
GZ3D.Scene.prototype.viewJoints = function(model) {
  if (model.joint === undefined || model.joint.length === 0) {
    return;
  }

  var child;

  // Visuals already exist
  if (model.jointVisuals) {
    // Hide = remove from parent
    if (model.jointVisuals[0].parent !== undefined) {
      for (var v = 0; v < model.jointVisuals.length; ++v) {
        model.jointVisuals[v].parent.remove(model.jointVisuals[v]);
      }
    } else {
      // Show: attach to parent
      for (var s = 0; s < model.joint.length; ++s) {
        child = model.getObjectByName(model.joint[s].child);

        if (!child) {
          continue;
        }

        child.add(model.jointVisuals[s]);
      }
    }
  } else {
    // Create visuals
    model.jointVisuals = [];
    for (var j = 0; j < model.joint.length; ++j) {
      child = model.getObjectByName(model.joint[j].child);

      if (!child) {
        continue;
      }

      // XYZ expressed w.r.t. child
      var jointVisual = this.jointAxis['XYZaxes'].clone();
      child.add(jointVisual);
      model.jointVisuals.push(jointVisual);
      jointVisual.scale.set(0.7, 0.7, 0.7);

      this.setPose(
        jointVisual,
        model.joint[j].pose.position,
        model.joint[j].pose.orientation
      );

      var mainAxis;
      if (model.joint[j].type !== this.jointTypes.BALL) {
        mainAxis = this.jointAxis['mainAxis'].clone();
        jointVisual.add(mainAxis);
      }

      var secondAxis;
      if (
        model.joint[j].type === this.jointTypes.REVOLUTE2 ||
        model.joint[j].type === this.jointTypes.UNIVERSAL
      ) {
        secondAxis = this.jointAxis['mainAxis'].clone();
        jointVisual.add(secondAxis);
      }

      if (
        model.joint[j].type === this.jointTypes.REVOLUTE ||
        model.joint[j].type === this.jointTypes.GEARBOX
      ) {
        mainAxis.add(this.jointAxis['rotAxis'].clone());
      } else if (
        model.joint[j].type === this.jointTypes.REVOLUTE2 ||
        model.joint[j].type === this.jointTypes.UNIVERSAL
      ) {
        mainAxis.add(this.jointAxis['rotAxis'].clone());
        secondAxis.add(this.jointAxis['rotAxis'].clone());
      } else if (model.joint[j].type === this.jointTypes.BALL) {
        jointVisual.add(this.jointAxis['ballVisual'].clone());
      } else if (model.joint[j].type === this.jointTypes.PRISMATIC) {
        mainAxis.add(this.jointAxis['transAxis'].clone());
      } else if (model.joint[j].type === this.jointTypes.SCREW) {
        mainAxis.add(this.jointAxis['screwAxis'].clone());
      }

      var direction, tempMatrix, rotMatrix;
      if (mainAxis) {
        // main axis expressed w.r.t. parent model or joint frame
        // needs Gazebo issue #1268 fixed, receive use_parent_model_frame on msg
        // for now, true by default because most old models have it true
        if (model.joint[j].axis1.use_parent_model_frame === undefined) {
          model.joint[j].axis1.use_parent_model_frame = true;
        }

        direction = new THREE.Vector3(
          model.joint[j].axis1.xyz.x,
          model.joint[j].axis1.xyz.y,
          model.joint[j].axis1.xyz.z
        );
        direction.normalize();

        tempMatrix = new THREE.Matrix4();
        if (model.joint[j].axis1.use_parent_model_frame) {
          tempMatrix.extractRotation(jointVisual.matrix);
          tempMatrix.getInverse(tempMatrix);
          direction.applyMatrix4(tempMatrix);
          tempMatrix.extractRotation(child.matrix);
          tempMatrix.getInverse(tempMatrix);
          direction.applyMatrix4(tempMatrix);
        }

        mainAxis.position = direction.multiplyScalar(0.3);
        rotMatrix = new THREE.Matrix4();
        rotMatrix.lookAt(direction, new THREE.Vector3(0, 0, 0), mainAxis.up);
        mainAxis.quaternion.setFromRotationMatrix(rotMatrix);
      }

      if (secondAxis) {
        if (model.joint[j].axis2.use_parent_model_frame === undefined) {
          model.joint[j].axis2.use_parent_model_frame = true;
        }

        direction = new THREE.Vector3(
          model.joint[j].axis2.xyz.x,
          model.joint[j].axis2.xyz.y,
          model.joint[j].axis2.xyz.z
        );
        direction.normalize();

        tempMatrix = new THREE.Matrix4();
        if (model.joint[j].axis2.use_parent_model_frame) {
          tempMatrix.extractRotation(jointVisual.matrix);
          tempMatrix.getInverse(tempMatrix);
          direction.applyMatrix4(tempMatrix);
          tempMatrix.extractRotation(child.matrix);
          tempMatrix.getInverse(tempMatrix);
          direction.applyMatrix4(tempMatrix);
        }

        secondAxis.position = direction.multiplyScalar(0.3);
        rotMatrix = new THREE.Matrix4();
        rotMatrix.lookAt(direction, new THREE.Vector3(0, 0, 0), secondAxis.up);
        secondAxis.quaternion.setFromRotationMatrix(rotMatrix);
      }
    }
  }
  this.refresh3DViews();
};

/**
 * Update a light entity from a message
 * @param {} entity
 * @param {} msg
 */
GZ3D.Scene.prototype.updateLight = function(entity, msg) {
  // TODO: Generalize this and createLight
  var lightObj = entity.children[0];
  var dir;

  var color = new THREE.Color();

  if (msg.diffuse) {
    color.r = msg.diffuse.r;
    color.g = msg.diffuse.g;
    color.b = msg.diffuse.b;
    lightObj.color = color.clone();
  }
  if (msg.specular) {
    color.r = msg.specular.r;
    color.g = msg.specular.g;
    color.b = msg.specular.b;
    entity.serverProperties.specular = color.clone();
  }

  if (msg.pose) {
    this.setPose(entity, msg.pose.position, msg.pose.orientation);
    entity.matrixWorldNeedsUpdate = true;
  }

  if (msg.range) {
    // THREE.js's light distance impacts the attenuation factor defined in the shader:
    // attenuation factor = 1.0 - distance-to-enlighted-point / light.distance
    // Gazebo's range (taken from OGRE 3D API) does not contribute to attenuation;
    // it is a hard limit for light scope.
    // Nevertheless, we identify them for sake of simplicity.
    lightObj.distance = msg.range;
  }

  if (msg.cast_shadows) {
    lightObj.castShadow = msg.cast_shadows;
  }

  if (msg.attenuation_constant) {
    entity.serverProperties.attenuation_constant = msg.attenuation_constant;
  }
  if (msg.attenuation_linear) {
    entity.serverProperties.attenuation_linear = msg.attenuation_linear;
    lightObj.intensity = lightObj.intensity / (1 + msg.attenuation_linear);
  }
  if (msg.attenuation_quadratic) {
    entity.serverProperties.attenuation_quadratic = msg.attenuation_quadratic;
    lightObj.intensity = lightObj.intensity / (1 + msg.attenuation_quadratic);
  }
  if (msg.attenuation_linear && msg.attenuation_quadratic) {
    // equation taken from
    // http://wiki.blender.org/index.php/Doc:2.6/Manual/Lighting/Lights/Light_Attenuation
    var E = 1;
    var D = 1;
    var r = 1;
    var L = msg.attenuation_linear;
    var Q = msg.attenuation_quadratic;
    lightObj.intensity =
      E *
      (D / (D + L * r)) *
      (Math.pow(D, 2) / (Math.pow(D, 2) + Q * Math.pow(r, 2)));
  }

  if (lightObj instanceof THREE.SpotLight) {
    if (msg.spot_outer_angle) {
      lightObj.angle = msg.spot_outer_angle;
      lightObj.shadowCameraFov = 2 * msg.spot_outer_angle / Math.PI * 180;
    }
    if (msg.spot_falloff) {
      lightObj.exponent = msg.spot_falloff;
    }
  }

  this.refresh3DViews();
};

GZ3D.Scene.prototype.setShadowMaps = function(enabled) {
  this.viewManager.setShadowMaps(enabled);

  var that = this;
  this.scene.traverse(function(node) {
    if (enabled) {
      if (node.material) {
        node.material.needsUpdate = true;
        if (node.material.materials) {
          for (var i = 0; i < node.material.materials.length; i = i + 1) {
            node.material.materials[i].needsUpdate = true;
          }
        }
      }
    } else {
      if (node instanceof THREE.Light) {
        if (
          that.viewManager &&
          node.shadow !== undefined &&
          node.shadow.map !== undefined
        ) {
          that.viewManager.clearRenderTargets(node.shadow.map);
        }
      }
    }
  });

  this.refresh3DViews();
};

/**
 * Apply composer settings
 * Reflect the post-processing composer settings in the 3D scene.
 * @param updateColorCurve
 */

GZ3D.Scene.prototype.applyComposerSettings = function(
  updateColorCurve,
  forcePBRUpdate,
  shadowReset
) {
  this.composer.applyComposerSettings(
    updateColorCurve,
    forcePBRUpdate,
    shadowReset
  );
  this.needsImmediateUpdate = true;
};

/**
 * Force an update of the dynamic environment map
 */

GZ3D.Scene.prototype.updateDynamicEnvMap = function() {
  this.composer.cubeMapNeedsUpdate = true;
  this.needsImmediateUpdate = true;
  this.refresh3DViews();
};

/**
 * Apply composer settings to a specific model
 * Update a model with the post-processing composer settings
 * @param updateColorCurve
 */

GZ3D.Scene.prototype.applyComposerSettingsToModel = function(model) {
  this.composer.applyComposerSettingsToModel(model);
};

/**
 * Set master settings
 * Master settings can be used to changed the global quality of the scene.
 * It overrides the composer settings.
 * @param master settings
 */

GZ3D.Scene.prototype.setMasterSettings = function(
  masterSettings,
  applySettings
) {
  this.composer.setMasterSettings(masterSettings, applySettings);
  if (applySettings) {
    this.needsImmediateUpdate = true;
    this.refresh3DViews();
  }
};

/**
 * Check a specific setting is supported by the current master setting level
 *
 * @param composer setting
 */

GZ3D.Scene.prototype.isSupportedByMasterSetting = function(setting) {
  return this.composer.isSupportedByMasterSetting(setting);
};

/**
 * 3D views need to be refreshed
 *
 */

GZ3D.Scene.prototype.refresh3DViews = function() {
  for (var i = 0; i < this.viewManager.views.length; i++) {
    this.viewManager.views[i].needsRefresh = true;
  }
};

/**
 * Set a callback that is called when the scene preparation is done. The callback is called with null when everyhing is ready or with a progress message.
 *
 * @param callback
 */

GZ3D.Scene.prototype.setScenePreparationReadyCallback = function(
  sceneReadyCallback
) {
  this.composer.sceneReadyCallback = sceneReadyCallback;
};

/**
 * Show labels for sensors and links
 *
 */

GZ3D.Scene.prototype.setLabelInfoVisible = function(object, visible) {
  this.labelManager.setLabelInfoVisible(object, visible);

  if (visible) {
    this.hideBoundingBox();
  } else if (this.selectedEntity) {
    this.showBoundingBox(this.selectedEntity);
  }
};

/**
 * Find skin oject (if any) for a model
 *
 */

GZ3D.Scene.prototype.findSkinObject = function(model) {
  if (model) {
    if (model._skinnedObject) return model._skinnedObject;

    var sko = null;

    model.traverse(function(obj) {
      if (obj._skinnedObject) sko = obj._skinnedObject;
    });

    return sko;
  }

  return null;
};

/**
 * Returns true if the model has a skin
 *
 */

GZ3D.Scene.prototype.hasSkin = function(model) {
  return !!this.findSkinObject(model);
};

/**
 * Show/hide skin for selection
 *
 */

GZ3D.Scene.prototype.setSkinVisible = function(model, visible) {
  var sko = this.findSkinObject(model);
  if (sko) this.composer.setSkinVisibility(sko, visible);
};

/**
 * Returns true if current selection's skin is visible
 *
 */

GZ3D.Scene.prototype.skinVisible = function(model) {
  var sko = this.findSkinObject(model);
  if (sko) return sko.skinMesh.visible;

  return false;
};

/**
 * Add a skin mesh
 *
 */

GZ3D.Scene.prototype.addSkinMesh = function(skinMeshDefinition) {
  this.composer.addSkinMesh(skinMeshDefinition);
};
