/**
 *
 * GZ3D RGB Curves correction shader.
 */

GZ3D.RGBCurvesShader = {
  uniforms: {
    tDiffuse: { value: null },
    tCurveMapper: { value: null }
  },

  vertexShader: [
    'varying vec2 vUv;',

    'void main() {',

    'vUv = uv;',
    'gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',

    '}'
  ].join('\n'),

  fragmentShader: [
    'uniform sampler2D tCurveMapper;',
    'uniform sampler2D tDiffuse;',
    'varying vec2 vUv;',

    'void main() {',

    'vec4 texel = texture2D( tDiffuse, vUv );',

    'texel[0] = texture2D( tCurveMapper, vec2(texel[0],0) )[0];',
    'texel[1] = texture2D( tCurveMapper, vec2(texel[1],0) )[1];',
    'texel[2] = texture2D( tCurveMapper, vec2(texel[2],0) )[2];',

    'gl_FragColor = texel;',

    '}'
  ].join('\n')
};

// This method takes one curve per channel and render it to a ramp texture that is used by the shader
// to correct the colors.

GZ3D.RGBCurvesShader.setupCurve = function(
  redCurve,
  greenCurve,
  blueCurve,
  shader
) {
  var rwidth = 256,
    rheight = 1,
    rsize = rwidth * rheight;
  var tcolor = new THREE.Color(0xffffff);
  var dataColor = new Uint8Array(rsize * 3);
  var i;

  for (var col = 0; col < 3; col++) {
    var colarr;

    switch (col) {
      case 0:
        colarr = redCurve;
        break;
      case 1:
        colarr = greenCurve;
        break;
      case 2:
        colarr = blueCurve;
        break;
    }

    for (i = 0; i < rwidth; i++) {
      dataColor[i * 3 + col] = i;
    }

    if (colarr.length > 2) {
      var curve = new THREE.CatmullRomCurve3(colarr);

      var steps = rwidth * 10.0;

      for (i = 0; i <= steps + 1.0; i++) {
        var vec = curve.getPoint(i / steps);
        var x = Math.floor(vec.x * (rwidth - 1));
        var val = Math.floor(vec.y * 255);

        dataColor[x * 3 + col] = Math.max(Math.min(val, 255.0), 0.0);
      }
    }
  }

  var colorRampTexture = shader.uniforms['tCurveMapper'].value;

  if (colorRampTexture !== null) {
    colorRampTexture.dispose();
  }

  colorRampTexture = new THREE.DataTexture(
    dataColor,
    rwidth,
    rheight,
    THREE.RGBFormat
  );
  shader.uniforms['tCurveMapper'].value = colorRampTexture;

  colorRampTexture.needsUpdate = true;
};
