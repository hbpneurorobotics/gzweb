module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    gerritBranch: process.env.GERRIT_BRANCH,

    concat: {
      build: {
        src  : ['../src/ROS*.js','../src/gz*.js','../src/**/*.js'],
        dest : '../build/gz3d.js'
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      files: [
        '../build/gz3d.js'
      ]
    },

    uglify: {
      options: {
        report: 'min'
      },
      build: {
        src: '../build/gz3d.js',
        dest: '../build/gz3d.min.js'
      }
    },

    watch: {
      dev: {
        options: {
          interrupt: true
        },
        files: [
          '../src/*.js',
          '../src/**/*.js'
        ],
        tasks: ['concat']
      },
      build_and_watch: {
        options: {
          interrupt: true
        },
        files: [
          'Gruntfile.js',
          '.jshintrc',
          '../src/*.js',
          '../src/**/*.js'
        ],
        tasks: ['build']
      }
    },

    clean: {
      options: {
        force: true
      },
      doc: ['../doc']
    },

    jsdoc: {
      doc: {
        src: [
          '../src/*.js',
          '../src/**/*.js'
        ],
        options: {
          destination: '../doc'
        }
      }
    },

    bump: {
      options: {
        files: ['package.json', '../../bower.json'],
        updateConfigs: ['pkg'],
        commit: false,
        push: false,
        createTag: false
      }
    },

    gitadd: {
      bump: {
        options: {
          force: true
        },
        files: {
          src: ['package.json', '../../bower.json']
        }
      },
      dist: {
        options: {
          message: 'add artefact',
          force: true,
          verbose: true
        },
        files: {
          src: ['../build/gz3d.js']
        }
      }
    },

    gitcommit: {
      bump: {
        options: {
          message: 'bump to <%= pkg.version %>',
          remote: 'origin',
          branch: 'HEAD:<%= gerritBranch %>',
          ignoreEmpty: true
        },
        files: {
          src: ['package.json', '../../bower.json']
        }
      },
      dist: {
        options: {
          message: 'commit artefact',
          allowEmpty: true,
          verbose: true
        },
        files: {
          src: ['../build/gz3d.js']
        }
      }
    },

    gittag: {
      dist: {
        options: {
          tag: '<%=pkg.version%>',
          message: 'Version <%=pkg.version%> release'
        }
      }
    },

    gitpush: {
      bump: {
        options: {
          remote: 'origin',
          branch: 'HEAD:<%= gerritBranch %>',
          tags: true,
          verbose: true // for debug purpose
        }
      },
      dist: {
        options: {
          remote: 'origin',
          branch: 'HEAD:<%= gerritBranch %>',
          verbose: true
        }
      }
    },

    ci: {
      options: {
        gerritBranch: '<%= gerritBranch %>'
      }
    },
  });

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  grunt.registerTask('dev', ['concat', 'watch']);
  grunt.registerTask('build', ['concat', 'jshint', 'uglify']);
  grunt.registerTask('build_and_watch', ['watch']);
  grunt.registerTask('doc', ['clean', 'jsdoc']);

  grunt.registerTask('ci', 'Run all the build steps on the CI server', function(target) {
    var tasks = ['concat', 'jshint', 'uglify', 'gitadd:dist', 'gitcommit:dist', 'gitpush:dist'];
    var branch = this.options().gerritBranch;
    grunt.log.writeln('[grunt ci:' + target + '] GERRIT_BRANCH is: ' + branch);
    if (target === 'patch' || target === 'minor' || target === 'major') {
      // commit bumped version numbers
      tasks.unshift('bump:' + target);
      tasks.push('gitadd:bump');
      tasks.push('gitcommit:bump');
      tasks.push('gitpush:bump');

      // commit git tag
      tasks.push('gittag:dist');
      tasks.push('gitpush:bump');
    }
    grunt.task.run(tasks);
  });
};
